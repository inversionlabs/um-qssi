Melt production results for all UM QSSI sites on the GrIS
Completed by Patrick Wright
Inversion Labs
11/8/2017

Contained in this directory:

1. Individual directories for each site (e.g. 'metDATA_27km-11')
-------------------------------------------------------------
Within each directory is:
--ReadMe describing input met data sources
--Python script
--Input met data files (either a single file or a 'data_input' directory)
--Output data files (melt production results, in the 'data_output' directory)

  The output data for each site consists of 3 files:

  1. Sonic Melt (NOTE: the sonic melt is indexed only for days with daily ave
  air temperature above 0 C. There are many days that show lowering of surface
  height due to wind scour of snow storms, and these days are (roughly) discluded
  by this method. The full sonic melt time series is also available and can be
  exported from the script.)
  2. PDD melt (positive degree day)
  3. MTI melt (modified temperature index)
  
  **All melt results are in units of (m/day)!
  
NOTE: The input directories contain met data that is redundant with other files
in the UM QSSI Box directory. I have replicated my exact directory structure here
such that someone could install the proper Python libraries, run the script,
and reproduce the results. I did not include a requirements.txt for the Python
libraries, but these can be deduced from the import statements at the top of the
script.
  
2. 'melt_results' directory. This contains all the output files described above.
(exact same files as in the individual site directories)

3. 'BlockSite-met.csv'
This is raw met data for the entire time series at the Block Site (33km-14).
Previously, all that was available were various files for different time ranges
as data was downloaded from the met station in chunks. This is now the single 
file with all continuous met data.

**NOTE: No QAQC on this file!
FOR RADIATION, the following must be applied to get the correct values:
radiation_corrected = (radiation / 1000.) * 76.34

4. 'BlockSite-met-QAQC.csv'
This contains QAQC data for the variables needed for melt production calculation.
Outliers and obvious bad data have been dropped.
Units:
radiation_corrected: W/m^2
sonic_edit: m
temp1: C (lower sensor)
temp2: C (upper sensor)

5. 'BlockSite-rain-dailytotal-meters.csv'
Daily rain totals in meters for the block site. No QAQC. I would be suspect of
winter rain totals, where it is possible for snow to accumulate in the tipping
bucket, then suddenly melt and go through the bucket as if a single rain event.

6. 'melt-methods.pdf'
This describes all methods used by P. Wright in melt production calculations.

7. 'melt-results.pdf'
Figures showing sonic melt, PDD melt, and MTI melt for each site.

8. 'melt-parameter-opt.pdf'
Figures showing parameter optimization (minimizing summed sqaure errors) for each site.



