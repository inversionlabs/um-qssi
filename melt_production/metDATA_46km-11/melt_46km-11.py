#!/usr/bin/env python
# Script for calculating melt production at site "46km-11"

# Patrick Wright
# Inversion Labs
# Oct, 2017

import numpy as np
import scipy as sp
import pandas as pd
from datetime import datetime, timedelta
from functools import partial
from IPython import embed
import matplotlib
import matplotlib.pyplot as plt

import statsmodels.formula.api as smf

import collections # needed for ordered dictionary

from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm

from sklearn.linear_model import LinearRegression

#-------------------------------------------------------------------------------
print "Processing datafiles..."

# Define directory paths:

fpath1='M_GL11_2.txt'

# Read in data:
data = pd.read_csv(fpath1, sep='	', skiprows=2, header=None)

# Name the variables:
DOY = data[0]
temp = data[1]
sonic = data[2]
rad = data[3]

# Make a new DataFrame with named variables:
data_dict = {'DOY':DOY, 'temp':temp, 'sonic':sonic, 'rad':rad}
df = pd.DataFrame(data=data_dict)

# Convert DOY to Datetime:
df.DOY = pd.to_datetime('2011-1-1') + pd.to_timedelta(df.DOY, unit='D') - pd.Timedelta(days=1)
#This also works:
#test = pd.to_datetime(df.DOY, unit='D', origin=pd.Timestamp('01-01-2011'))

# set index of dataframe to datetime, round to nearest minute, and drop redundant column:
df = df.set_index(df.DOY)
round_index = df.index.round('min')
df = df.set_index(round_index)
df.index.rename('Datetime', inplace=True)
df.drop('DOY', axis=1, inplace=True)

# Drop partial days at beginning and end...
# (average daily temp and radiation totals will not be accurate)
df = df.drop(df['2011-07-12'].index)
df = df.drop(df['2012-08-18'].index)

# NOTE:
# This dataset switches from 1-hr to 4-hr resolution here:
#2011-09-14 06:00:00
#2011-09-14 07:00:00   
#2011-09-14 08:00:00  
#2011-09-14 09:00:00  
#2011-09-14 10:00:00
#-------------------
#2011-09-14 12:00:00  
#2011-09-14 16:00:00  
#2011-09-14 20:00:00
#2011-09-15 00:00:00
#-------------------------------------------------------------------------------

print "Define variables..."

rad = df['rad']
sonic = df['sonic']
temp = df['temp']

# -----------------------------------------------------------------
# AIR TEMP:
# -----------------------------------------------------------------

# Before calculating daily averages, get '2011-09-14' to all 4-hr resolution:
temp_onehour = temp[:'2011-09-13 23:00:00']
temp_start0914 = temp['2011-09-14 00:00:00':'2011-09-14 12:00:00']
# This selects the actual value for each 4-hr entry in the original data:
temp_start0914_resample = temp_start0914.resample('4H').first()
temp_fourhour1 = temp_start0914_resample
temp_fourhour2 = temp['2011-09-14 16:00:00':]
temp_fourhour = pd.concat([temp_fourhour1, temp_fourhour2])
temp = pd.concat([temp_onehour, temp_fourhour])

# Drop partial days at beginning/end:

# Calculate daily average:
temp_dailyave = temp.resample('D').mean()

# Daily ave temps above zero:
Td = temp_dailyave[temp_dailyave > 0.0]

# -----------------------------------------------------------------
# RADIATION:
# -----------------------------------------------------------------

# Hourly data, sum all values for daily total.
# 4-hr data, multiply each value * 4, then sum to get equivalent daily total (Wh/m^2)

# Calculate daily totals from one-hour:
rad_onehour = rad[:'2011-09-13 23:00:00']
rad_onehour_dailytotal = rad_onehour.resample('D').sum()

# Calculate daily totals from four-hour:
# For simplicity, resample the beginning of '2011-09-15' to 4-hr.
rad_start0914 = rad['2011-09-14 00:00:00':'2011-09-14 12:00:00']
# This selects the actual value for each 4-hr entry in the original data:
rad_start0914_resample = rad_start0914.resample('4H').first()
rad_fourhour1 = rad_start0914_resample
rad_fourhour2 = rad['2011-09-14 16:00:00':]
rad_fourhour = pd.concat([rad_fourhour1, rad_fourhour2])
rad_4hour_mult = rad_fourhour * 4.
rad_fourhour_dailytotal = rad_4hour_mult.resample('D').sum()

rad_dailytotal = pd.concat([rad_onehour_dailytotal, rad_fourhour_dailytotal])

# Rad data looks bad '2012-03-07':'2012-03-18'
rad_dailytotal = rad_dailytotal.drop(rad_dailytotal['2012-03-07':'2012-03-18'].index)

# -----------------------------------------------------------------
# SONIC:
# -----------------------------------------------------------------

# Sonic is smoothed, no outliers, ready to go!
# NOTE: Sonic is NaNs until first entry at '2011-07-19 18:00:00'
sonic.dropna(inplace=True)

# -----------------------------------------------------------------
# SONIC MELT:
# -----------------------------------------------------------------
# Calculate daily height change from smoothed sonic data:
midnight_index = sonic.index.indexer_at_time('00:00:00') # returns integer index locations
midnight_height = sonic.iloc[midnight_index]

# height_delta value is assigned to date at the end of the 24-hr period
height_delta = midnight_height.diff()

# shift data such that height_delta value represents melt occuring over that day
# (i.e., value is assigned to date at the beginning of the 24-hr period)
height_delta = height_delta.shift(-1, freq='D')

# get rid of positive values:
# NOTE: The sonic data becomes increasingly negative, thus a negative diff value indicates melt! (or scour)
height_delta_neg = height_delta[height_delta < 0.]

# For calculating melt, turn the values positive:
height_delta_pos = height_delta_neg * -1.

melt_sonic = height_delta_pos * (910./1000.) # m/day
# make into Series and rename:
melt_sonic = melt_sonic.rename('melt_sonic')

# Reduce to values only where daily ave temp is above freezing:
# NOTE: As for Block Site, this is attempting to get rid of periods of scour that still show a negative height change...
# For parameter optimization, will want this anyway, as PDD or MTI models only run for days above freezing.
melt_sonic_abovefreeze = melt_sonic[Td.index]
#melt_sonic_abovefreeze.dropna(inplace=True)

# make into Series and rename:
melt_sonic_abovefreeze = melt_sonic_abovefreeze.rename('melt_sonic_abovefreeze')
# Export data:
melt_sonic_abovefreeze.to_csv("melt_sonic_46km-11.csv", header=True, index=True)

# -----------------------------------------------------------------
# SIMPLE PDD MELT:
# -----------------------------------------------------------------
# PDD should result in melt with range of ~0.0 - 0.1 m/day (mostly 0.02 - 0.04)

# Range of melt factors (fm) to test:
# Note, Hock (2003) reports range of ~5-20 mm/day/C for Greenland
# This is 0.005 - 0.02 meters/day/C

# Based on initial calculations, need to test even higher values...

fm = np.arange(0.0,0.0301,0.0001)

# Return an ordered dictionary, where each item in the dict
# is a Pandas series, using each value from fm
def calc_PDD(fm, Td):
  d={}
  for x in fm:
    PDD = x * Td
    d["fm{0}".format(x)] = PDD
  od = collections.OrderedDict(sorted(d.items()))
  return od

#PDD_testallfm = calc_PDD(fm, Td)

# If there are NaNs in 'melt_sonic_abovefreeze' (days with no measured melt, but still Td>0), drop these days from both.
melt_sonic_abovefreeze_NaNs = melt_sonic_abovefreeze.isnull()
NaNlist_index = melt_sonic_abovefreeze_NaNs[melt_sonic_abovefreeze_NaNs==True].index

melt_sonic_abovefreeze_limited = melt_sonic_abovefreeze.drop(NaNlist_index)
Td_limited = Td.drop(NaNlist_index)
# Now we have two series of equal length (matching indices), to use for optimization

PDD_testallfm_limited = calc_PDD(fm, Td_limited)

# Calculate the sum of ((sonic melt - PDD melt)^2) using each PDD.
# This is summing the result of the squared difference (error) for each day.
# Return a dictionary where (key, value) is (fm, sum)
def calc_sumleastsquares(melt_sonic,PDD_ordereddict):
  d={}
  for key, value in PDD_ordereddict.iteritems():
    diff_squared = (melt_sonic - PDD_ordereddict[key])**2.
    sum_diff_squared = diff_squared.sum()
    d["{0}".format(key)] = sum_diff_squared
  od = collections.OrderedDict(sorted(d.items()))
  return od

# Simply get the values out of the ordered dictionary, using the keys as an index:
def get_values(od):
  series = pd.Series()
  for key, value in od.iteritems():
    series[key] = value
  return series
    
sumleastsquares_dict = calc_sumleastsquares(melt_sonic_abovefreeze_limited, PDD_testallfm_limited)

sumleastsquares_values = get_values(sumleastsquares_dict)

# Find the minimum!
min_fm_sum = sumleastsquares_values[sumleastsquares_values==sumleastsquares_values.min()]

print ''
print 'PDD'
print min_fm_sum.index
#-------------------------------
min_fm = 0.0157
#-------------------------------
print 'Minimized fm = %s' %str(min_fm)

# NOTE: this is optimized PDD for all days with ave temp > 0 (NOT using Td_limited):
optimized_PDD = min_fm * Td

# make into Series and rename:
optimized_PDD = optimized_PDD.rename('optimized_PDD')
# Export PDD:
optimized_PDD.to_csv("melt_PDD_46km-11.csv", header=True, index=True)

# For plotting PDD vs. sonic, limit PDD to index of melt_sonic_abovefreeze_limited
# optimized_PDD is 136 days
# melt_sonic_abovefreeze_limited is 128 days
# Therefore, 8 days where PDD predicted melt, but sonic showed no melt (spatial variability?).
# Note, cannot be due to limiting sonic to above freezing, since by definition PDD is only for days above freezing.
optimized_PDD_matchsonic = optimized_PDD[melt_sonic_abovefreeze_limited.index]

# Code for producing best-fit line and R^2, using StatsModels:
d = {'sonic': melt_sonic_abovefreeze_limited, 'PDD': optimized_PDD_matchsonic}
df = pd.DataFrame(data=d)

# points linearly spaced on sonic
x1 = pd.DataFrame({'sonic': np.linspace(df.sonic.min(), df.sonic.max(), 100)})

# 1st order polynomial
poly_1 = smf.ols(formula='PDD ~ 1 + sonic', data=df).fit()

# -----------------------------------------------------------------
# MODIFIED TEMP INDEX (MTI) MELT:
# -----------------------------------------------------------------
# Range of parameter space to explore:
fmx = np.arange(0.0095,0.0095+(0.0001*100),0.0001) #len 100 (tighter range - for solution)
#fmx = np.arange(0.005,0.0150,0.0001) #len 100 (test larger range)
#a = np.arange(3.5e-07,3.5e-07+(0.6e-08*100),0.6e-08) #(tighter range - for solution)
a = np.arange(5.e-09,3.e-06,3.e-08) #len 100 (test larger range)

# limit rad (len=426) to index for Td (len=136):
rad_limited = rad_dailytotal[Td.index]

# No NaNs present in rad_limited...

# If there are NaNs in 'melt_sonic_abovefreeze', drop these days from all three.
melt_sonic_abovefreeze_NaNs = melt_sonic_abovefreeze.isnull()
NaNlist_index = melt_sonic_abovefreeze_NaNs[melt_sonic_abovefreeze_NaNs==True].index

# MODEL INPUTS:
rad_limited2 = rad_limited.drop(NaNlist_index)
Td_limited2 = Td.drop(NaNlist_index)
melt_sonic_abovefreeze_limited2 = melt_sonic_abovefreeze.drop(NaNlist_index)
# Now we have three series of equal length (matching indices), to use for optimization

# -----------------------------------------------------------------
# TURN THE FOLLOWING ON/OFF TO RUN NEW PARAMETERS AND FIND NEW MIN SUM
# Comment out to run script with faster speeds, using defined parameters below.
# This block is also required to generate 3D plots (mti parameter optimization plot)

# Return an ordered dictionary, where each item in the dict
# is a Pandas series, using each possible value combination from 'fmx' and 'a'.
def calc_mti(fmx, a, Td, rad):
  d={}
  d_fmx={}
  d_a={}
  for x in fmx:
    for p in a:
      mti = (x * Td) + (p * rad)
      d[("fmx{0}".format(x), "a{0}".format(p))] = mti
  od = collections.OrderedDict(sorted(d.items()))
  return od

# Calculate the sum of ((sonic melt - mti melt)^2) using each mti.
# To be explicit, this is summing the result of the squared difference (error) for each day.
# Return a dictionary where (key, key, value) is (fmx, a, sum)
def calc_sumleastsquares_mti(melt_sonic, mti_ordereddict):
  d={}
  for key, value in mti_ordereddict.iteritems():
    diff_squared = (melt_sonic - value)**2.
    sum_diff_squared = diff_squared.sum()
    d["{0}".format(key)] = sum_diff_squared
  od = collections.OrderedDict(sorted(d.items()))
  return od

# Simply get the values out of the ordered dictionary, using the keys as an index:
def get_values_mti(od):
  series = pd.Series()
  for key, value in od.iteritems():
    series[key] = value
  return series

# This retrieves the values of 'fmx' and 'a' as used in the parameter optimization,
# with the order of each Series the same as used in calc_mti()
def get_param_series(fmx, a):
  d_fmx={}
  d_a={}
  for x in fmx:
    for p in a:
      d_fmx[("fmx{0}".format(x), "a{0}".format(p))] = x
      d_a[("fmx{0}".format(x), "a{0}".format(p))] = p
  od_fmx = collections.OrderedDict(sorted(d_fmx.items()))
  od_a = collections.OrderedDict(sorted(d_a.items()))
  return od_fmx, od_a
  
mti_testall = calc_mti(fmx, a, Td_limited2, rad_limited)
sumleastsquares_dict_mti = calc_sumleastsquares_mti(melt_sonic_abovefreeze_limited2, mti_testall)
sumleastsquares_values_mti = get_values_mti(sumleastsquares_dict_mti)
fmx_values_dict, a_values_dict = get_param_series(fmx,a)

# Return the values, without corresponding index:
# (needed for 3D plots)
fmx_values = fmx_values_dict.values()
a_values = a_values_dict.values()

# Find the minimum!
min_mti_sum = sumleastsquares_values_mti[sumleastsquares_values_mti==sumleastsquares_values_mti.min()]

print ''
print 'MTI'
print min_mti_sum
#print min_mti_sum.index
# -----------------------------------------------------------------
# Enter the actual values below, make sure they match the terminal print statement!!
#-------------------------------
min_fmx = 0.0125
min_a = 1.265e-06
#-------------------------------
print 'Minimized fmx = %s' %str(min_fmx)
print 'Minimized a = %s' %str(min_a)

# NOTE: this is optimized mti for all days with ave temp > 0:
optimized_mti = (min_fmx * Td) + (min_a * rad_limited)

# make into Series and rename:
optimized_mti = optimized_mti.rename('optimized_mti')
# Export MTI:
optimized_mti.to_csv("melt_MTI_46km-11.csv", header=True, index=True)

# NOTE: this is optimized mti for all days with ave temp > 0, AND that show sonic melt:
optimized_mti_limited = (min_fmx * Td_limited2) + (min_a * rad_limited2)

# Code for producing best-fit line and R^2, using StatsModels:
d_mti = {'sonic': melt_sonic_abovefreeze_limited2, 'mti': optimized_mti_limited}
df_mti = pd.DataFrame(data=d_mti)

# points linearly spaced on sonic
x1_mti = pd.DataFrame({'sonic': np.linspace(df_mti.sonic.min(), df_mti.sonic.max(), 100)})

# 1st order polynomial
poly_1_mti = smf.ols(formula='mti ~ 1 + sonic', data=df_mti).fit()

# -----------------------------------------------------------------
# COMPARE WITH 'sklearn'
# -----------------------------------------------------------------
X = pd.concat([Td_limited2, rad_limited2], axis=1)  # the input of the model
y = melt_sonic_abovefreeze_limited2  # measurements

estimator = LinearRegression()  # create the estimator object
estimator.fit(X, y)  # optimize the parameters of the model on the measurements
alpha, beta = estimator.coef_  # the obtained parameters

print ''
print 'SKLEARN'
print 'alpha = %s' %str(alpha)
print 'beta = %s' %str(beta)

optimized_mti_skl = (alpha * Td_limited2) + (beta * rad_limited2)

d_skl={'key_skl': optimized_mti_skl}
sumleastsquares_dict_mti_skl = calc_sumleastsquares_mti(melt_sonic_abovefreeze_limited2, d_skl)
sumleastsquares_values_mti_skl = get_values_mti(sumleastsquares_dict_mti_skl)
print 'min sum using skl:'
print sumleastsquares_values_mti_skl[0]

# Code for producing best-fit line and R^2, using StatsModels:
d_mti_skl = {'sonic_skl': melt_sonic_abovefreeze_limited2, 'mti_skl': optimized_mti_skl}
df_mti_skl = pd.DataFrame(data=d_mti_skl)

# points linearly spaced on sonic
x1_mti_skl = pd.DataFrame({'sonic_skl': np.linspace(df_mti_skl.sonic_skl.min(), df_mti_skl.sonic_skl.max(), 100)})

# 1st order polynomial
poly_1_mti_skl = smf.ols(formula='mti_skl ~ 1 + sonic_skl', data=df_mti_skl).fit()

# -----------------------------------------------------------------
# PLOTS
# -----------------------------------------------------------------
# SIMPLE PLOTS -- MEASURED VARIABLES:

#AIR TEMP
fig1 = plt.figure()
ax1 = fig1.add_subplot(111)
ax1.plot(temp.index, temp, label='temp (C)') 
ax1.set_ylabel('Temp ($^\circ$C)')
plt.axhline(y=0.0, color='k')
ax1.set_title('Air Temp, Site 46km-11')
ax1.legend()
fig1.tight_layout()  # Make the figure use all available whitespace

#AIR TEMP DAILY AVE'S
fig10 = plt.figure()
ax1 = fig10.add_subplot(111)
ax1.plot(temp_dailyave.index, temp_dailyave, label='temp (C)') 
ax1.set_ylabel('Temp ($^\circ$C)')
plt.axhline(y=0.0, color='k')
ax1.set_title('Air Temp Daily Ave, Site 46km-11')
ax1.legend()
fig10.tight_layout()  # Make the figure use all available whitespace

# SONIC
fig3 = plt.figure()
ax1 = fig3.add_subplot(111)
ax1.plot(sonic.index,sonic)
ax1.set_ylabel('change in surface height (m)', fontsize=12)
ax1.set_title('Sonic, Site 46km-11', fontsize=14)
ax1.tick_params(labelsize=12)
#plt.gca().invert_yaxis()
fig3.tight_layout()

# RADIATION
fig7 = plt.figure()
ax1 = fig7.add_subplot(111)
ax1.plot(rad.index, rad, label='radiation (W/m^2)') 
ax1.set_ylabel('radiation (W/m^2)', fontsize=12)
ax1.set_title('Shortwave Incoming Radiation, Site 46km-11', fontsize=14)
ax1.tick_params(labelsize=12)
fig7.tight_layout()

# RADIATION DAILY TOTALS
fig70 = plt.figure()
ax1 = fig70.add_subplot(111)
ax1.plot(rad_dailytotal.index, rad_dailytotal, label='radiation daily total (Wh/m^2)') 
ax1.set_ylabel('radiation (Wh/m^2)', fontsize=12)
ax1.set_title('Radiation Daily Total, Site 46km-11', fontsize=14)
ax1.tick_params(labelsize=12)
fig70.tight_layout()

# -----------------------------------------------------------------
# OPTIMIZING FACTORS:

# OPTIMIZED FM FACTORS (PDD):
fig100 = plt.figure()
ax1 = fig100.add_subplot(111)
ax1.scatter(fm, sumleastsquares_values, s=2) 
plt.axvline(x=min_fm, color='red')
ax1.set_ylabel(r'sum of squared errors $\sum (sonic_i - PDD_i)^2$')
ax1.set_xlabel(r'$F_m$ value')
ax1.set_xlim(-0.001,0.032)
plt.xticks(np.arange(min(fm), max(fm)+0.0005, 0.005))
ax1.set_title(r'Optimization of $F_m$, Site 46km-11')
fig100.tight_layout()  # Make the figure use all available whitespace

# OPTIMIZED MTI -- 3D error surface:
x = fmx_values
y = a_values
z = sumleastsquares_values_mti

#histoplot(z,'MID (Dec. 15 - March 30)')

d = {'fmx': x, 'a': y, 'sumls': z}
df = pd.DataFrame(data=d)

X = df.fmx
Y = df.a
Z = df.sumls

VecStart_x = min_fmx #temp
VecStart_y = min_a #rad
VecStart_z = np.min(df.sumls)
VecEnd_x = min_fmx
VecEnd_y = min_a
VecEnd_z = 0.04 # arbitrary

VecStart_x_skl = alpha #temp
VecStart_y_skl = beta #rad
VecStart_z_skl = sumleastsquares_values_mti_skl[0]
VecEnd_x_skl = alpha
VecEnd_y_skl = beta
VecEnd_z_skl = 0.04 # arbitrary

# 3D Scatter
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.scatter(X, Y, Z, c='b', marker='.', zorder=0)
ax.plot([VecStart_x,VecEnd_x],[VecStart_y,VecEnd_y],[VecStart_z,VecEnd_z], color='r')
ax.plot([VecStart_x_skl,VecEnd_x_skl],[VecStart_y_skl,VecEnd_y_skl],[VecStart_z_skl,VecEnd_z_skl], color='b') #min using skl method

ax.set_xlim([np.min(X), np.max(X)])
ax.set_ylim([np.min(Y), np.max(Y)])
ax.set_zlim([np.min(Z), np.max(Z)])

ax.set_xlabel(r'$f_m$', fontsize=12)
ax.set_ylabel(r'$a$', fontsize=12)
ax.set_zlabel(r'sum of squared errors $\sum (sonic_i - mti_i)^2$')
ax.set_title('Parameter Optimization: Modified Temp Index melt, Site 46km-11')

# OPTIMIZED FMX FACTORS (MTI):
fig101 = plt.figure()
ax1 = fig101.add_subplot(111)
ax1.scatter(x, z, s=2) 
#plt.axvline(x=min_z, color='red')
ax1.set_ylabel(r'sum of squared errors $\sum (sonic_i - MTI_i)^2$')
ax1.set_xlabel(r'$f_m$ value')
#ax1.set_xlim(-0.001,0.032)
#plt.xticks(np.arange(min(fm), max(fm)+0.0005, 0.005))
ax1.set_title(r'Optimization of $f_m$: MTI model')
fig101.tight_layout()  # Make the figure use all available whitespace

# OPTIMIZED a FACTORS (MTI):
fig101 = plt.figure()
ax1 = fig101.add_subplot(111)
ax1.scatter(y, z, s=2) 
#plt.axvline(x=min_z, color='red')
ax1.set_ylabel(r'sum of squared errors $\sum (sonic_i - MTI_i)^2$')
ax1.set_xlabel(r'$a$ value')
#ax1.set_xlim(0,6e-7)
#plt.xticks(np.arange(min(fm), max(fm)+0.0005, 0.005))
ax1.set_title(r'Optimization of $a$: MTI model')
fig101.tight_layout()  # Make the figure use all available whitespace

# -----------------------------------------------------------------
# RESULTS -- MODEL vs. MEASURE

# SONIC MELT ABOVE FREEZE + OPTIMIZED PDD MELT (BARS)
fig4 = plt.figure()
ax1 = fig4.add_subplot(111)
WIDTH = 0.3
# align can only be 'center' or 'edge', where 'edge' uses left edge of bar at corresponding index
# This implementation plots bars at midnight, representing melt over following day.
ax1.bar(melt_sonic_abovefreeze.index -  pd.DateOffset(days=0.15), melt_sonic_abovefreeze, width=WIDTH, edgecolor='black', align='center', label='sonic')
ax1.bar(optimized_PDD.index +  pd.DateOffset(days=0.15), optimized_PDD, width=WIDTH, edgecolor='black', align='center', label='PDD')
ax1.xaxis_date()
ax1.set_ylabel('Melt (m/day)', fontsize=12)
ax1.set_title('Sonic Melt (above freezing only) and PDD Melt - Site 46km-11', fontsize=14)
ax1.tick_params(labelsize=12)
plt.legend()
fig4.tight_layout()

# Make continuous and fill missing data with 0. (for line plots)
date_range1 = pd.date_range(melt_sonic_abovefreeze.index.min(), melt_sonic_abovefreeze.index.max())
melt_sonic_insertzeros = melt_sonic_abovefreeze.reindex(date_range1,fill_value=0.)

date_range2 = pd.date_range(optimized_PDD.index.min(), optimized_PDD.index.max())
melt_PDD_insertzeros = optimized_PDD.reindex(date_range2,fill_value=0.)

# SONIC MELT ABOVE FREEZE + OPTIMIZED PDD MELT (LINES)
fig40 = plt.figure()
ax1 = fig40.add_subplot(111)
ax1.plot(melt_sonic_insertzeros.index, melt_sonic_insertzeros, label='sonic')
ax1.plot(melt_PDD_insertzeros.index, melt_PDD_insertzeros, label='PDD')
ax1.xaxis_date()
ax1.set_ylabel('Melt (m/day)', fontsize=12)
ax1.set_title('Sonic Melt (above freezing only) and PDD Melt - Site 46km-11', fontsize=14)
ax1.tick_params(labelsize=12)
plt.legend()
fig40.tight_layout()

# -----------------------------------------------------------------
# SONIC MELT ABOVE FREEZE + OPTIMIZED MTI MELT (BARS)
fig200 = plt.figure()
ax1 = fig200.add_subplot(111)
WIDTH = 0.3
# align can only be 'center' or 'edge', where 'edge' uses left edge of bar at corresponding index
# This implementation plots bars at midnight, representing melt over following day.
ax1.bar(melt_sonic_abovefreeze.index -  pd.DateOffset(days=0.15), melt_sonic_abovefreeze, width=WIDTH, edgecolor='black', align='center', label='sonic')
ax1.bar(optimized_mti.index +  pd.DateOffset(days=0.15), optimized_mti, width=WIDTH, edgecolor='black', align='center', label='Modified Temp Index')
ax1.xaxis_date()
ax1.set_ylabel('Melt (m/day)', fontsize=12)
ax1.set_title('Sonic Melt (above freezing only) and Modified Temp Index Melt - Site 46km-11', fontsize=14)
ax1.tick_params(labelsize=12)
plt.legend()
fig200.tight_layout()

# Make continuous and fill missing data with 0. (for line plots)
date_range2 = pd.date_range(optimized_mti.index.min(), optimized_mti.index.max())
melt_MTI_insertzeros = optimized_mti.reindex(date_range2,fill_value=0.)

# SONIC MELT ABOVE FREEZE + OPTIMIZED MTI MELT (LINES)
fig41 = plt.figure()
ax1 = fig41.add_subplot(111)
ax1.plot(melt_sonic_insertzeros.index, melt_sonic_insertzeros, label='sonic', color='black')
ax1.plot(melt_PDD_insertzeros.index, melt_PDD_insertzeros, label='PDD', color='blue')
ax1.plot(melt_MTI_insertzeros.index, melt_MTI_insertzeros, label='MTI', color='red')
ax1.xaxis_date()
ax1.set_ylabel('Melt (m/day)', fontsize=12)
ax1.set_title('Sonic Melt (above freezing only), PDD Melt, & MTI Melt - Site 46km-11', fontsize=14)
ax1.tick_params(labelsize=12)
plt.legend()
fig41.tight_layout()

# -----------------------------------------------------------------
# CORRELATION PLOTS:

#SONIC VS OPTIMIZED PDD -- BEST FIT LINE
fig400 = plt.figure()
ax1 = fig400.add_subplot(111)
plt.plot(melt_sonic_abovefreeze_limited, optimized_PDD_matchsonic, 'o', alpha=.8, zorder=0, label='')
plt.plot(x1.sonic, poly_1.predict(x1), 'b-', label='1st order poly fit, $R^2$=%.2f' % poly_1.rsquared, 
         alpha=0.9)
ax1.set_ylabel('optimized PDD melt (m/day)')
ax1.set_xlabel('sonic melt (m/day)')
#ax1.set_xlim()
#plt.xticks()
ax1.set_title('PDD vs. Sonic, Site 46km-11')
plt.legend()
fig400.tight_layout()

# SONIC VS OPTIMIZED MTI -- BEST FIT LINE
fig450 = plt.figure()
ax1 = fig450.add_subplot(111)
plt.plot(melt_sonic_abovefreeze_limited2, optimized_mti_limited, 'o', alpha=.8, zorder=0, label='')
plt.plot(x1_mti.sonic, poly_1_mti.predict(x1_mti), 'b-', label='1st order poly fit, $R^2$=%.2f' % poly_1_mti.rsquared, 
         alpha=0.9)
ax1.set_ylabel('optimized mti melt (m/day)')
ax1.set_xlabel('sonic melt (m/day)')
#ax1.set_xlim()
#plt.xticks()
ax1.set_title('Modified Temp Index (MTI) vs. Sonic, Site 46km-11')
plt.legend()
fig450.tight_layout()

# SONIC VS OPTIMIZED MTI -- USING SKL -- BEST FIT LINE
fig500 = plt.figure()
ax1 = fig500.add_subplot(111)
plt.plot(melt_sonic_abovefreeze_limited2, optimized_mti_skl, 'o', alpha=.8, zorder=0, label='')
plt.plot(x1_mti_skl.sonic_skl, poly_1_mti_skl.predict(x1_mti_skl), 'b-', label='1st order poly fit, $R^2$=%.2f' % poly_1_mti_skl.rsquared, 
         alpha=0.9)
ax1.set_ylabel('optimized mti melt -- skl (m/day)')
ax1.set_xlabel('sonic melt (m/day)')
#ax1.set_xlim()
#plt.xticks()
ax1.set_title('Modified Temp Index (MTI) -- SKL vs. Sonic, Site 46km-11')
plt.legend()
fig500.tight_layout()

#plt.show()