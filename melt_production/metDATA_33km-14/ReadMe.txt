ReadMe.txt
Created 9/22/2017
Patrick Wright
Inversion Labs

The following files are located in 'data_input'.

9/22/2017
---------
Downloaded 3 data sources from Box (1-3 below):

1. "BlockMetStation 2016 to 2017". Downloaded entire directory as .zip from:
../Our_Field_DATA/Meteorological_Data/Block Site Met 2016 to 2017/BlockMetStation 2016 to 2017

This directory contains raw .dat files ranging met696.dat - met1038.dat. 
I added catfiles.sh to produce concat.txt, however, the data needed to be split 
into two directories: 'part1' and 'part2' -- to deal with 3-digit vs. 4-digit 
filenames and the order of concatenation with the .sh script.
Editing steps:
--run concat.sh in both 'part1' and 'part2'.
--rename concat.csv in each of 'part1' and 'part2' to 'concat1.csv' and 'concat2.csv', respectively.
--copy both .csv files back to main directory ('BlockMetStation 2016 to 2017').
--delete extra empty line at the end of each file.
--in terminal, run 'cat *.csv > 2016-17.csv'
--copy header from 'fullseries_July15.csv' and paste onto '2016-17.csv'
--for some reason there was not a line break before the entry for "2017-06-13 00:15:00".
  Manually added line break.

'2016-17.csv' contains all concatenated data, ranging '2016-08-14' to '2017-07-22'
Met station was pulled from ice at the end of this date range!

2. 'fullseries_July15.csv'. This is all met data at the block site from the start
of measurements (2014-07-16) through 2015-07-21. Created by PJW. This copy was downloaded from:
Our_Field_DATA/Meteorological_Data/2014/GL14_met 
(not copied over from 'DATA_work_old')

3. 'MET_fullseries_Aprl15toAug16.txt'. This file was created with a shell script written by B. Hills. 
Downloaded from:
Our_Field_DATA/Meteorological_Data/GL2015_MET. 
Range is 2015-04-22 to 2016-08-14.

4. 'CR1000_Met_Table1_plot.dat' and 'CR1000_Met_Table1_plot-EDIT.dat'
These files contain data that was missing for a one-day period when the met station was lowered on '2014-08-01'.
PJW had this file on his local computer that was manually taken from met station.