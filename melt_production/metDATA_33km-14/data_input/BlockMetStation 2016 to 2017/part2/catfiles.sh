#!/bin/bash
# This will strip headers from all files and concatenate.
# '-q' will never output headers giving file names between files
# '-n +5' output lines starting with the 5th
tail -q -n +5 met*.dat > concat.csv

