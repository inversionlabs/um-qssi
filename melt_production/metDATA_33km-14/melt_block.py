#!/usr/bin/env python
# Script for calculating melt production at the "Block site" (33km-14).
# Originally copied from 'met_plotter.py'.

# Patrick Wright
# Inversion Labs
# Sept, 2017

import numpy as np
import scipy as sp
import pandas as pd
from datetime import datetime, timedelta
from functools import partial
from IPython import embed
import matplotlib
import matplotlib.pyplot as plt
from math import factorial

import statsmodels.formula.api as smf

import collections # needed for ordered dictionary

from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm

from sklearn.linear_model import LinearRegression

#-------------------------------------------------------------------------------
# Define functions:

# NOTE: this filter is now provided with SciPy
# see http://scipy.github.io/devdocs/generated/scipy.signal.savgol_filter.html#scipy.signal.savgol_filter
def savitzky_golay(y, window_size, order, deriv=0, rate=1):
	try:
		window_size = np.abs(np.int(window_size))
		order = np.abs(np.int(order))
	except ValueError, msg:
		raise ValueError("window_size and order have to be of type int")
	if window_size % 2 != 1 or window_size < 1:
		raise TypeError("window_size size must be a positive odd number")
	if window_size < order + 2:
		raise TypeError("window_size is too small for the polynomials order")
	order_range = range(order+1)
	half_window = (window_size -1) // 2
	# precompute coefficients
	b = np.mat([[k**i for i in order_range] for k in range(-half_window, half_window+1)])
	m = np.linalg.pinv(b).A[deriv] * rate**deriv * factorial(deriv)
	# pad the signal at the extremes with
	# values taken from the signal itself
	firstvals = y[0] - np.abs( y[1:half_window+1][::-1] - y[0] )
	lastvals = y[-1] + np.abs(y[-half_window-1:-1][::-1] - y[-1])
	y = np.concatenate((firstvals, y, lastvals))
	return np.convolve( m[::-1], y, mode='valid')

#-------------------------------------------------------------------------------
print "Processing datafiles..."

# Define directory paths:

# '2014-07-16' to '2015-07-21':
fpath1='data_input/fullseries_July15.csv'

# '2015-04-22' to '2016-08-14'
fpath2='data_input/MET_fullseries_Apr15toAug16.txt'

# '2016-08-14' to '2017-07-22':
fpath3='data_input/BlockMetStation 2016 to 2017/2016-17.csv'

# Using for extra data needed during '2014-08-01' station lower:
fpath4='data_input/CR1000_Met_Table1_plot-EDIT.dat'

# Read in data:
data1 = pd.read_csv(fpath1, sep=',',header=0, low_memory=False)
data2 = pd.read_csv(fpath2, sep=',',header=0, low_memory=False)
data3 = pd.read_csv(fpath3, sep=',',header=0, low_memory=False)
data4 = pd.read_csv(fpath4, sep=',',header=0, low_memory=False)

# Convert datetime string to a Pandas datetime object
timestamp1 = data1['TIMESTAMP']
timestamp2 = data2['TIMESTAMP']
timestamp3 = data3['TIMESTAMP']
timestamp4 = data4['TIMESTAMP']

dt1 = pd.to_datetime(timestamp1, infer_datetime_format=True, errors='coerce')
dt2 = pd.to_datetime(timestamp2, infer_datetime_format=True, errors='coerce')
dt3 = pd.to_datetime(timestamp3, infer_datetime_format=True, errors='coerce')
dt4 = pd.to_datetime(timestamp4, infer_datetime_format=True, errors='coerce')
  
# set index of dataframe to datetime
data_date1 = data1.set_index(dt1)
data_date2 = data2.set_index(dt2)
data_date3 = data3.set_index(dt3)
data_date4 = data4.set_index(dt4)

# Drop duplicate 'TIMESTAMP' column:
data_date1.drop('TIMESTAMP', axis=1, inplace=True)
data_date2.drop('TIMESTAMP', axis=1, inplace=True)
data_date3.drop('TIMESTAMP', axis=1, inplace=True)
data_date4.drop('TIMESTAMP', axis=1, inplace=True)

# index into 'data_date2' to clip off beginning overlap with 'data_date1'.
# Defaulting to using my original file for this overlap data, rather than Ben's.
# Shouldn't matter at all.
# data2 --> data3 is seamless.
data_date2 = data_date2['2015-07-21 00:15:00':]

# Grab needed section from 'data_date4' (missing period associated with station lower):
data_date4_splice = data_date4['2014-08-01 00:10:00':'2014-08-01 20:40:00'] # Note, the entry for 20:50:00 is missing.

# combine all four dataframes -- this method seems to work perfect. Should check....
# Without the sort_index, the concat works but there are out-of-order dates.
data = [data_date1, data_date4_splice, data_date2, data_date3]
full_block_data = pd.concat(data) # This is an outer join?
full_block_data.sort_index(inplace=True)

# Get rid of last 3 rows (garbage strings from the header...)
full_block_data = full_block_data.iloc[:-3]

# Export data:
full_block_data.to_csv("BlockSite-met.csv", header=True, index=True)

# Drop partial days at beginning and end...
# (average daily temp and radiation totals will not be accurate)
full_block_data = full_block_data.drop(full_block_data['2014-07-16'].index)
full_block_data = full_block_data.drop(full_block_data['2017-07-22'].index)

#-------------------------------------------------------------------------------

print "Define variables..."

temp1 = pd.to_numeric(full_block_data['AirTC_1'], errors='coerce')
temp2 = pd.to_numeric(full_block_data['AirTC_2'], errors='coerce')
rh1 = pd.to_numeric(full_block_data['RH_1'], errors='coerce')
rh2 = pd.to_numeric(full_block_data['RH_2'], errors='coerce')
sonic = pd.to_numeric(full_block_data['TCDT'], errors='coerce')
windspeed = pd.to_numeric(full_block_data['WS_ms'], errors='coerce')
winddir = pd.to_numeric(full_block_data['WindDir'], errors='coerce')
radiation = pd.to_numeric(full_block_data['CNR_Wm2'], errors='coerce')
rain = pd.to_numeric(full_block_data['Rain_mm_Tot'], errors='coerce')
battV = pd.to_numeric(full_block_data['BattV'], errors='coerce')
paneltemp = pd.to_numeric(full_block_data['PTemp_C'], errors='coerce')
ducer1 = pd.to_numeric(full_block_data['Ducer_1'], errors='coerce')
ducer2 = pd.to_numeric(full_block_data['Ducer_2'], errors='coerce')


print "Enter main program..."
# -----------------------------------------------------------------
# AIR TEMP:
# -----------------------------------------------------------------
# Drop outliers from both temp records:
temp1 = temp1[temp1 < 10] #50F
temp2 = temp2[temp2 < 10] #50F

# Calculate daily average:
# temp1 or temp2? temp1 tends to read slightly higher than temp2
# temp1 is lower sensor, temp2 is upper sensor

#temp1_dailyave = temp1.resample('D', how='mean') # old implementation
temp2_dailyave = temp2.resample('D').mean()

# Daily ave temps above zero:
Td = temp2_dailyave[temp2_dailyave > 0.0]

# -----------------------------------------------------------------
# RADIATION:
# -----------------------------------------------------------------

# Apply correct MULT value:
radiation_corrected = (radiation / 1000.) * 76.34

# Get rid of outlier values:
radiation_corrected = radiation_corrected[radiation_corrected > -155.]
radiation_corrected = radiation_corrected[radiation_corrected < 540.]

# Get one-hour averages:
# Are these assigned to hour before or hour after? Shouldn't matter for calculating 24-hr sums?
radiation_hourlyave = radiation_corrected.resample('H').mean()

# Sum up the hourly aves for each day (both methods equivalent):
# NOTE: this includes daily net negative values.
# NOTE: the units for the daily totals is Wh/m^2 (energy or 'irradiation')
radiation_dailytotal = radiation_hourlyave.resample('D').sum()
#radiation_dailytotal = radiation_hourlyave.groupby(radiation_hourlyave.index.date).sum()

# July 21-25, 2014 have met station power-issues, should be thrown out.
rad_droplist = radiation_dailytotal['2014-07-21':'2014-07-25']
radiation_dailytotal = radiation_dailytotal.drop(rad_droplist.index)

# After throwing out the period above, 8 net negative days remain (shoulder season days).
# Get only net positive days:
radiation_dailytotal_pos = radiation_dailytotal[radiation_dailytotal > 0.0]

# -----------------------------------------------------------------
# RAIN:
# -----------------------------------------------------------------
# Returns summed daily totals in mm:
rain_dailytotal = rain.resample('D').sum()
rain_dailytotal_m = rain_dailytotal * 0.001 # mm --> meters

# Export rain .csv file:
data = {'rain_dailytotal_meters':rain_dailytotal_m}

df = pd.DataFrame(data=data, index=rain_dailytotal_m.index)

df.to_csv("BlockSite-rain-dailytotal-meters.csv", header=True, index=True)
 
# Plot up rain totals:
#fig100 = plt.figure(figsize=(20,8))
#ax1 = fig100.add_subplot(111)
#WIDTH = 0.75
## align can only be 'center' or 'edge', where 'edge' uses left edge of bar at corresponding index
## This implementation plots bars at midnight, representing rain over following day.
#ax1.bar(rain_dailytotal_m.index, rain_dailytotal_m, width=WIDTH, edgecolor='black', align='center', label='rain (m/day)')
#ax1.xaxis_date()
#ax1.set_ylabel('Rain (m/day)', fontsize=12)
#ax1.set_title('Rain daily totals (m/day), Grid Site', fontsize=14)
#ax1.tick_params(labelsize=12)
#fig100.tight_layout()
#plt.show()

# -----------------------------------------------------------------
# SONIC:
# -----------------------------------------------------------------

sonic_edit = sonic # Make a copy

# STATION LOWER: 2014-08-01
# Set entire lower period to NaN.
# Some beginning "good" data (after 1120) looks suspicious, w/surface gaining
# height rapidly. Field book says sonic data no good 1115 - 1310.
# 1120 is first bad point (start NaN period here).
# However, 1300 and 1310 datapoints look good.
sonic_edit['2014-08-01 11:20:00':'2014-08-01 12:50:00'] = np.NaN

slice1 = sonic_edit[:'2014-08-01 11:30:00'] # All previous to splice
slice1 = slice1 - 0.754 # (1.808-1.054)
slice2 = sonic_edit['2014-08-01 11:40:00':] # All after splice
sonic_edit = pd.concat([slice1, slice2])

# STATION LOWER: 2015-07-16
# No bad data present.
# Station was unplugged, with no data for period 1430-1645 (2.25 hrs)
# Simply splicing last data point to first surrounding lower period.
slice1 = sonic_edit[:'2015-07-16 14:15:00'] # All previous to splice
slice1 = slice1 - 1.689 # (2.665-0.976)
slice2 = sonic_edit['2015-07-16 17:00:00':] # All after splice
sonic_edit = pd.concat([slice1, slice2])

# STATION LOWER: 2016-08-14
# Only one bad data point at 1400
sonic_edit['2016-08-14 14:00:00'] = np.NaN
# Splice first to last:
slice1 = sonic_edit[:'2016-08-14 13:45:00'] # All previous to splice
slice1 = slice1 - 2.339 # (4.094-1.755)
slice2 = sonic_edit['2016-08-14 14:15:00':] # All after splice
sonic_edit = pd.concat([slice1, slice2])

# shift entire record to positive values:
sonic_edit = sonic_edit + 3.628

# Drop NaNs (needed for combine_first)
sonic_edit.dropna(inplace=True)

# get rid of obvious outliers for each winter season:
piece1 = sonic_edit[:'2014-09-01 23:50:00']
season1 = sonic_edit['2014-09-02':'2015-06-16']
piece2 = sonic_edit['2015-06-17 00:00:00':'2015-08-07 23:45:00']
season2 = sonic_edit['2015-08-08':'2016-04-22']
piece3 = sonic_edit['2016-04-23 00:00:00':'2016-09-04 23:45:00']
season3 = sonic_edit['2016-09-05':'2017-05-02']
piece4 = sonic_edit['2017-05-03 00:00:00':]

season1 = season1[(season1 < 1.62 ) & (season1 > 0.98)]
season2 = season2[(season2 < 3.10 ) & (season2 > 2.45)]
season3 = season3[(season3 < 5.76 ) & (season3 > 5.13)]

all_combined = pd.concat([piece1, season1, piece2, season2, piece3, season3, piece4])

# get rid of weird data in June 2015:
all_combined['2015-06-12 14:45:00':'2015-06-14 00:45:00'] = np.NaN
all_combined['2015-06-14 06:00:00':'2015-06-15 00:45:00'] = np.NaN
all_combined['2015-06-15 08:45:00':'2015-06-15 13:15:00'] = np.NaN

sonic_edit = all_combined
sonic_edit.dropna(inplace=True)

# SMOOTH:
sonic_edit_smooth = savitzky_golay(sonic_edit, 71, 3) # window size (71 * 15min = 17.75h), poly order 3
data = {'sonic_edit_smooth':sonic_edit_smooth}
sonic_edit_smooth = pd.DataFrame(data=data, index=sonic_edit.index)

# Get QAQC variables to common index for export (fill with NaNs):
temp1_reindex = temp1.reindex(index=radiation_corrected.index) #radiation_corrected is longest index
temp2_reindex = temp2.reindex(index=radiation_corrected.index)
sonic_edit_reindex = sonic_edit.reindex(index=radiation_corrected.index)

# Export QAQC Air Temp, Radiation, and Sonic data:
data = {'temp1':temp1_reindex,
        'temp2':temp2_reindex,
        'radiation_corrected':radiation_corrected,
        'sonic_edit':sonic_edit_reindex}

df = pd.DataFrame(data=data, index=radiation_corrected.index)

df.to_csv("BlockSite-met-QAQC.csv", header=True, index=True)

# -----------------------------------------------------------------
# SONIC MELT:
# -----------------------------------------------------------------
# Calculate daily height change from smoothed sonic data:
midnight_index = sonic_edit_smooth.index.indexer_at_time('00:00:00') # returns integer index locations
midnight_height = sonic_edit_smooth.iloc[midnight_index]
# make into Series and rename:
#midnight_height = midnight_height.sonic_splice_smooth.rename('midnight_height')

# height_delta value is assigned to date at the end of the 24-hr period
height_delta = midnight_height.diff()

# shift data such that height_delta value represent melt occuring over that day
# (i.e., value is assigned to date at the beginning of the 24-hr period)
height_delta = height_delta.shift(-1, freq='D')

# get rid of negative values:
height_delta_pos = height_delta[height_delta > 0.]

melt_sonic = height_delta_pos * (910./1000.) # m/day
melt_sonic.dropna(inplace=True)

# make into Series and rename:
melt_sonic = melt_sonic.sonic_edit_smooth.rename('melt_sonic')

## Export data:
#melt_sonic.to_csv("melt_sonic_33km-14.csv", header=True, index=True)

# Reduce to values only where daily ave temp is above freezing:
melt_sonic_abovefreeze = melt_sonic[Td.index]

# make into Series and rename:
melt_sonic_abovefreeze = melt_sonic_abovefreeze.rename('melt_sonic_abovefreeze')
# Export data:
melt_sonic_abovefreeze.to_csv("melt_sonic_33km-14.csv", header=True, index=True)

# -----------------------------------------------------------------
# SIMPLE PDD MELT:
# -----------------------------------------------------------------
# PDD should result in melt with range of ~0.0 - 0.1 m/day (mostly 0.02 - 0.04)

# Range of melt factors (fm) to test:
# Note, Hock (2003) reports range of ~5-20 mm/day/C for Greenland
# This is 0.005 - 0.02 meters/day/C

# Based on initial calculations, need to test even higher values...

fm = np.arange(0.0,0.0301,0.0001)

# Return an ordered dictionary, where each item in the dict
# is a Pandas series, using each value from fm
def calc_PDD(fm, Td):
  d={}
  for x in fm:
    PDD = x * Td
    d["fm{0}".format(x)] = PDD
  od = collections.OrderedDict(sorted(d.items()))
  return od

#PDD_testallfm = calc_PDD(fm, Td)

# If there are NaNs in 'melt_sonic_abovefreeze' (days with no measured melt), drop these days from both.
melt_sonic_abovefreeze_NaNs = melt_sonic_abovefreeze.isnull()
NaNlist_index = melt_sonic_abovefreeze_NaNs[melt_sonic_abovefreeze_NaNs==True].index

melt_sonic_abovefreeze_limited = melt_sonic_abovefreeze.drop(NaNlist_index)
Td_limited = Td.drop(NaNlist_index)
# Now we have two series of equal length (matching indices), to use for optimization

PDD_testallfm_limited = calc_PDD(fm, Td_limited)

#embed()

# Calculate the sum of ((sonic melt - PDD melt)^2) using each PDD.
# This is summing the result of the squared difference (error) for each day.
# Return a dictionary where (key, value) is (fm, sum)
def calc_sumleastsquares(melt_sonic,PDD_ordereddict):
  d={}
  for key, value in PDD_ordereddict.iteritems():
    diff_squared = (melt_sonic - PDD_ordereddict[key])**2.
    sum_diff_squared = diff_squared.sum()
    d["{0}".format(key)] = sum_diff_squared
  od = collections.OrderedDict(sorted(d.items()))
  return od

# Simply get the values out of the ordered dictionary, using the keys as an index:
def get_values(od):
  series = pd.Series()
  for key, value in od.iteritems():
    series[key] = value
  return series
    
sumleastsquares_dict = calc_sumleastsquares(melt_sonic_abovefreeze_limited, PDD_testallfm_limited)

sumleastsquares_values = get_values(sumleastsquares_dict)

# Find the minimum!
min_fm_sum = sumleastsquares_values[sumleastsquares_values==sumleastsquares_values.min()]


print ''
print 'PDD'
print min_fm_sum.index
#-------------------------------
min_fm = 0.0167
#-------------------------------
print 'Minimized fm = %s' %str(min_fm)

# NOTE: this is optimized PDD for all days with ave temp > 0 (NOT using Td_limited):
optimized_PDD = min_fm * Td

# make into Series and rename:
optimized_PDD = optimized_PDD.rename('optimized_PDD')
# Export PDD:
optimized_PDD.to_csv("melt_PDD_33km-14.csv", header=True, index=True)

# Test a different Fm value:
#optimized_PDD_test = 0.027 * Td
#optimized_PDD_test = optimized_PDD_test[optimized_PDD_test < 0.12]

print 'Optimized PDD outliers?'
#print optimized_PDD[optimized_PDD > 0.12]
count = len(optimized_PDD)
# Get rid of obvious outlier:
optimized_PDD = optimized_PDD[optimized_PDD < 0.12]
count2 = len(optimized_PDD)
print 'Dropped %s outlier(s) from optimized_PDD' %str(count-count2)

# For plotting PDD vs. sonic, limit PDD to index of melt_sonic_abovefreeze_limited
# optimized_PDD is 235 days
# melt_sonic_abovefreeze_limited is 225 days
# Therefore, 10 days where PDD predicted melt, but sonic showed no melt (spatial variability?).
# Note, cannot be due to limiting sonic to above freezing, since by definition PDD is only for days above freezing.
#melt_sonic_abovefreeze_matchPDD = melt_sonic_abovefreeze[optimized_PDD.index] # THIS IS NOT RIGHT?? (original implementation)
optimized_PDD_matchsonic = optimized_PDD[melt_sonic_abovefreeze_limited.index]

# Code for producing best-fit line and R^2, using StatsModels:
#d = {'sonic': melt_sonic_abovefreeze_matchPDD, 'PDD': optimized_PDD}
d = {'sonic': melt_sonic_abovefreeze_limited, 'PDD': optimized_PDD_matchsonic}
df = pd.DataFrame(data=d)

# points linearly spaced on sonic
x1 = pd.DataFrame({'sonic': np.linspace(df.sonic.min(), df.sonic.max(), 100)})

# 1st order polynomial
poly_1 = smf.ols(formula='PDD ~ 1 + sonic', data=df).fit()
#--------------------------------------------------------------------------------
## TEST:
## Code for producing best-fit line and R^2, using StatsModels:
#d_test = {'sonic': melt_sonic_abovefreeze_matchPDD, 'PDD': optimized_PDD_test} # This needs to change. See above.
#df_test = pd.DataFrame(data=d_test)

## points linearly spaced on sonic
#x1_test = pd.DataFrame({'sonic': np.linspace(df_test.sonic.min(), df_test.sonic.max(), 100)})

## 1st order polynomial
#poly_1_test = smf.ols(formula='PDD ~ 1 + sonic', data=df_test).fit()

# -----------------------------------------------------------------
# MODIFIED TEMP INDEX (MTI) MELT:
# -----------------------------------------------------------------

# Range of parameter space to explore:
fmx = np.arange(0.0115,0.0125,0.00001) #len 100 (tighter for exact solution)
#fmx = np.arange(0.005,0.0150,0.0001) #len 100 (for plot)
a = np.arange(9.e-08,1.e-05,1.e-07) #len 100
#a = np.arange(0,0.02, (0.02/100.)) # sanity check on bigger values --> floors to zero

rad = radiation_dailytotal_pos
#rad = radiation_dailytotal

# limit rad (len=376) to index for Td (len=235):
rad_limited = rad[Td.index]

# If there are NaNs in 'rad_limited', drop these days from both.
rad_limited_NaNs = rad_limited.isnull()
NaNlist_index = rad_limited_NaNs[rad_limited_NaNs==True].index

rad_limited2 = rad_limited.drop(NaNlist_index)
Td_limited2 = Td.drop(NaNlist_index) # naming is unique from 'Td_limited' for PDD

melt_sonic_abovefreeze_limited2 = melt_sonic_abovefreeze[Td_limited2.index]

# If there are NaNs in 'melt_sonic_abovefreeze_limited2', drop these days from all three.
melt_sonic_abovefreeze_limited_NaNs = melt_sonic_abovefreeze_limited2.isnull()
NaNlist_index = melt_sonic_abovefreeze_limited_NaNs[melt_sonic_abovefreeze_limited_NaNs==True].index

# MODEL INPUTS:
rad_limited3 = rad_limited2.drop(NaNlist_index)
Td_limited3 = Td_limited2.drop(NaNlist_index)
melt_sonic_abovefreeze_limited3 = melt_sonic_abovefreeze_limited2.drop(NaNlist_index)
# Now we have three series of equal length (matching indices), to use for optimization

# -----------------------------------------------------------------
# TURN THE FOLLOWING ON/OFF TO RUN NEW PARAMETERS AND FIND NEW MIN SUM
# Comment out to run script with faster speeds, using defined parameters below.
# This block is also required to generate 3D plots (mti parameter optimization plot)

# Return an ordered dictionary, where each item in the dict
# is a Pandas series, using each possible value combination from 'fmx' and 'a'.
def calc_mti(fmx, a, Td, rad):
  d={}
  d_fmx={}
  d_a={}
  for x in fmx:
    for p in a:
      mti = (x * Td) + (p * rad)
      d[("fmx{0}".format(x), "a{0}".format(p))] = mti
  od = collections.OrderedDict(sorted(d.items()))
  return od

# Calculate the sum of ((sonic melt - mti melt)^2) using each mti.
# To be explicit, this is summing the result of the squared difference (error) for each day.
# Return a dictionary where (key, key, value) is (fmx, a, sum)
def calc_sumleastsquares_mti(melt_sonic, mti_ordereddict):
  d={}
  for key, value in mti_ordereddict.iteritems():
    diff_squared = (melt_sonic - value)**2.
    sum_diff_squared = diff_squared.sum()
    d["{0}".format(key)] = sum_diff_squared
  od = collections.OrderedDict(sorted(d.items()))
  return od

# Simply get the values out of the ordered dictionary, using the keys as an index:
def get_values_mti(od):
  series = pd.Series()
  for key, value in od.iteritems():
    series[key] = value
  return series

# This retrieves the values of 'fmx' and 'a' as used in the parameter optimization,
# with the order of each Series the same as used in calc_mti()
def get_param_series(fmx, a):
  d_fmx={}
  d_a={}
  for x in fmx:
    for p in a:
      d_fmx[("fmx{0}".format(x), "a{0}".format(p))] = x
      d_a[("fmx{0}".format(x), "a{0}".format(p))] = p
  od_fmx = collections.OrderedDict(sorted(d_fmx.items()))
  od_a = collections.OrderedDict(sorted(d_a.items()))
  return od_fmx, od_a
  
mti_testall = calc_mti(fmx, a, Td_limited3, rad_limited3)
sumleastsquares_dict_mti = calc_sumleastsquares_mti(melt_sonic_abovefreeze_limited3, mti_testall)
sumleastsquares_values_mti = get_values_mti(sumleastsquares_dict_mti)
fmx_values_dict, a_values_dict = get_param_series(fmx,a)

# Return the values, without corresponding index:
# (needed for 3D plots)
fmx_values = fmx_values_dict.values()
a_values = a_values_dict.values()

# This not working:
#fmx_vals = get_values_mti(od_fmx)
#a_vals = get_values_mti(od_a)

# Find the minimum!
min_mti_sum = sumleastsquares_values_mti[sumleastsquares_values_mti==sumleastsquares_values_mti.min()]

print ''
print 'MTI'
print min_mti_sum
#print min_mti_sum.index
# -----------------------------------------------------------------

# Enter the actual values below, make sure they match the terminal print statement!!
#-------------------------------
min_fmx = 0.01202
min_a = 5.09e-06
# Using all radiation (includes 8 net-neg days):
# (makes negligible difference...)
#min_fmx = 0.0123
#min_a = 4.79e-06
#-------------------------------
print 'Minimized fmx = %s' %str(min_fmx)
print 'Minimized a = %s' %str(min_a)

# NOTE: this is optimized mti for all days with ave temp > 0 and net rad > 0 (no NaNs):
optimized_mti = (min_fmx * Td_limited2) + (min_a * rad_limited2)

# Sum for summer 2015:
optimized_mti_summer15 = optimized_mti['2015-05-01':'2015-11-01']
# Sum for summer 2016:
optimized_mti_summer16 = optimized_mti['2016-05-01':'2016-11-01']
#embed()

# make into Series and rename:
optimized_mti = optimized_mti.rename('optimized_mti')
# Export MTI:
optimized_mti.to_csv("melt_MTI_33km-14.csv", header=True, index=True)

# NOTE: this is optimized mti for all days with ave temp > 0, net rad > 0, AND that show sonic melt:
# (for correlation plots)
optimized_mti_limited = (min_fmx * Td_limited3) + (min_a * rad_limited3)

# Code for producing best-fit line and R^2, using StatsModels:
d_mti = {'sonic': melt_sonic_abovefreeze_limited3, 'mti': optimized_mti_limited}
df_mti = pd.DataFrame(data=d_mti)

# points linearly spaced on sonic
x1_mti = pd.DataFrame({'sonic': np.linspace(df_mti.sonic.min(), df_mti.sonic.max(), 100)})

# 1st order polynomial
poly_1_mti = smf.ols(formula='mti ~ 1 + sonic', data=df_mti).fit()

# -----------------------------------------------------------------
# COMPARE WITH 'sklearn'
# -----------------------------------------------------------------
X = pd.concat([Td_limited3, rad_limited3], axis=1)  # the input of the model
y = melt_sonic_abovefreeze_limited3  # measurements

estimator = LinearRegression()  # create the estimator object
estimator.fit(X, y)  # optimize the parameters of the model on the measurements
alpha, beta = estimator.coef_  # the obtained parameters

print ''
print 'SKLEARN'
print 'alpha = %s' %str(alpha)
print 'beta = %s' %str(beta)

optimized_mti_skl = (alpha * Td_limited3) + (beta * rad_limited3)

d_skl={'key_skl': optimized_mti_skl}
sumleastsquares_dict_mti_skl = calc_sumleastsquares_mti(melt_sonic_abovefreeze_limited3, d_skl)
sumleastsquares_values_mti_skl = get_values_mti(sumleastsquares_dict_mti_skl)
print 'min sum using skl:'
print sumleastsquares_values_mti_skl[0]

# Code for producing best-fit line and R^2, using StatsModels:
d_mti_skl = {'sonic_skl': melt_sonic_abovefreeze_limited3, 'mti_skl': optimized_mti_skl}
df_mti_skl = pd.DataFrame(data=d_mti_skl)

# points linearly spaced on sonic
x1_mti_skl = pd.DataFrame({'sonic_skl': np.linspace(df_mti_skl.sonic_skl.min(), df_mti_skl.sonic_skl.max(), 100)})

# 1st order polynomial
poly_1_mti_skl = smf.ols(formula='mti_skl ~ 1 + sonic_skl', data=df_mti_skl).fit()

#embed()

# -----------------------------------------------------------------
# PLOTS
# -----------------------------------------------------------------
# SIMPLE PLOTS -- MEASURED VARIABLES:

#AIR TEMP
#fig1 = plt.figure()
#ax1 = fig1.add_subplot(111)
#ax1.plot(temp1.index, temp1, label='temp1 (C)') 
#ax1.plot(temp2.index, temp2, label='temp2 (C)') 
#ax1.set_ylabel('Temp ($^\circ$C)')
#plt.axhline(y=0.0, color='k')
#ax1.set_title('Air Temp - Block Site')
#ax1.legend()
#fig1.tight_layout()  # Make the figure use all available whitespace

# SONIC MELT
#fig4 = plt.figure()
#ax1 = fig4.add_subplot(111)
## align can only be 'center' or 'edge', where 'edge' uses left edge of bar at corresponding index
## This implementation plots bars at midnight, representing melt over following day.
#ax1.bar(melt_sonic.index, melt_sonic, width=1, edgecolor='black', align='center') 
#ax1.xaxis_date()
#ax1.set_ylabel('Sonic Melt (m/day)', fontsize=12)
#ax1.set_title('Sonic Melt', fontsize=14)
#ax1.tick_params(labelsize=12)
##plt.gca().invert_yaxis()
##fig4.tight_layout()

# SONIC -- WITH SMOOTHING LINE
fig3 = plt.figure()
ax1 = fig3.add_subplot(111)
ax1.plot(sonic_edit.index,sonic_edit)
ax1.plot(sonic_edit_smooth.index,sonic_edit_smooth, color='r') 
ax1.set_ylabel('distance to surface (m)', fontsize=12)
ax1.set_title('Sonic - Block Site', fontsize=14)
ax1.tick_params(labelsize=12)
plt.gca().invert_yaxis()
fig3.tight_layout()

# RADIATION
fig7 = plt.figure()
ax1 = fig7.add_subplot(111)
ax1.plot(radiation_corrected.index, radiation_corrected, label='net radiation (W/m^2)') 
ax1.set_ylabel('Net Radiation (W/m^2)', fontsize=12)
ax1.set_title('Net Radiation - Block Site', fontsize=14)
ax1.tick_params(labelsize=12)
fig7.tight_layout()  

# RAIN
#fig8 = plt.figure()
#ax1 = fig8.add_subplot(111)
#ax1.plot(rain.index, rain, label='rain') 
#ax1.set_ylabel('rain (mm)')
##plt.axhline(y=0.0, color='k')
#ax1.set_title('Rain')
#fig8.tight_layout()

# -----------------------------------------------------------------
# OPTIMIZING FACTORS:

# OPTIMIZED FM FACTORS (PDD):
fig100 = plt.figure()
ax1 = fig100.add_subplot(111)
ax1.scatter(fm, sumleastsquares_values, s=2) 
plt.axvline(x=min_fm, color='red')
ax1.set_ylabel(r'sum of squared errors $\sum (sonic_i - PDD_i)^2$')
ax1.set_xlabel(r'$F_m$ value')
ax1.set_xlim(-0.001,0.032)
plt.xticks(np.arange(min(fm), max(fm)+0.0005, 0.005))
ax1.set_title(r'Optimization of $F_m$, Block Site')
fig100.tight_layout()  # Make the figure use all available whitespace

# OPTIMIZED MTI -- 3D error surface:
x = fmx_values
y = a_values
z = sumleastsquares_values_mti

#histoplot(z,'MID (Dec. 15 - March 30)')

d = {'fmx': x, 'a': y, 'sumls': z}
df = pd.DataFrame(data=d)

X = df.fmx
Y = df.a
Z = df.sumls

VecStart_x = min_fmx #temp
VecStart_y = min_a #rad
VecStart_z = np.min(df.sumls)
VecEnd_x = min_fmx
VecEnd_y = min_a
VecEnd_z = 0.12 # arbitrary

VecStart_x_skl = alpha #temp
VecStart_y_skl = beta #rad
VecStart_z_skl = sumleastsquares_values_mti_skl[0]
VecEnd_x_skl = alpha
VecEnd_y_skl = beta
VecEnd_z_skl = 0.12 # arbitrary

# 3D Scatter
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.scatter(X, Y, Z, c='b', marker='.', zorder=0)
ax.plot([VecStart_x,VecEnd_x],[VecStart_y,VecEnd_y],[VecStart_z,VecEnd_z], color='r')
#ax.plot([VecStart_x_skl,VecEnd_x_skl],[VecStart_y_skl,VecEnd_y_skl],[VecStart_z_skl,VecEnd_z_skl], color='b') #min using skl method

ax.set_xlim([np.min(X), np.max(X)])
ax.set_ylim([np.min(Y), np.max(Y)])
ax.set_zlim([np.min(Z), np.max(Z)])

ax.set_xlabel(r'$f_m$', fontsize=12)
ax.set_ylabel(r'$a$', fontsize=12)
ax.set_zlabel(r'sum of squared errors $\sum (sonic_i - mti_i)^2$')
ax.set_title('Parameter Optimization: Modified Temp Index melt, Block Site')

# OPTIMIZED FMX FACTORS (MTI):
fig101 = plt.figure()
ax1 = fig101.add_subplot(111)
ax1.scatter(x, z, s=2) 
#plt.axvline(x=min_z, color='red')
ax1.set_ylabel(r'sum of squared errors $\sum (sonic_i - MTI_i)^2$')
ax1.set_xlabel(r'$f_m$ value')
#ax1.set_xlim(-0.001,0.032)
#plt.xticks(np.arange(min(fm), max(fm)+0.0005, 0.005))
ax1.set_title(r'Optimization of $f_m$: MTI model')
fig101.tight_layout()  # Make the figure use all available whitespace

# OPTIMIZED a FACTORS (MTI):
fig101 = plt.figure()
ax1 = fig101.add_subplot(111)
ax1.scatter(y, z, s=2) 
#plt.axvline(x=min_z, color='red')
ax1.set_ylabel(r'sum of squared errors $\sum (sonic_i - MTI_i)^2$')
ax1.set_xlabel(r'$a$ value')
ax1.set_xlim(0,0.000011)
#plt.xticks(np.arange(min(fm), max(fm)+0.0005, 0.005))
ax1.set_title(r'Optimization of $a$: MTI model')
fig101.tight_layout()  # Make the figure use all available whitespace

# -----------------------------------------------------------------
# RESULTS -- MODEL vs. MEASURE

# SONIC MELT ABOVE FREEZE + OPTIMIZED PDD MELT (BARS)
fig4 = plt.figure()
ax1 = fig4.add_subplot(111)
WIDTH = 0.3
# align can only be 'center' or 'edge', where 'edge' uses left edge of bar at corresponding index
# This implementation plots bars at midnight, representing melt over following day.
ax1.bar(melt_sonic_abovefreeze.index -  pd.DateOffset(days=0.15), melt_sonic_abovefreeze, width=WIDTH, edgecolor='black', align='center', label='sonic')
ax1.bar(optimized_PDD.index +  pd.DateOffset(days=0.15), optimized_PDD, width=WIDTH, edgecolor='black', align='center', label='PDD')
ax1.xaxis_date()
ax1.set_ylabel('Melt (m/day)', fontsize=12)
ax1.set_title('Sonic Melt (above freezing only) and PDD Melt', fontsize=14)
ax1.tick_params(labelsize=12)
plt.legend()
fig4.tight_layout()

# Make continuous and fill missing data with 0. (for line plots)
date_range1 = pd.date_range(melt_sonic_abovefreeze.index.min(), melt_sonic_abovefreeze.index.max())
melt_sonic_insertzeros = melt_sonic_abovefreeze.reindex(date_range1,fill_value=0.)

date_range2 = pd.date_range(optimized_PDD.index.min(), optimized_PDD.index.max())
melt_PDD_insertzeros = optimized_PDD.reindex(date_range2,fill_value=0.)
#melt_PDD_insertzeros_test = optimized_PDD_test.reindex(date_range2,fill_value=0.)

# SONIC MELT ABOVE FREEZE + OPTIMIZED PDD MELT (LINES)
fig40 = plt.figure()
ax1 = fig40.add_subplot(111)
ax1.plot(melt_sonic_insertzeros.index, melt_sonic_insertzeros, label='sonic')
ax1.plot(melt_PDD_insertzeros.index, melt_PDD_insertzeros, label='PDD')
#ax1.plot(melt_PDD_insertzeros_test.index, melt_PDD_insertzeros_test, label='PDD')
ax1.xaxis_date()
ax1.set_ylabel('Melt (m/day)', fontsize=12)
ax1.set_title('Sonic Melt (above freezing only) and PDD Melt - Block Site', fontsize=14)
ax1.tick_params(labelsize=12)
plt.legend()
fig40.tight_layout()

# -----------------------------------------------------------------
# SONIC MELT ABOVE FREEZE + OPTIMIZED MTI MELT (BARS)
fig200 = plt.figure()
ax1 = fig200.add_subplot(111)
WIDTH = 0.3
# align can only be 'center' or 'edge', where 'edge' uses left edge of bar at corresponding index
# This implementation plots bars at midnight, representing melt over following day.
ax1.bar(melt_sonic_abovefreeze.index -  pd.DateOffset(days=0.15), melt_sonic_abovefreeze, width=WIDTH, edgecolor='black', align='center', label='sonic')
ax1.bar(optimized_mti.index +  pd.DateOffset(days=0.15), optimized_mti, width=WIDTH, edgecolor='black', align='center', label='Modified Temp Index')
ax1.xaxis_date()
ax1.set_ylabel('Melt (m/day)', fontsize=12)
ax1.set_title('Sonic Melt (above freezing only) and Modified Temp Index Melt - Block Site', fontsize=14)
ax1.tick_params(labelsize=12)
plt.legend()
fig200.tight_layout()

# Make continuous and fill missing data with 0. (for line plots)
date_range2 = pd.date_range(optimized_mti.index.min(), optimized_mti.index.max())
melt_MTI_insertzeros = optimized_mti.reindex(date_range2,fill_value=0.)

# Make continuous and fill missing data with 0. (for line plots)
date_range3 = pd.date_range(optimized_mti_skl.index.min(), optimized_mti_skl.index.max())
melt_MTI_skl_insertzeros = optimized_mti_skl.reindex(date_range3,fill_value=0.)

# SONIC MELT ABOVE FREEZE + OPTIMIZED MTI MELT (LINES)
fig41 = plt.figure()
ax1 = fig41.add_subplot(111)
ax1.plot(melt_sonic_insertzeros.index, melt_sonic_insertzeros, label='sonic', color='black')
ax1.plot(melt_PDD_insertzeros.index, melt_PDD_insertzeros, label='PDD', color='blue')
ax1.plot(melt_MTI_insertzeros.index, melt_MTI_insertzeros, label='MTI', color='red')
#ax1.plot(melt_MTI_skl_insertzeros.index, melt_MTI_skl_insertzeros, label='MTI - SKL')
ax1.xaxis_date()
ax1.set_ylabel('Melt (m/day)', fontsize=12)
ax1.set_title('Sonic Melt (above freezing only), PDD Melt, & MTI Melt - Block Site', fontsize=14)
ax1.tick_params(labelsize=12)
plt.legend()
fig41.tight_layout()

# -----------------------------------------------------------------
# CORRELATION PLOTS:

#SONIC VS OPTIMIZED PDD -- BEST FIT LINE
fig400 = plt.figure()
ax1 = fig400.add_subplot(111)
#plt.plot(melt_sonic_abovefreeze_matchPDD, optimized_PDD, 'o', alpha=.8, zorder=0, label='')
plt.plot(melt_sonic_abovefreeze_limited, optimized_PDD_matchsonic, 'o', alpha=.8, zorder=0, label='')
plt.plot(x1.sonic, poly_1.predict(x1), 'b-', label='1st order poly fit, $R^2$=%.2f' % poly_1.rsquared, 
         alpha=0.9)
ax1.set_ylabel('optimized PDD melt (m/day)')
ax1.set_xlabel('sonic melt (m/day)')
#ax1.set_xlim()
#plt.xticks()
ax1.set_title('PDD vs. Sonic, Block Site')
plt.legend()
fig400.tight_layout()

# TEST 
#fig401 = plt.figure()
#ax1 = fig401.add_subplot(111)
#plt.plot(melt_sonic_abovefreeze_matchPDD, optimized_PDD_test, 'o', alpha=.8, zorder=0, label='')
#plt.plot(x1_test.sonic, poly_1_test.predict(x1_test), 'b-', label='1st order poly fit, $R^2$=%.2f' % poly_1_test.rsquared, 
         #alpha=0.9)
#ax1.set_ylabel('optimized PDD melt (m/day)')
#ax1.set_xlabel('sonic melt (m/day)')
##ax1.set_xlim()
##plt.xticks()
#ax1.set_title('PDD vs. sonic, Block Site')
#plt.legend()
#fig401.tight_layout()

# SONIC VS OPTIMIZED MTI -- BEST FIT LINE
fig450 = plt.figure()
ax1 = fig450.add_subplot(111)
plt.plot(melt_sonic_abovefreeze_limited3, optimized_mti_limited, 'o', alpha=.8, zorder=0, label='')
plt.plot(x1_mti.sonic, poly_1_mti.predict(x1_mti), 'b-', label='1st order poly fit, $R^2$=%.2f' % poly_1_mti.rsquared, 
         alpha=0.9)
ax1.set_ylabel('optimized mti melt (m/day)')
ax1.set_xlabel('sonic melt (m/day)')
#ax1.set_xlim()
#plt.xticks()
ax1.set_title('Modified Temp Index (MTI) vs. Sonic, Block Site')
plt.legend()
fig450.tight_layout()

# SONIC VS OPTIMIZED MTI -- USING SKL -- BEST FIT LINE
fig500 = plt.figure()
ax1 = fig500.add_subplot(111)
plt.plot(melt_sonic_abovefreeze_limited3, optimized_mti_skl, 'o', alpha=.8, zorder=0, label='')
plt.plot(x1_mti_skl.sonic_skl, poly_1_mti_skl.predict(x1_mti_skl), 'b-', label='1st order poly fit, $R^2$=%.2f' % poly_1_mti_skl.rsquared, 
         alpha=0.9)
ax1.set_ylabel('optimized mti melt -- skl (m/day)')
ax1.set_xlabel('sonic melt (m/day)')
#ax1.set_xlim()
#plt.xticks()
ax1.set_title('Modified Temp Index (MTI) -- SKL vs. Sonic, Block Site')
plt.legend()
fig500.tight_layout()

plt.show()
