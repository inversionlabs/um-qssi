#!/bin/bash
# This will add leading '0' digits for filenames
# Allows catfiles.sh to concatenate in correct order.

last=$(ls -v *.dat | tail -n1) # last file has the biggest number
let max=${#last}-7 # minus the number of all the other chars (constant)

# below grep gets only the number part and head ignores the last line
# which we don't need to change 
ls -v met*.dat | egrep -o '[0-9]+' | head -n -1 | \
while read n; do
    mv -ivf met$n.dat met$(printf "%0"$max"d" $n).dat
done