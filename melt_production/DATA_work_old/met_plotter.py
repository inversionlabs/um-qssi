#!/usr/bin/env python
# Script to import and plot met data
# Patrick Wright
# June, 2015

import numpy as np
import scipy as sp
import pandas as pd
from datetime import datetime
from functools import partial
from IPython import embed
import matplotlib
import matplotlib.pyplot as plt
from math import factorial

def savitzky_golay(y, window_size, order, deriv=0, rate=1):
	try:
		window_size = np.abs(np.int(window_size))
		order = np.abs(np.int(order))
	except ValueError, msg:
		raise ValueError("window_size and order have to be of type int")
	if window_size % 2 != 1 or window_size < 1:
		raise TypeError("window_size size must be a positive odd number")
	if window_size < order + 2:
		raise TypeError("window_size is too small for the polynomials order")
	order_range = range(order+1)
	half_window = (window_size -1) // 2
	# precompute coefficients
	b = np.mat([[k**i for i in order_range] for k in range(-half_window, half_window+1)])
	m = np.linalg.pinv(b).A[deriv] * rate**deriv * factorial(deriv)
	# pad the signal at the extremes with
	# values taken from the signal itself
	firstvals = y[0] - np.abs( y[1:half_window+1][::-1] - y[0] )
	lastvals = y[-1] + np.abs(y[-half_window-1:-1][::-1] - y[-1])
	y = np.concatenate((firstvals, y, lastvals))
	return np.convolve( m[::-1], y, mode='valid')

print "Processing datafiles..."

# Define directory paths:
fpath='fullseries_July15.csv'

# Read in data:
data = pd.read_csv(fpath, sep=',',header=0, low_memory=False)

# Convert datetime string to a Pandas datetime object
timestamp = data['TIMESTAMP']

dt = pd.to_datetime(timestamp, infer_datetime_format=True, errors='coerce')
  
# set index of dataframe to datetime
data_date = data.set_index(dt)

# Apparently these are all individual dataframes:
temp1 = pd.to_numeric(data_date['AirTC_1'], errors='coerce')
temp2 = pd.to_numeric(data_date['AirTC_2'], errors='coerce')
rh1 = pd.to_numeric(data_date['RH_1'], errors='coerce')
rh2 = pd.to_numeric(data_date['RH_2'], errors='coerce')
sonic = pd.to_numeric(data_date['TCDT'], errors='coerce')
windspeed = pd.to_numeric(data_date['WS_ms'], errors='coerce')
winddir = pd.to_numeric(data_date['WindDir'], errors='coerce')
radiation = pd.to_numeric(data_date['CNR_Wm2'], errors='coerce')
rain = pd.to_numeric(data_date['Rain_mm_Tot'], errors='coerce') # need to calc daily totals (m)
battV = pd.to_numeric(data_date['BattV'], errors='coerce')
paneltemp = pd.to_numeric(data_date['PTemp_C'], errors='coerce')
ducer1 = pd.to_numeric(data_date['Ducer_1'], errors='coerce')
ducer2 = pd.to_numeric(data_date['Ducer_2'], errors='coerce')

# -----------------------------------------------------------------
# SONIC:
# -----------------------------------------------------------------

# Index to first melt season, git rid of outliers. QUICK AND DIRTY.
sonic_splice1 = sonic[:'2014-08-01 00:00:00']
sonic_splice1_up = sonic_splice1 - 0.729 # does this value come from the difference between consecutive data points before/after station lower?
# The splice is definitely missing a full day of melt
sonic_after = sonic['2014-08-01 21:00:00':]
sonic_splice = pd.concat([sonic_splice1_up, sonic_after], axis=0)
sonic_splice_melt1 = sonic_splice[:'2014-11-01 00:00:00']

# get rid of obvious outliers:
sonic_splice_melt1 = sonic_splice_melt1[(sonic_splice_melt1 < 2.05 ) & (sonic_splice_melt1 > 0.38)]

sonic_splice_smooth = savitzky_golay(sonic_splice_melt1, 71, 3) # how were the smoothing parameters chosen?
data = {'sonic_splice_smooth':sonic_splice_smooth}
sonic_splice_smooth = pd.DataFrame(data=data, index=sonic_splice_melt1.index)

# -----------------------------------------------------------------
# SONIC MELT:
# -----------------------------------------------------------------
# Calculate daily height change from smoothed sonic data:
midnight_index = sonic_splice_smooth.index.indexer_at_time('00:00:00') # returns integer index locations
midnight_height = sonic_splice_smooth.iloc[midnight_index]
# make into Series and rename:
#midnight_height = midnight_height.sonic_splice_smooth.rename('midnight_height')

# height_delta value is assigned to date at the end of the 24-hr period
height_delta = midnight_height.diff()
# get rid of negative values:
height_delta_pos = height_delta[height_delta > 0.]

melt_sonic = height_delta_pos * (910./1000.) # m/day
melt_sonic.dropna(inplace=True)
# make into Series and rename:
melt_sonic = melt_sonic.sonic_splice_smooth.rename('melt_sonic')

# -----------------------------------------------------------------
# RADIATION:
# -----------------------------------------------------------------

# Apply correct MULT value:
radiation_corrected = (radiation / 1000.) * 76.34

# -----------------------------------------------------------------

#embed()

# AIR TEMP
#fig1 = plt.figure()
#ax1 = fig1.add_subplot(111)
#ax1.plot(temp1.index, temp1, label='temp1 (C)') 
#ax1.plot(temp2.index, temp2, label='temp2 (C)') 
#ax1.set_ylabel('Temp(C)')
#plt.axhline(y=0.0, color='k')
#ax1.set_title('Air Temp')
#ax1.legend()
#fig1.tight_layout()  # Make the figure use all available whitespace

# AIR TEMP + SONIC
#fig2 = plt.figure()
#ax1 = fig2.add_subplot(111)
#ax1.plot(sonic.index,sonic) 
#plt.gca().invert_yaxis()
#ax1.set_ylabel('distance to surface (m)')
#ax1.set_title('Temp and Sonic')
#ax2 = ax1.twinx()
#ax2.plot(temp1.index, temp1, color='r') 
#ax2.set_ylabel('temp (C)')
#fig2.tight_layout()

# SONIC
fig3 = plt.figure()
ax1 = fig3.add_subplot(111)
ax1.plot(sonic_splice.index,sonic_splice) 
ax1.plot(sonic_splice_smooth.index,sonic_splice_smooth, color='r') 
#ax1.plot(sonic.index,sonic)
ax1.set_ylabel('distance to surface (m)', fontsize=12)
ax1.set_title('Sonic', fontsize=14)
ax1.tick_params(labelsize=12)
plt.gca().invert_yaxis()
fig3.tight_layout()

fig4 = plt.figure()
ax1 = fig4.add_subplot(111)
# align can only be 'center' or 'edge', where 'edge' uses left edge of bar at corresponding index
# if you want bars to plot for preceeding day, need to change input data
# This implementation plots bars at midnight, representing melt over previous day.
ax1.bar(melt_sonic.index, melt_sonic, width=1, edgecolor='black', align='center') 
ax1.xaxis_date()

ax1.set_ylabel('Sonic Melt (m/day)', fontsize=12)
ax1.set_title('Sonic Melt', fontsize=14)
ax1.tick_params(labelsize=12)
#plt.gca().invert_yaxis()
fig4.tight_layout()

## SONIC + WIND
#fig4 = plt.figure()
#ax1 = fig4.add_subplot(111)
#ax1.plot(sonic.index,sonic)
#plt.gca().invert_yaxis()
#ax1.set_ylabel('distance to surface (m)')
#ax1.set_title('Wind')
#ax1.set_title('Wind and Sonic')
#ax2 = ax1.twinx()
#ax2.plot(windspeed.index, windspeed, color='k', label='wind speed (m/s)') 
#ax2.set_ylabel('wind speed (m/s)')
#ax2.set_ylim(-10,20)
#fig4.tight_layout()  

# WIND
#fig5 = plt.figure()
#ax1 = fig5.add_subplot(111)
#ax1.plot(windspeed.index, windspeed, color='k', label='wind speed (m/s)') 
#ax1.set_title('Windspeed')
#ax1.set_ylabel('wind speed (m/s)')
#ax1.set_ylim(-10,20)
#fig5.tight_layout()  

## WIND DIRECTION
#fig6 = plt.figure()
#ax1 = fig6.add_subplot(111)
#ax1.plot(winddir.index,winddir) 
#ax1.set_ylabel('wind direction (degrees)')
#ax1.set_title('Wind Direction')
#fig6.tight_layout()  

# RAD
fig7 = plt.figure()
ax1 = fig7.add_subplot(111)
ax1.plot(radiation_corrected.index, radiation_corrected, label='net radiation (W/m^2)') 
ax1.set_ylabel('Net Radiation (W/m^2)', fontsize=12)
ax1.set_title('Net Radiation', fontsize=14)
ax1.tick_params(labelsize=12)
fig7.tight_layout()  

# RAIN
#fig8 = plt.figure()
#ax1 = fig8.add_subplot(111)
#ax1.plot(rain.index, rain, label='rain') 
#ax1.set_ylabel('rain (mm)')
##plt.axhline(y=0.0, color='k')
#ax1.set_title('Rain')
#fig8.tight_layout()

# Batt Voltage
#fig9 = plt.figure()
#ax1 = fig9.add_subplot(111)
#ax1.plot(battV.index, battV, label='Battery Voltage') 
#ax1.set_ylabel('Volts')
##plt.axhline(y=0.0, color='k')
#ax1.set_title('Battery Voltage')
#fig9.tight_layout()  

# Panel Temp
#fig10 = plt.figure()
#ax1 = fig10.add_subplot(111)
#ax1.plot(paneltemp.index, paneltemp, label='Panel Temp') 
#ax1.set_ylabel('Temp (C)')
##plt.axhline(y=0.0, color='k')
#ax1.set_title('Panel Temp')
#fig10.tight_layout()  

# Ducers
#fig11 = plt.figure()
#ax1 = fig11.add_subplot(111)
#ax1.plot(ducer1.index, ducer1, label='Ducer 1') 
#ax1.plot(ducer2.index, ducer2, label='Ducer 2') 
#ax1.set_ylabel('')
##plt.axhline(y=0.0, color='k')
#ax1.set_title('Ducers')
#fig11.tight_layout() 

plt.show()

#data.to_csv('met_data.csv',columns= [], header=True, index=False)
