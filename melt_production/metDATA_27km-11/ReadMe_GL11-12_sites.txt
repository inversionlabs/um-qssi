ReadMe_GL11-12_sites.txt
Created 10/20/2017
Patrick Wright
Inversion Labs

10/20/2017
---------
Sourcing data for sites GL11-1 (27km-11), GL11-2 (46km-11), and GL12-2 (27km-12)
from 'Our_Field_DATA/Meteorological_Data/Processed Met on W. GL time'.

Files are:
M_GL11_1.txt
M_GL11_2.txt
M_GL12_2.txt

The ReadMe in this directory states:
"These meteorology datasets are from the final GAP report so they are fully processed
and on Western Greenland Time."

Each file contains:

Date (Day of year)
Temperature (C)
Relative Ice Height (m)
Shortwave Radiation (Wm-2)


