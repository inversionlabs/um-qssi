#!/usr/bin/env python
# Script for calculating melt totals at GrIS sites.
# Using PROMICE stations, adiabatic lapse rate, and PDD

# Patrick Wright
# Inversion Labs
# April, 2018

import numpy as np
import scipy as sp
import pandas as pd
from datetime import datetime, timedelta
from IPython import embed
import matplotlib
import matplotlib.pyplot as plt

#-------------------------------------------------------------------------------
print "Processing PROMICE datafiles..."

# Define directory paths:
KAN_L_path = 'promice_data_20180417-112408/KAN_L_day.txt'
KAN_M_path = 'promice_data_20180417-112408/KAN_M_day.txt'

# Read in data:
KAN_L = pd.read_csv(KAN_L_path, sep=' ', skiprows=1, header=None)
KAN_M = pd.read_csv(KAN_M_path, sep=' ', skiprows=1, header=None)

# Define datetime from year, month, day columns:
datetime_df_KAN_L = pd.DataFrame({'year': KAN_L[0],
                            'month': KAN_L[1],
                            'day': KAN_L[2]})
datetime_df_KAN_M = pd.DataFrame({'year': KAN_M[0],
                            'month': KAN_M[1],
                            'day': KAN_M[2]})

KAN_L_datetime = pd.to_datetime(datetime_df_KAN_L)
KAN_M_datetime = pd.to_datetime(datetime_df_KAN_M)

# Define daily ave temp column:
dailyavetemp_KAN_L = KAN_L[6]
dailyavetemp_KAN_M = KAN_M[6]

# Make a new DataFrame with just datetime and air temp:
df_KAN_L = pd.DataFrame({'datetime':KAN_L_datetime, 'temp':dailyavetemp_KAN_L})
df_KAN_M = pd.DataFrame({'datetime':KAN_M_datetime, 'temp':dailyavetemp_KAN_M})

# set index of dataframe to datetime and drop redundant column:
df_KAN_L = df_KAN_L.set_index(df_KAN_L.datetime)
df_KAN_L.index.rename('Datetime', inplace=True)
df_KAN_L.drop('datetime', axis=1, inplace=True)

df_KAN_M = df_KAN_M.set_index(df_KAN_M.datetime)
df_KAN_M.index.rename('Datetime', inplace=True)
df_KAN_M.drop('datetime', axis=1, inplace=True)

# Change -999 values to NaN:
df_KAN_L.temp = df_KAN_L.temp[df_KAN_L.temp > -100]
df_KAN_M.temp = df_KAN_M.temp[df_KAN_M.temp > -100]

print " "
print "Calculating PDD for stations..."

#-------------------------------------------------------------------------------
# 27km-11
#-------------------------------------------------------------------------------
# Adjust for elevation using adiabatic lapse rate:
site27km_11 = df_KAN_L - (0.006277 * 179) # 1.12 C colder

# Get only positive values:
site27km_11 = site27km_11[site27km_11 > 0.0]

# Index for the 2011 melt season:
site27km_11 = site27km_11['2011-01-01':'2011-12-31']

# NOTE: this is optimized PDD for all days with ave temp > 0:
optimized_PDD_site27km_11 = 0.0115 * site27km_11.temp

# make into Series and rename:
optimized_PDD_site27km_11 = optimized_PDD_site27km_11.rename('optimized_PDD_site27km_11')

# Export PDD:
#optimized_PDD_site27km_11.to_csv("PROMICEmelt_27km-11.csv", header=True, index=True)

# Graph it::
#fig1 = plt.figure()
#ax1 = fig1.add_subplot(111)
#WIDTH = 0.3
## align can only be 'center' or 'edge', where 'edge' uses left edge of bar at corresponding index
## This implementation plots bars at midnight, representing melt over following day.
#ax1.bar(optimized_PDD_site27km_11.index, optimized_PDD_site27km_11, width=WIDTH, edgecolor='black', align='center', label='PDD 27km-11')
#ax1.xaxis_date()
#ax1.set_ylabel('Melt (m/day)', fontsize=12)
#ax1.set_title('PROMICE-derived PDD Melt - Site 27km-11', fontsize=14)
#ax1.tick_params(labelsize=12)
##plt.legend()
#fig1.tight_layout()
#plt.show()

# Get rid of outlier November melt day:
optimized_PDD_site27km_11 = optimized_PDD_site27km_11[:'2011-11-01']

# Melt total for season:
melt_total_site27km_11 = optimized_PDD_site27km_11.sum()
print " "
print 'melt total (2011), site 27km-11: %s' % melt_total_site27km_11

#-------------------------------------------------------------------------------
# 46km-11
#-------------------------------------------------------------------------------
# Adjust for elevation using adiabatic lapse rate:
site46km_11 = df_KAN_M + (0.006277 * 179) # 1.12 C warmer

# Get only positive values:
site46km_11 = site46km_11[site46km_11 > 0.0]

# Index for the 2011 and 2012 melt seasons:
site46km_11 = site46km_11['2011-01-01':'2012-12-31']

# NOTE: this is optimized PDD for all days with ave temp > 0:
optimized_PDD_site46km_11 = 0.0157 * site46km_11.temp

# make into Series and rename:
optimized_PDD_site46km_11 = optimized_PDD_site46km_11.rename('optimized_PDD_site46km_11')

# Export PDD:
#optimized_PDD_site46km_11.to_csv("PROMICEmelt_46km-11.csv", header=True, index=True)

# Graph it::
#fig1 = plt.figure()
#ax1 = fig1.add_subplot(111)
#WIDTH = 0.3
## align can only be 'center' or 'edge', where 'edge' uses left edge of bar at corresponding index
## This implementation plots bars at midnight, representing melt over following day.
#ax1.bar(optimized_PDD_site46km_11.index, optimized_PDD_site46km_11, width=WIDTH, edgecolor='black', align='center', label='PDD 46km-11')
#ax1.xaxis_date()
#ax1.set_ylabel('Melt (m/day)', fontsize=12)
#ax1.set_title('PROMICE-derived PDD Melt - Site 46km-11', fontsize=14)
#ax1.tick_params(labelsize=12)
##plt.legend()
#fig1.tight_layout()
#plt.show()

# Melt total for 2011 season:
optimized_PDD_site46km_11_2011 = optimized_PDD_site46km_11['2011-05-01':'2011-10-01']
melt_total_site46km_11_2011 = optimized_PDD_site46km_11_2011.sum()
print " "
print 'melt total (2011), site 46km-11: %s' % melt_total_site46km_11_2011

# Melt total for 2012 season:
optimized_PDD_site46km_11_2012 = optimized_PDD_site46km_11['2012-03-01':'2012-11-01']
melt_total_site46km_11_2012 = optimized_PDD_site46km_11_2012.sum()
print " "
print 'melt total (2012), site 46km-11: %s' % melt_total_site46km_11_2012

#-------------------------------------------------------------------------------
# 33km-14, using KAN_M
#-------------------------------------------------------------------------------
# Adjust for elevation using adiabatic lapse rate:
#site33km_14 = df_KAN_M + (0.006277 * 291) # 1.83 C warmer
site33km_14 = df_KAN_M + (0.00921 * 291) # 2.68 C warmer

# Get only positive values:
site33km_14 = site33km_14[site33km_14 > 0.0]

# Index for the 2014 melt season:
site33km_14 = site33km_14['2014-01-01':'2014-12-31']

# NOTE: this is optimized PDD for all days with ave temp > 0:
optimized_PDD_site33km_14 = 0.0167 * site33km_14.temp

# make into Series and rename:
optimized_PDD_site33km_14 = optimized_PDD_site33km_14.rename('optimized_PDD_site33km_14')

# Export PDD:
#optimized_PDD_site33km_14.to_csv("PROMICEmelt_33km-14.csv", header=True, index=True)

# Graph it::
#fig1 = plt.figure()
#ax1 = fig1.add_subplot(111)
#WIDTH = 0.3
## align can only be 'center' or 'edge', where 'edge' uses left edge of bar at corresponding index
## This implementation plots bars at midnight, representing melt over following day.
#ax1.bar(optimized_PDD_site33km_14.index, optimized_PDD_site33km_14, width=WIDTH, edgecolor='black', align='center', label='PDD 33km-14')
#ax1.xaxis_date()
#ax1.set_ylabel('Melt (m/day)', fontsize=12)
#ax1.set_title('PROMICE-derived PDD Melt - Site 33km-14', fontsize=14)
#ax1.tick_params(labelsize=12)
##plt.legend()
#fig1.tight_layout()
#plt.show()

# Melt total for 2014 season:
melt_total_site33km_14_KAN_M = optimized_PDD_site33km_14.sum()
print " "
print 'melt total (2014), site 33km-14 (using KAN_M): %s' % melt_total_site33km_14_KAN_M

#-------------------------------------------------------------------------------
# 33km-14, using KAN_L
#-------------------------------------------------------------------------------
# Adjust for elevation using adiabatic lapse rate:
#site33km_14 = df_KAN_L - (0.006277 * 309) # 1.94 C colder
site33km_14 = df_KAN_L - (0.00921 * 309) # 2.85 C colder

# Get only positive values:
site33km_14 = site33km_14[site33km_14 > 0.0]

# Index for the 2014 melt season:
site33km_14 = site33km_14['2014-01-01':'2014-12-31']

# NOTE: this is optimized PDD for all days with ave temp > 0:
optimized_PDD_site33km_14 = 0.0167 * site33km_14.temp

# make into Series and rename:
optimized_PDD_site33km_14 = optimized_PDD_site33km_14.rename('optimized_PDD_site33km_14')

# Export PDD:
#optimized_PDD_site33km_14.to_csv("PROMICEmelt_33km-14.csv", header=True, index=True)

# Graph it::
#fig1 = plt.figure()
#ax1 = fig1.add_subplot(111)
#WIDTH = 0.3
## align can only be 'center' or 'edge', where 'edge' uses left edge of bar at corresponding index
## This implementation plots bars at midnight, representing melt over following day.
#ax1.bar(optimized_PDD_site33km_14.index, optimized_PDD_site33km_14, width=WIDTH, edgecolor='black', align='center', label='PDD 33km-14')
#ax1.xaxis_date()
#ax1.set_ylabel('Melt (m/day)', fontsize=12)
#ax1.set_title('PROMICE-derived PDD Melt - Site 33km-14', fontsize=14)
#ax1.tick_params(labelsize=12)
##plt.legend()
#fig1.tight_layout()
#plt.show()

# Melt total for 2014 season:
melt_total_site33km_14_KAN_L = optimized_PDD_site33km_14.sum()
print " "
print 'melt total (2014), site 33km-14 (using KAN_L): %s' % melt_total_site33km_14_KAN_L

