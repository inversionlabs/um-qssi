#!/usr/bin/env python

"""
Analysis of Fall 2012 GPS, pressure, and melt
Patrick Wright, Inversion Labs
Job: UM QSSI
Jan. 2018
"""

import datetime as dt
import numpy as np
import scipy as sp
import pandas as pd
import matplotlib
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from matplotlib.dates import date2num
#from matplotlib.ticker import MultipleLocator, FormatStrFormatter
from matplotlib import rcParams
from dateutil.relativedelta import relativedelta
import pylab
from IPython import embed
from math import factorial
from datetime import timedelta

# Convert DOY to datetime:
def dateparser_2012(doy):
  return dt.datetime(year=2012, month=1, day=1) + dt.timedelta(days=doy) - dt.timedelta(days=1)

# set index of dataframes to datetime:
def set_index(df):
  index = pd.to_datetime(df.Datetime)
  df = df.set_index(index)
  df.index.rename('Datetime_index', inplace=True)
  df = df.drop('Datetime', axis=1)
  return df

# ------------------------------------------------------------------------------
# VELOCITY IMPORTS:
# ------------------------------------------------------------------------------

vel1 = pd.read_csv('velocity/processed/27km-12_velocity.csv', sep=',', header=0, names=['timestamp','DOY','velocity_myear'])
vel1_index = pd.to_datetime(vel1['timestamp'])
vel1_index.name = 'datetimeindex'
vel1 = vel1.set_index(vel1_index)
vel1 = vel1.drop('timestamp', axis=1)

# drop suspect periods:
vel1['2012-09-07 11:00':'2012-09-08 09:00'] = np.NaN
vel1['2012-09-18 11:00': ] = np.NaN


vel2 = pd.read_csv('velocity/processed/27km-11_velocity.csv', sep=',', header=0, names=['timestamp','DOY','velocity_myear'])
vel2_index = pd.to_datetime(vel2['timestamp'])
vel2_index.name = 'datetimeindex'
vel2 = vel2.set_index(vel2_index)
vel2 = vel2.drop('timestamp', axis=1)

# ------------------------------------------------------------------------------
# PRESSURE IMPORTS:
# ------------------------------------------------------------------------------

# NOTE: These pressure data are from '~/UM_RESEARCH/water_pressure/Pressure_data_PJWpublication'
#       (this directory is replicated on Box)
# NOTE: No resample / interpolate.... just for viewing trends. Data could have uneven sampling.

GL12_2A = pd.read_csv('pressure/GL12-2A.txt', sep='	', header=None, names=['DOY', 'm_h2o'])
GL12_2A_index = GL12_2A['DOY'].map(dateparser_2012) # make a datetime index
GL12_2A_index.name = 'timestamp' # change name of index
GL12_2A_dt = GL12_2A.set_index(GL12_2A_index) # set index to datetime, re-name
GL12_2A_scaled = (9.81 * 1000. * GL12_2A_dt.m_h2o)  / (910. * 9.81 * 695.5) # pgh water column / pgh ice
GL12_2A_scaled.name = 'scaled' # change name of series

GL12_2C = pd.read_csv('pressure/GL12-2C.txt', sep=' ',skipinitialspace=True, header=None, names=['DOY', 'm_h2o'])
GL12_2C_index = GL12_2C['DOY'].map(dateparser_2012) # make a datetime index
GL12_2C_index.name = 'timestamp' # change name of index
GL12_2C_dt = GL12_2C.set_index(GL12_2C_index) # set index to datetime, re-name
GL12_2C_scaled = (9.81 * 1000. * GL12_2C_dt.m_h2o)  / (910. * 9.81 * 688.) # pgh water column / pgh ice
GL12_2C_scaled.name = 'scaled' # change name of series

GL12_2D = pd.read_csv('pressure/GL12-2D.txt', sep=' ',skipinitialspace=True, header=None, names=['DOY', 'm_h2o'])
GL12_2D_index = GL12_2D['DOY'].map(dateparser_2012) # make a datetime index
GL12_2D_index.name = 'timestamp' # change name of index
GL12_2D_dt = GL12_2D.set_index(GL12_2D_index) # set index to datetime, re-name
GL12_2D_scaled = (9.81 * 1000. * GL12_2D_dt.m_h2o)  / (910. * 9.81 * 696.) # pgh water column / pgh ice
GL12_2D_scaled.name = 'scaled' # change name of series

# ------------------------------------------------------------------------------
# MELT PRODUCTION IMPORTS: NOTE: Files replaced on 12/1/17
# ------------------------------------------------------------------------------
GL11_1_melt = pd.read_csv('melt/melt_MTI_27km-11.csv', sep=',', header=0, names=['Datetime', 'optimized_mti'])
GL11_1_melt_index = pd.to_datetime(GL11_1_melt['Datetime'])
GL11_1_melt_index.name = 'timestamp'
GL11_1_melt = GL11_1_melt.set_index(GL11_1_melt_index)
GL11_1_melt = GL11_1_melt.drop('Datetime', axis=1)

GL12_2_melt = pd.read_csv('melt/melt_MTI_27km-12.csv', sep=',', header=0, names=['Datetime', 'optimized_mti'])
GL12_2_melt_index = pd.to_datetime(GL12_2_melt['Datetime'])
GL12_2_melt_index.name = 'timestamp'
GL12_2_melt = GL12_2_melt.set_index(GL12_2_melt_index)
GL12_2_melt = GL12_2_melt.drop('Datetime', axis=1)

# ------------------------------------------------------------------------------
# PLOTS:
# ------------------------------------------------------------------------------

# MULTI-PANEL: velocity, pressure, melt

fig = plt.figure(figsize=(10,10))

ax1 = plt.subplot2grid((3, 1), (0, 0)) # GPS 
ax2 = plt.subplot2grid((3, 1), (1, 0)) # water pressure 
ax3 = plt.subplot2grid((3, 1), (2, 0)) # melt production

ax1.plot(vel1.index, vel1.velocity_myear, color='r', label='27km-12')
ax1.plot(vel2.index, vel2.velocity_myear, color='b', label='27km-11')
ax1.set_xlim('2012-08-24','2012-09-25')
ax1.set_ylim(45,225)
ax1.set_ylabel('velocity (m/yr)')
ax1.tick_params(axis='x', labelbottom='off')

#plt.setp(ax1.get_yticklabels()[0], visible=False)
#plt.setp(ax1.get_yticklabels()[-1], visible=False)
#plt.setp(ax1.get_yticklabels()[2], visible=False)
#plt.setp(ax1.get_yticklabels()[4], visible=False)
#plt.setp(ax1.get_yticklabels()[6], visible=False)

#ax1.yaxis.set_tick_params(labelsize=12)
ax1.legend(loc='upper right')
ax1.grid(color='black', alpha=0.3, linestyle='-', linewidth=1)
ax1.annotate('GPS Velocity', (mdates.datestr2num('2012-08-25'), 205), style='oblique')


ax2.plot(GL12_2D_dt.index, GL12_2D_scaled, color='r', alpha=0.95, label='27km-12D', linewidth=1)
ax2.plot(GL12_2C_dt.index, GL12_2C_scaled, color='r', alpha=0.65, label='27km-12C', linewidth=1)
ax2.plot(GL12_2A_dt.index, GL12_2A_scaled, color='r', alpha=0.4, label='27km-12A', linewidth=1)

ax2.set_xlim('2012-08-24','2012-09-25')
ax2.tick_params(axis='x', labelbottom='off')
ax2.set_ylim(0.85,1.08)
#ax2.set_ylim(0.935,1.01)
ax2.set_ylabel('pressure (fract OB)')

plt.setp(ax2.get_yticklabels()[0], visible=False)
#plt.setp(ax2.get_yticklabels()[-1], visible=False)
#plt.setp(ax2.get_yticklabels()[1], visible=False)
#plt.setp(ax2.get_yticklabels()[3], visible=False)
#plt.setp(ax2.get_yticklabels()[5], visible=False)

#ax2.yaxis.set_tick_params(labelsize=12)
ax2.legend(loc='lower right')
ax2.grid(color='black', alpha=0.3, linestyle='-', linewidth=1)
ax2.annotate('Water Pressure', (mdates.datestr2num('2012-08-25'), 1.06), style='oblique')

WIDTH = 0.3
# align can only be 'center' or 'edge', where 'edge' uses left edge of bar at corresponding index
# This implementation plots bars at midnight, representing melt over following day.
ax3.bar(GL12_2_melt.index +  pd.DateOffset(days=0.15), GL12_2_melt.optimized_mti,
        width=WIDTH, edgecolor='black', color='red', align='center', label='27km-12')
ax3.bar(GL11_1_melt.index -  pd.DateOffset(days=0.15), GL11_1_melt.optimized_mti,
        width=WIDTH, edgecolor='black', color='blue', align='center', label='27km-11')
ax3.xaxis_date()
ax3.set_ylabel('melt (m/day)')
ax3.set_xlim('2012-08-24','2012-09-25')
ax3.set_ylim(0,0.040)
#ax3.tick_params(axis='x', labelbottom='off')

#plt.setp(ax3.get_yticklabels()[0], visible=False)
#plt.setp(ax3.get_yticklabels()[-1], visible=False)
#plt.setp(ax3.get_yticklabels()[2], visible=False)
#plt.setp(ax3.get_yticklabels()[4], visible=False)
#plt.setp(ax3.get_yticklabels()[5], visible=False)

ax3.legend(loc='upper right')
ax3.grid(color='black', alpha=0.3, linestyle='-', linewidth=1)
ax3.annotate('Melt Production (MTI)', (mdates.datestr2num('2012-08-25'), 0.037), style='oblique')

plt.subplots_adjust(wspace=0.001, hspace=0.001)
plt.subplots_adjust(
  left=0.085, right=0.95,
  top=0.95, bottom=0.13)

#plt.legend()

plt.xticks(rotation=90, fontsize=10)

#plt.savefig('meltvelocity.svg', format='svg')

plt.show()