#!/usr/bin/env python

"""
GPS post-processing (velocity calculations)
Patrick Wright, Inversion Labs
adapted from Joel Brown, Aesir Consulting
Job: UM QSSI
Dec. 2017
"""

import datetime as dt
from datetime import timedelta
import numpy as np
import scipy as sp
from scipy import stats
from scipy.interpolate import interp1d
import pandas as pd
import matplotlib
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from matplotlib.dates import date2num
from matplotlib import rcParams
#from matplotlib.ticker import MultipleLocator, FormatStrFormatter
from dateutil.relativedelta import relativedelta
import pylab
from math import factorial

from IPython import embed

# ------------------------------------------------------------------------------
# DEFINE FILE PATH AND SITE NAME FOR POSITION DATA:

filepath = "GPS_Pressure_Data_Organized/GL11_1/GL11-1_GPS.txt"
site = '27km-11'

# ------------------------------------------------------------------------------
# DEFINE FUNCTIONS:

def nan_helper(y):
    """Helper to handle indices and logical indices of NaNs.
    From accepted answer here: 
    https://stackoverflow.com/questions/6518811/interpolate-nan-values-in-a-numpy-array

    Input:
        - y, 1d numpy array with possible NaNs
    Output:
        - nans, logical indices of NaNs
        - index, a function, with signature indices= index(logical_indices),
          to convert logical indices of NaNs to 'equivalent' indices
    """
    return np.isnan(y), lambda z: z.nonzero()[0]
  
def drop_outliers_north(split_arrays, graph_add):
  step=1
  new_array_list=[]
  for arr in split_arrays:
    step = step*-1
    if step == -1:
      color = 'r'
    else:
      color = 'b'
    x = arr[:,0]
    y = arr[:,1]
    slope, intercept, r_value, p_value, std_err = stats.linregress(x,y)
    linear_fit = slope * x + intercept
    if graph_add == 'yes':
      ax.plot(x, linear_fit, color=color) 
    
    arr = np.insert(arr, 7, linear_fit, axis=1) # add the linear regression results to the array
    
    #arr = arr[(np.absolute(arr[:,1] - arr[:,7])) < 0.02] # DROP OUTLIERS
    arr[:,1:3][(np.absolute(arr[:,1] - arr[:,7])) > 0.02] = np.NaN # OUTLIERS --> NaN
    
    new_array_list.append(arr)
    
  return new_array_list

def drop_outliers_east(split_arrays):
  step=1
  new_array_list=[]
  for arr in split_arrays:
    step = step*-1
    if step == -1:
      color = 'r'
    else:
      color = 'b'
    x = arr[:,0]
    y = arr[:,2]
    slope, intercept, r_value, p_value, std_err = stats.linregress(x,y)
    linear_fit = slope * x + intercept
    
    arr = np.insert(arr, 8, linear_fit, axis=1) # add the linear regression results to the array
    
    #arr = arr[(np.absolute(arr[:,1] - arr[:,7])) < 0.02] # DROP OUTLIERS
    arr[:,1:3][(np.absolute(arr[:,2] - arr[:,8])) > 0.07] = np.NaN # OUTLIERS --> NaN
    
    new_array_list.append(arr)
    
  return new_array_list

def lin_interp(pos_data):
  y1= pos_data[:,1]
  nans, x= nan_helper(y1)
  y1[nans]= np.interp(x(nans), x(~nans), y1[~nans])

  y2= pos_data[:,2]
  nans, x= nan_helper(y2)
  y2[nans]= np.interp(x(nans), x(~nans), y2[~nans])

  pos_data[:,1] = y1 # North
  pos_data[:,2] = y2 # East
  
  return pos_data
  
# Convert DOY to datetime (2011):
def dateparser_2011(doy):
  return dt.datetime(year=2011, month=1, day=1) + dt.timedelta(days=doy) - dt.timedelta(days=1)

# set index of Pandas dataframe to datetime:
def set_index(df):
  index = pd.to_datetime(df.Datetime)
  df = df.set_index(index)
  df.index.rename('Datetime_index', inplace=True)
  df = df.drop('Datetime', axis=1)
  return df

# Make a histogram:
def histoplot(data, num_bins, titlestring):
    fig1 = plt.figure(figsize=(6*1.618, 6))
    ax1 = fig1.add_subplot(111)
    n, bins, patches = plt.hist(data, num_bins, facecolor='blue', edgecolor='k', alpha=0.5)
    ax1.set_title(titlestring, fontsize=12)
    ax1.set_xlabel('error', fontsize=12)
    ax1.set_ylabel('samples', fontsize=12)
    ax1.set_xlim(0,0.1)
    plt.xticks(fontsize=12)
    plt.yticks(fontsize=12)

#-------------------------------------------------------------------------------
# Joel Brown's loading / binning / smoothing / velocity functions:

""" Define data read and write functions """
    
def loadGPSdata(filenam):
    with open(filenam,"r") as f:
        pData=np.loadtxt(f, skiprows=1,delimiter=',')
        return pData

""" 
--------------------------------------------
      Define binning function                   
if data in DOY, timeMultiplier = 24(60/tbin)
where tbin == number of minutes to bin over
timeMultiplier == 144   --> 10 min
timeMultiplier == 96    --> 15 min
timeMultiplier == 48    --> 30 min
timeMultiplier == 24    --> 1 hr
timeMultiplier == 8     --> 3 hrs
timeMultiplier == 4     --> 6 hrs
NOTE: the calculation of timeMultiplier is independent of GPS measurement
interval (i.e. 15s or 30s)
-------------------------------------------- 
"""
    
def binDataByTime(gpsDat,timeMultiplier):
    dailyAveGPS=[]
    gpsint, ind = np.unique(np.floor(gpsDat[:,0]*timeMultiplier),return_index="true")     
    for ii in range(len(ind)-1):
        dailyAveGPS.append(np.median(gpsDat[ind[ii]:ind[ii+1],:],axis=0))
    dailyAveGPS.append(np.median(gpsDat[ind[ii+1]:-1,:],axis=0))
    dailyAveGPS=np.asarray(dailyAveGPS)
    return dailyAveGPS
    
""" 
----------------------------------------------
      Define smoothing function
the window length for this function is number 
of samples in the window.
window_len = smooth length *(60/dt)
dt      |smooth length  |window_len
15 min  |6 hr           |24
30 min  |6 hr           |12
10 min  |6 hr           |36
40 min  |12 hr          |18
----------------------------------------------
"""

def smooth(x,window_len,window='hamming'):
        s=np.r_[2*x[0]-x[window_len-1::-1],x,2*x[-1]-x[-1:-window_len:-1]]
        if window == 'flat': #moving average
          w=np.ones(window_len,'d')
        else:  
          w=eval('np.'+window+'(window_len)')
        y=np.convolve(w/w.sum(),s,mode='same')
        return y[window_len:-window_len+1]

"""
------------------------------------------------------------------------
Velocity Calculation over longer time periods - Bartholomew method
dt and dvel are in hours
dt is data interval time in hours (ie. dt=0.25 --> every 15 min., dt=0.5 --> every 30 min)
dvel is calculation time interval (i.e. dvel=4 --> ave vel over 4 hours)
------------------------------------------------------------------------
"""

def calculateVelocity(gpsDat,dt,dvel):
	win=np.zeros(int(dvel/dt))
	win[0]=-1
	win[-1]=1
	xdiff = np.convolve(gpsDat[:,1],win,mode='same')
	ydiff = np.convolve(gpsDat[:,2],win,mode='same')	
	tdiff = np.convolve(gpsDat[:,0],win,mode='same')
	velx=xdiff/tdiff
	vely=ydiff/tdiff
	return velx, vely

# ------------------------------------------------------------------------------
# VELOCITY DATA IMPORTS:
# ------------------------------------------------------------------------------

# JB positions (UM QSSI Box) (in UTC):
file2 = loadGPSdata(filepath)

# Make histograms of reported error:
histoplot(file2[:,4], 200, 'North error')
histoplot(file2[:,5], 200, 'East error')

# BROWN'S VELOCITY:
""" ---------------------------- """
""" Deal with GPS data           """
""" ---------------------------- """

# Define threshold limit for reported error (m):
limit = 0.02 # (m)

print 'removing high-error positions'
print ''
print 'fraction of North error > threshold:'
print np.float(len(file2[file2[:,4] > limit])) / np.float(len(file2[:,4]))
print ''
print 'fraction of East error > threshold:'
print np.float(len(file2[file2[:,5] > limit])) / np.float(len(file2[:,5]))
print''

# Remove outliers using threshold limit:
file2=file2[file2[:,4] < limit]
file2=file2[file2[:,5] < limit]

# Make histograms of reported error, with > threshold removed:
histoplot(file2[:,4], 25, 'North error - remove high')
histoplot(file2[:,5], 25, 'East error - remove high')

""" Bin position data 
# 96 == 15 minutes 
# 48 == 30 minutes 
# 24 == 1 hour

"""
print("Bin data on 15 minute intervals")
gpssm = binDataByTime(file2,96)

#-------------------------------------------------------------------------------

#plot binned positions, colored by DOY:
fig1 = plt.figure()
ax = fig1.add_subplot(111)
ax.scatter(gpssm[:,2],gpssm[:,1],c=gpssm[:,0],cmap=plt.cm.coolwarm, marker='.',s=75)
ax.set_xlabel('East position (m)')
ax.set_ylabel('North position (m)')
ax.set_title('Binned Position Data')

#-------------------------------------------------------------------------------
# EAST
#plot binned East component
fig2 = plt.figure()
ax = fig2.add_subplot(111)
ax.scatter(gpssm[:,0],gpssm[:,2], marker='.',s=75)
ax.set_ylabel('East position (m)')
ax.xaxis.grid(True, which='major', color='black')
ax.xaxis.grid(True, which='minor', color='gray')
ax.set_title('Binned Position Data, 15 min (East)')

# NORTH
# plot binned North component
fig3 = plt.figure()
ax = fig3.add_subplot(111)
ax.scatter(gpssm[:,0],gpssm[:,1], marker='.',s=75)
ax.set_ylabel('North position (m)')
ax.xaxis.grid(True, which='major', color='black')
ax.xaxis.grid(True, which='minor', color='gray')
ax.set_title('Binned Position Data, 15 min (North)')
#-------------------------------------------------------------------------------

# JB POSITIONS, Drop outlier binned positions
# For 27km-11, outliers are almost entirely confined to North positions

## Split array into melt season 1 and 2:
#gpssm_1 = gpssm[gpssm[:,0] < 340]
#gpssm_2 = gpssm[gpssm[:,0] > 340]

## Split binned positions into 2-week segments, melt season 1:
## (best-fit lines for 2-wk segments used to determine outlier distance thresholds)
#split_arrays_1 = np.array_split(gpssm_1, (len(gpssm_1) * 0.25 / (168. * 2))) # 0.5 == 30 min. bins

## melt season 2:
#split_arrays_2 = np.array_split(gpssm_2, (len(gpssm_2) * 0.25 / (168. * 2))) # 0.5 == 30 min. bins

## Drop outliers in North component (season 1), and add linear regress lines to plot:
#modified_melt1_split = drop_outliers_north(split_arrays_1, 'yes')
#dropoutliers1 = np.concatenate(modified_melt1_split)
## Linear interpolation through NaN points:
#dropoutiers1 = lin_interp(dropoutliers1)

## Drop outliers in North component (season 2), and add linear regress lines to plot:
#modified_melt2_split = drop_outliers_north(split_arrays_2, 'yes')
#dropoutliers2 = np.concatenate(modified_melt2_split)
## Linear interpolation through NaN points:
#dropoutiers2 = lin_interp(dropoutliers2)

#modlist = [dropoutliers1, dropoutliers2]
#gpssm_mod = np.concatenate(modlist)

gpssm_mod = gpssm
#-------------------------------------------------------------------------------

# Modifed north positions:
fig33 = plt.figure()
ax = fig33.add_subplot(111)
ax.scatter(gpssm_mod[:,0],gpssm_mod[:,1], marker='.',s=75)
ax.set_ylabel('North position (m)')
ax.xaxis.grid(True, which='major', color='black')
ax.xaxis.grid(True, which='minor', color='gray')
ax.set_title('Binned Position Data - MODIFIED, 15 min (North)')

# Modified east positions:
fig333 = plt.figure()
ax = fig333.add_subplot(111)
ax.scatter(gpssm_mod[:,0],gpssm_mod[:,2], marker='.',s=75)
ax.set_ylabel('East position (m)')
ax.xaxis.grid(True, which='major', color='black')
ax.xaxis.grid(True, which='minor', color='gray')
ax.set_title('Binned Position Data - MODIFIED, 15 min (East)')

# Modified binned positions, colored by DOY:
fig11 = plt.figure()
ax = fig11.add_subplot(111)
ax.scatter(gpssm_mod[:,2],gpssm_mod[:,1],c=gpssm_mod[:,0],cmap=plt.cm.coolwarm, marker='.',s=75)
ax.set_xlabel('East position (m)')
ax.set_ylabel('North position (m)')
ax.set_title('Binned Position Data - MODIFIED: 27km-11')

    
""" Smooth position data """
print("Apply 8 hour smoothing to binned positions")

# window_len == smooth length *(60/dt)
# 720 == 6 hrs @ 30 sec intervals (6 hrs * (60 / 0.5))
#  24 == 6 hrs @ 15 min intervals (6 hrs * (60 / 15))
#  12 == 6 hrs @ 30 min intervals (6 hrs * (60 / 30))
#   6 == 6 hrs @ 1 hr intervals (6 hrs * (60 / 60))
#  32 == 8 hrs @ 15 min intervals (8 hrs * (60 / 15))
#  16 == 8 hrs @ 30 min intervals (8 hrs * (60 / 30))
#   8 == 8 hrs @ 1 hr intervals (8 hrs * (60 / 60))
#  24 == 12 hrs @ 30 min intervals (12 hrs * (60 / 30))
#   4 == 12 hrs @ 3 hr intervals (12 hrs * (60/ (3*60))

gpssm_mod[:,1] = smooth(gpssm_mod[:,1],32)  
gpssm_mod[:,2] = smooth(gpssm_mod[:,2],32)

""" Calculate velocity """
print("Calculating velocity over 8-hr spans")
xVelsm, yVelsm = calculateVelocity(gpssm_mod,0.25,8) # 15 min data, 8-hr velocity
smVel = np.sqrt(xVelsm**2+yVelsm**2) # m/day

print("Apply 8 hour smoothing to velocity")
smVel = smooth(smVel,32)

#-------------------------------------------------------------------------------
Brown_data = {'DOY':gpssm_mod[:,0], 'velocity_myear':smVel*365.25} # m/year
df = pd.DataFrame(data=Brown_data)

Brown_index = df['DOY'].map(dateparser_2011) # make a datetime index
Brown_index.name = 'timestamp' # change name of index
Brown_data_dt = df.set_index(Brown_index) # set index to datetime, re-name
#Brown_data_dt.index = Brown_data_dt.index.round('min')

# Shift to local time:
Brown_data_dt.index = Brown_data_dt.index - dt.timedelta(hours=2)

# Drop high/low velocity:
#Brown_data_dt['velocity_myear'][Brown_data_dt['velocity_myear'] > 250.] = np.NaN
#Brown_data_dt['velocity_myear'][Brown_data_dt['velocity_myear'] < 60.] = np.NaN

# Export velocity data:
Brown_data_dt.to_csv("%s_velocity.csv" % site, columns=['DOY','velocity_myear'],index=True)

# ------------------------------------------------------------------------------
# VELOCITY PLOTS:
# ------------------------------------------------------------------------------

# COMPARE BROWN & PJW

#majorLocator   = MultipleLocator(1)
#majorFormatter = FormatStrFormatter('%d')
#minorLocator   = MultipleLocator(0.5)

fig1 = plt.figure()
ax = fig1.add_subplot(111)
ax.plot(Brown_data_dt.index, Brown_data_dt.velocity_myear, color='r', alpha=0.9, label='JB positions')
ax.plot(Brown_data_dt.index, Brown_data_dt.velocity_myear, 'k.', alpha=0.9)

ax.set_ylabel('GPS velocity (m/yr)')
#ax.set_xlim('2011-08-05','2011-09-25')
ax.set_xlim('2011-07-01','2011-09-25')
ax.set_ylim(0,350)
#ax.xaxis.set_major_locator(majorLocator)
#ax.xaxis.set_major_formatter(majorFormatter)
#ax.xaxis.set_minor_locator(minorLocator)
ax.xaxis.grid(True, which='major', color='black')
ax.xaxis.grid(True, which='minor', color='gray')
ax.set_title('27km-11, GPS velocity')
#plt.subplots_adjust(left=0.09, right=0.8, top=0.9, bottom=0.1)
plt.tight_layout()
plt.subplots_adjust(left=0.06)
plt.legend(loc=1)

#plt.text('2011-08-07', 325, '15-min bin size, 6-hr smoothing, 6-hr velocity')

plt.show()