#!/usr/bin/env python

"""
Analysis of Fall 2014 GPS, pressure, and melt
Patrick Wright, Inversion Labs
Job: UM QSSI
Jan. 2018
"""

import datetime as dt
import numpy as np
import scipy as sp
import pandas as pd
import matplotlib
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from matplotlib.dates import date2num
#from matplotlib.ticker import MultipleLocator, FormatStrFormatter
from matplotlib import rcParams
from dateutil.relativedelta import relativedelta
import pylab
from IPython import embed
from math import factorial
from datetime import timedelta

# Convert DOY to datetime:
def dateparser_2014(doy):
  return dt.datetime(year=2014, month=1, day=1) + dt.timedelta(days=doy) - dt.timedelta(days=1)

# set index of dataframes to datetime:
def set_index(df):
  index = pd.to_datetime(df.Datetime)
  df = df.set_index(index)
  df.index.rename('Datetime_index', inplace=True)
  df = df.drop('Datetime', axis=1)
  return df

# ------------------------------------------------------------------------------
# VELOCITY IMPORTS:
# ------------------------------------------------------------------------------

vel1 = pd.read_csv('velocity/processed_revisedJB/33km-14-West_velocity.csv', sep=',', header=0, names=['timestamp','DOY','velocity_myear'])
vel1_index = pd.to_datetime(vel1['timestamp'])
vel1_index.name = 'datetimeindex'
vel1 = vel1.set_index(vel1_index)
vel1 = vel1.drop('timestamp', axis=1)

# drop suspect periods:
#vel1['2012-09-07 11:00':'2012-09-08 09:00'] = np.NaN
#vel1['2012-09-18 11:00': ] = np.NaN

vel2 = pd.read_csv('velocity/processed_revisedJB/33km-14-North_velocity.csv', sep=',', header=0, names=['timestamp','DOY','velocity_myear'])
vel2_index = pd.to_datetime(vel2['timestamp'])
vel2_index.name = 'datetimeindex'
vel2 = vel2.set_index(vel2_index)
vel2 = vel2.drop('timestamp', axis=1)

vel3 = pd.read_csv('velocity/processed_revisedJB/33km-14-East_velocity.csv', sep=',', header=0, names=['timestamp','DOY','velocity_myear'])
vel3_index = pd.to_datetime(vel3['timestamp'])
vel3_index.name = 'datetimeindex'
vel3 = vel3.set_index(vel3_index)
vel3 = vel3.drop('timestamp', axis=1)

vel4 = pd.read_csv('velocity/processed_revisedJB/33km-14-South_velocity.csv', sep=',', header=0, names=['timestamp','DOY','velocity_myear'])
vel4_index = pd.to_datetime(vel4['timestamp'])
vel4_index.name = 'datetimeindex'
vel4 = vel4.set_index(vel4_index)
vel4 = vel4.drop('timestamp', axis=1)

# ------------------------------------------------------------------------------
# PRESSURE IMPORTS: 
# ------------------------------------------------------------------------------

# NOTE: These pressure data are from '~/UM_RESEARCH/water_pressure/Pressure_data_PJWpublication'
#       (this directory is replicated on Box)
# NOTE: No resample / interpolate.... just for viewing trends. Data could have uneven sampling.

GL14_1C = pd.read_csv('pressure/GL14_W_H3.txt',sep=',')

GL14_1C_index = GL14_1C['Jday_col'].map(dateparser_2014) # make a datetime index
GL14_1C_index.name = 'timestamp' # change name of index
GL14_1C_dt = GL14_1C.set_index(GL14_1C_index) # set index to datetime, re-name
GL14_1C_scaled = (9.81 * 1000 * GL14_1C_dt.m_h2o_col)  / (910 * 9.81 * 661) # pgh water column / pgh ice 
GL14_1C_scaled.name = 'scaled' # change name of series


GL14_1D = pd.read_csv('pressure/GL14_N_H4.txt',sep=',')

GL14_1D_index = GL14_1D['Jday_col'].map(dateparser_2014) # make a datetime index
GL14_1D_index.name = 'timestamp' # change name of index
GL14_1D_dt = GL14_1D.set_index(GL14_1D_index) # set index to datetime, re-name
GL14_1D_scaled = (9.81 * 1000 * GL14_1D_dt.m_h2o_col)  / (910 * 9.81 * 641) # pgh water column / pgh ice 
GL14_1D_scaled.name = 'scaled' # change name of series


# ------------------------------------------------------------------------------
# MELT PRODUCTION IMPORTS: NOTE: Files replaced on 12/1/17
# ------------------------------------------------------------------------------
GL14_melt = pd.read_csv('melt/melt_MTI_33km-14.csv', sep=',', header=0, names=['Datetime', 'optimized_mti'])
GL14_melt_index = pd.to_datetime(GL14_melt['Datetime'])
GL14_melt_index.name = 'timestamp'
GL14_melt = GL14_melt.set_index(GL14_melt_index)
GL14_melt = GL14_melt.drop('Datetime', axis=1)

# ------------------------------------------------------------------------------
# RAIN IMPORT (Block Site)
# ------------------------------------------------------------------------------
GL14_rain = pd.read_csv('melt/BlockSite-rain-dailytotal-meters.csv',
                        sep=',', header=0, names=['Datetime', 'rain_dailytotal_meters'])
GL14_rain_index = pd.to_datetime(GL14_rain['Datetime'])
GL14_rain_index.name = 'timestamp'
GL14_rain = GL14_rain.set_index(GL14_rain_index)
GL14_rain = GL14_rain.drop('Datetime', axis=1)

# ------------------------------------------------------------------------------
# PLOTS:
# ------------------------------------------------------------------------------

# MULTI-PANEL: velocity, pressure, melt

fig = plt.figure(figsize=(10,10))

ax1 = plt.subplot2grid((4, 1), (0, 0)) # GPS 
ax2 = plt.subplot2grid((4, 1), (1, 0)) # water pressure 
ax3 = plt.subplot2grid((4, 1), (2, 0)) # melt production
ax4 = plt.subplot2grid((4, 1), (3, 0)) # rain

ax1.plot(vel1.index, vel1.velocity_myear, label='West')
ax1.plot(vel2.index, vel2.velocity_myear, label='North')
ax1.plot(vel2.index, vel2.velocity_myear, label='East')
ax1.plot(vel2.index, vel2.velocity_myear, label='South')

ax1.set_xlim('2014-08-15','2014-10-15')
ax1.set_ylim(45,375)
ax1.set_ylabel('velocity (m/yr)')
ax1.tick_params(axis='x', labelbottom='off')

plt.setp(ax1.get_yticklabels()[1], visible=False)
#plt.setp(ax1.get_yticklabels()[-1], visible=False)
#plt.setp(ax1.get_yticklabels()[2], visible=False)
#plt.setp(ax1.get_yticklabels()[4], visible=False)
#plt.setp(ax1.get_yticklabels()[6], visible=False)

#ax1.yaxis.set_tick_params(labelsize=12)
ax1.legend(loc='upper right')
ax1.grid(color='black', alpha=0.3, linestyle='-', linewidth=1)
ax1.annotate('GPS Velocity', (mdates.datestr2num('2014-08-16'), 325), style='oblique')


ax2.plot(GL14_1C_dt.index, GL14_1C_scaled, label='GL14-W-H3', linewidth=1)
ax2.plot(GL14_1D_dt.index, GL14_1D_scaled, label='GL14-N-H4', linewidth=1)

ax2.set_xlim('2014-08-15','2014-10-15')
ax2.tick_params(axis='x', labelbottom='off')
ax2.set_ylim(1.02,1.16)
#ax2.set_ylim(0.935,1.01)
ax2.set_ylabel('pressure (fract OB)')

#plt.setp(ax2.get_yticklabels()[0], visible=False)
#plt.setp(ax2.get_yticklabels()[-1], visible=False)
#plt.setp(ax2.get_yticklabels()[1], visible=False)
#plt.setp(ax2.get_yticklabels()[3], visible=False)
#plt.setp(ax2.get_yticklabels()[5], visible=False)

#ax2.yaxis.set_tick_params(labelsize=12)
ax2.legend(loc='upper right')
ax2.grid(color='black', alpha=0.3, linestyle='-', linewidth=1)
ax2.annotate('Water Pressure', (mdates.datestr2num('2014-08-15 12:00'), 1.085), style='oblique')

WIDTH = 0.3
# align can only be 'center' or 'edge', where 'edge' uses left edge of bar at corresponding index
# This implementation plots bars at midnight, representing melt over following day.
ax3.bar(GL14_melt.index, GL14_melt.optimized_mti,
        width=WIDTH, edgecolor='black', color='red', align='center', label='33km-14')

ax3.xaxis_date()
ax3.set_ylabel('melt (m/day)')
ax3.set_xlim('2014-08-15','2014-10-15')
ax3.set_ylim(0,0.050)
ax3.tick_params(axis='x', labelbottom='off')

#plt.setp(ax3.get_yticklabels()[0], visible=False)
plt.setp(ax3.get_yticklabels()[-1], visible=False)
#plt.setp(ax3.get_yticklabels()[2], visible=False)
#plt.setp(ax3.get_yticklabels()[4], visible=False)
#plt.setp(ax3.get_yticklabels()[5], visible=False)

ax3.legend(loc='upper right')
ax3.grid(color='black', alpha=0.3, linestyle='-', linewidth=1)
ax3.annotate('Melt Production (MTI)', (mdates.datestr2num('2014-08-20'), 0.045), style='oblique')

WIDTH = 0.3
# align can only be 'center' or 'edge', where 'edge' uses left edge of bar at corresponding index
# This implementation plots bars at midnight, representing melt over following day.
ax4.bar(GL14_rain.index, GL14_rain.rain_dailytotal_meters,
        width=WIDTH, edgecolor='black', color='blue', align='center', label='rain_dailytotal')

ax4.xaxis_date()
ax4.set_ylabel('rain (m/day)')
ax4.set_xlim('2014-08-01','2014-10-15')
ax4.set_ylim(0,0.02)

plt.setp(ax4.get_yticklabels()[-1], visible=False)

ax4.legend(loc='upper right')
ax4.grid(color='black', alpha=0.3, linestyle='-', linewidth=1)
ax4.annotate('Rain Daily Total', (mdates.datestr2num('2014-08-02'), 0.018), style='oblique')

plt.subplots_adjust(wspace=0.001, hspace=0.001)
plt.subplots_adjust(
  left=0.085, right=0.95,
  top=0.95, bottom=0.13)

#plt.legend()

plt.xticks(rotation=90, fontsize=10)

#plt.savefig('meltvelocity.svg', format='svg')

plt.show()