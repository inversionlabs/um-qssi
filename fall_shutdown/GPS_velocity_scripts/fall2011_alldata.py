#!/usr/bin/env python

"""
Analysis of Fall 2011 GPS, pressure, and melt
Patrick Wright, Inversion Labs
Job: UM QSSI
Nov. 2017
"""

import datetime as dt
import numpy as np
import scipy as sp
import pandas as pd
import matplotlib
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from matplotlib.dates import date2num
#from matplotlib.ticker import MultipleLocator, FormatStrFormatter
from matplotlib import rcParams
from dateutil.relativedelta import relativedelta
import pylab
from IPython import embed
from math import factorial
from datetime import timedelta

# Convert DOY to datetime:
def dateparser_2011(doy):
  return dt.datetime(year=2011, month=1, day=1) + dt.timedelta(days=doy) - dt.timedelta(days=1)

# set index of dataframes to datetime:
def set_index(df):
  index = pd.to_datetime(df.Datetime)
  df = df.set_index(index)
  df.index.rename('Datetime_index', inplace=True)
  df = df.drop('Datetime', axis=1)
  return df

def histoplot(data, num_bins, titlestring):
    fig1 = plt.figure(figsize=(6*1.618, 6))
    ax1 = fig1.add_subplot(111)
    n, bins, patches = plt.hist(data, num_bins, facecolor='blue', edgecolor='k', alpha=0.5)
    #plt.axvline(x=np.median(data), ymin=0, ymax=180, linewidth=1, color='r') # plot the median
    #plt.axvline(x=np.mean(data), ymin=0, ymax=180, linewidth=1, color='k') # plot the mean
    ax1.set_title(titlestring, fontsize=12)
    ax1.set_xlabel('error', fontsize=12)
    ax1.set_ylabel('samples', fontsize=12)
    ax1.set_xlim(0,0.1)
    plt.xticks(fontsize=12)
    plt.yticks(fontsize=12)

#-------------------------------------------------------------------------------
# Joel Brown's loading / binning / smoothing / velocity functions:

""" Define data read and write functions """

#def loadPdata(filenam):
    #with open(filenam,"r") as f:
        #pData=np.loadtxt(f,delimiter=',')
        #return pData
    
def loadGPSdata(filenam):
    with open(filenam,"r") as f:
        pData=np.loadtxt(f, skiprows=1,delimiter=',')
        return pData

""" 
--------------------------------------------
      Define binning function                   
if data in DOY, timeMultiplier = 24(60/tbin)
where tbin == number of minutes to bin over
timeMultiplier == 144   --> 10 min
timeMultiplier == 96    --> 15 min
timeMultiplier == 48    --> 30 min
timeMultiplier == 24    --> 1 hr
timeMultiplier == 8     --> 3 hrs
timeMultiplier == 4     --> 6 hrs
NOTE: the calculation of timeMultiplier is independent of GPS measurement
interval (i.e. 15s or 30s)
-------------------------------------------- 
"""
    
def binDataByTime(gpsDat,timeMultiplier):
    dailyAveGPS=[]
    gpsint, ind = np.unique(np.floor(gpsDat[:,0]*timeMultiplier),return_index="true")     
    for ii in range(len(ind)-1):
        dailyAveGPS.append(np.median(gpsDat[ind[ii]:ind[ii+1],:],axis=0))
    dailyAveGPS.append(np.median(gpsDat[ind[ii+1]:-1,:],axis=0))
    dailyAveGPS=np.asarray(dailyAveGPS)
    return dailyAveGPS
    
""" 
----------------------------------------------
      Define smoothing function
the window length for this function is number 
of samples in the window.
window_len = smooth length *(60/dt)
dt      |smooth length  |window_len
15 min  |6 hr           |24
30 min  |6 hr           |12
10 min  |6 hr           |36
40 min  |12 hr          |18
----------------------------------------------
"""

def smooth(x,window_len,window='hamming'):
        s=np.r_[2*x[0]-x[window_len-1::-1],x,2*x[-1]-x[-1:-window_len:-1]]
        if window == 'flat': #moving average
                w=np.ones(window_len,'d')
        else:  
                w=eval('np.'+window+'(window_len)')
        y=np.convolve(w/w.sum(),s,mode='same')
        return y[window_len:-window_len+1]

"""
------------------------------------------------------------------------
Velocity Calculation over longer time periods - Bartholomew method
dt and dvel are in hours
dt is data interval time in hours (ie. dt=0.25 --> every 15 min., dt=0.5 --> every 30 min)
dvel is calculation time interval (i.e. dvel=4 --> ave vel over 4 hours)
------------------------------------------------------------------------
"""

def calculateVelocity(gpsDat,dt,dvel):
	win=np.zeros(int(dvel/dt))
	win[0]=-1
	win[-1]=1
	xdiff = np.convolve(gpsDat[:,1],win,mode='same')
	ydiff = np.convolve(gpsDat[:,2],win,mode='same')	
	tdiff = np.convolve(gpsDat[:,0],win,mode='same')
	velx=xdiff/tdiff
	vely=ydiff/tdiff
	return velx, vely

# ------------------------------------------------------------------------------
# VELOCITY IMPORTS:
# ------------------------------------------------------------------------------

# NOTE: These file were converted from DOY to Datetime (including -1 day).
#       JB uploaded all files in UTC.
file1 = pd.read_csv('velocity/27km-11_30sec_smooth.csv', sep=',') # 27km-11, Brown's positions (30s vel calcs)
GL11_1_2011_30sec_JB = set_index(file1)
# Change UTC time to local time (UTC-2):
GL11_1_2011_30sec_JB.index = GL11_1_2011_30sec_JB.index - dt.timedelta(hours=2)

file2 = pd.read_csv('velocity/27km-11_6hr.csv', sep=',') # 27km-11, Brown's positions (6hr vel calcs)
GL11_1_2011_6hr_JB = set_index(file2)
# Change UTC time to local time (UTC-2):
GL11_1_2011_6hr_JB.index = GL11_1_2011_6hr_JB.index - dt.timedelta(hours=2)

# NOTE: Are you sure that the files in PJW thesis are UTC?
file3 = pd.read_csv('velocity/GL11-1_2011melt.csv', sep=',') # 27km-11, PJW's thesis
GL11_1_2011 = set_index(file3)
# Change UTC time to local time (UTC-2):
GL11_1_2011.index = GL11_1_2011.index - dt.timedelta(hours=2)

file4 = pd.read_csv('velocity/GL11-2_2011melt.csv', sep=',') # 46km-11, PJW's thesis
GL11_2_2011 = set_index(file4)
# Change UTC time to local time (UTC-2):
GL11_2_2011.index = GL11_2_2011.index - dt.timedelta(hours=2)

file5 = loadGPSdata("GPS_Pressure_Data_Organized/GL11_1/GL11-1_GPS.txt")

# Make histograms of reported error:
histoplot(file5[:,4], 100, 'North error')
histoplot(file5[:,5], 100, 'East error')

# BROWN'S VELOCITY:
""" ---------------------------- """
""" Deal with GPS data           """
""" ---------------------------- """

# Remove outliers using threshold limit in reported error (m):
limit = 0.02

print 'removing high-error positions'
print ''
print 'fraction North error over limit:'
print np.float(len(file5[file5[:,4] > limit,:])) / np.float(len(file5[:,4]))
print ''
print 'fraction East error over limit:'
print np.float(len(file5[file5[:,5] > limit,:])) / np.float(len(file5[:,5]))
print''

file5=file5[file5[:,4] < limit,:]
file5=file5[file5[:,5] < limit,:]

""" Bin position data 
# 96 == 15 minutes 
# 48 == 30 minutes 
# 24 == 1 hour

"""
#print("Bin data on 30 minute intervals")
#gpssm = binDataByTime(file5,48)
print("Bin data on 3-hr intervals")
gpssm = binDataByTime(file5,8)

""" Smooth position data """
#print("Apply 6 hour smoothing")
print("Apply 12 hour smoothing")

# window_len == smooth length *(60/dt)
# 720 == 6 hrs @ 30 sec intervals {(1 30sec-step * 2) * 60min * 6hrs}
#  24 == 6 hrs @ 15 min intervals {(1 15min-step * 4) * 6hrs}
#  12 == 6 hrs @ 30 min intervals {(1 30min-step * 2) * 6hrs}
#   8 == 8 hrs @ 1 hr intervals {(1 1hr-step * 8hrs)}

#   4 == 12 hrs * (60/ (3*60))

gpssm[:,1] = smooth(gpssm[:,1],4)  
gpssm[:,2] = smooth(gpssm[:,2],4)

""" Calculate velocity """
#print("Calculating velocity over 6-hr spans")
#xVelsm, yVelsm = calculateVelocity(gpssm,.5,6) # 30 min data, 6-hr velocity

print("Calculating velocity over 8-hr spans")
xVelsm, yVelsm = calculateVelocity(gpssm,3,8) # 3-hr data, 8-hr velocity

smVel = np.sqrt(xVelsm**2+yVelsm**2) # m/day

Brown_data = {'DOY':gpssm[:,0], 'velocity_myear':smVel*365.25} # m/year
df = pd.DataFrame(data=Brown_data)

Brown_index = df['DOY'].map(dateparser_2011) # make a datetime index
Brown_index.name = 'timestamp' # change name of index
Brown_data_dt = df.set_index(Brown_index) # set index to datetime, re-name

# Shift to local time:
Brown_data_dt.index = Brown_data_dt.index - dt.timedelta(hours=2)

# Drop high velocity:
Brown_data_dt['velocity_myear'][Brown_data_dt['velocity_myear'] > 250.] = np.NaN

# ------------------------------------------------------------------------------
# PRESSURE IMPORTS:
# ------------------------------------------------------------------------------

# NOTE: These pressure data are from '~/UM_RESEARCH/water_pressure/Pressure_data_PJWpublication'
#       (this directory is replicated on Box)
# NOTE: No resample / interpolate.... just for viewing trends. Data could have uneven sampling.

GL11_1A = pd.read_csv('pressure/GL11-1A.txt', sep=' ',skipinitialspace=True, header=None, names=['DOY', 'm_h2o'])
GL11_1A_index = GL11_1A['DOY'].map(dateparser_2011) # make a datetime index
GL11_1A_index.name = 'timestamp' # change name of index
GL11_1A_dt = GL11_1A.set_index(GL11_1A_index) # set index to datetime, re-name
GL11_1A_scaled = (9.81 * 1000. * GL11_1A_dt.m_h2o)  / (910. * 9.81 * 457.5) # pgh water column / pgh ice
GL11_1A_scaled.name = 'scaled' # change name of series

GL11_1B = pd.read_csv('pressure/GL11-1B.txt', sep=' ',skipinitialspace=True, header=None, names=['DOY', 'm_h2o'])
GL11_1B_index = GL11_1B['DOY'].map(dateparser_2011) # make a datetime index
GL11_1B_index.name = 'timestamp' # change name of index
GL11_1B_dt = GL11_1B.set_index(GL11_1B_index) # set index to datetime, re-name
GL11_1B_scaled = (9.81 * 1000. * GL11_1B_dt.m_h2o)  / (910. * 9.81 * 466.) # pgh water column / pgh ice
GL11_1B_scaled.name = 'scaled' # change name of series

GL11_1C = pd.read_csv('pressure/GL11-1C.txt', sep=' ',skipinitialspace=True, header=None, names=['DOY', 'm_h2o'])
GL11_1C_index = GL11_1C['DOY'].map(dateparser_2011) # make a datetime index
GL11_1C_index.name = 'timestamp' # change name of index
GL11_1C_dt = GL11_1C.set_index(GL11_1C_index) # set index to datetime, re-name
GL11_1C_scaled = (9.81 * 1000. * GL11_1C_dt.m_h2o)  / (910. * 9.81 * 459.5) # pgh water column / pgh ice
GL11_1C_scaled.name = 'scaled' # change name of series

GL11_2B = pd.read_csv('pressure/GL11-2B.txt', sep=' ',skipinitialspace=True, header=None, names=['DOY', 'm_h2o'])
GL11_2B_index = GL11_2B['DOY'].map(dateparser_2011) # make a datetime index
GL11_2B_index.name = 'timestamp' # change name of index
GL11_2B_dt = GL11_2B.set_index(GL11_2B_index) # set index to datetime, re-name
GL11_2B_scaled = (9.81 * 1000. * GL11_2B_dt.m_h2o)  / (910. * 9.81 * 821.) # pgh water column / pgh ice
GL11_2B_scaled.name = 'scaled' # change name of series

GL11_2D = pd.read_csv('pressure/GL11-2D.txt', sep=' ',skipinitialspace=True, header=None, names=['DOY', 'm_h2o'])
GL11_2D_index = GL11_2D['DOY'].map(dateparser_2011) # make a datetime index
GL11_2D_index.name = 'timestamp' # change name of index
GL11_2D_dt = GL11_2D.set_index(GL11_2D_index) # set index to datetime, re-name
GL11_2D_scaled = (9.81 * 1000. * GL11_2D_dt.m_h2o)  / (910. * 9.81 * 814.5) # pgh water column / pgh ice
GL11_2D_scaled.name = 'scaled' # change name of series

# ------------------------------------------------------------------------------
# MELT PRODUCTION IMPORTS: NOTE: Files replaced on 12/1/17
# ------------------------------------------------------------------------------
GL11_1_melt = pd.read_csv('melt/melt_MTI_27km-11.csv', sep=',', header=0, names=['Datetime', 'optimized_mti'])
GL11_1_melt_index = pd.to_datetime(GL11_1_melt['Datetime'])
GL11_1_melt_index.name = 'timestamp'
GL11_1_melt = GL11_1_melt.set_index(GL11_1_melt_index)
GL11_1_melt = GL11_1_melt.drop('Datetime', axis=1)

GL11_2_melt = pd.read_csv('melt/melt_MTI_46km-11.csv', sep=',', header=0, names=['Datetime', 'optimized_mti'])
GL11_2_melt_index = pd.to_datetime(GL11_2_melt['Datetime'])
GL11_2_melt_index.name = 'timestamp'
GL11_2_melt = GL11_2_melt.set_index(GL11_2_melt_index)
GL11_2_melt = GL11_2_melt.drop('Datetime', axis=1)

# ------------------------------------------------------------------------------
# PLOTS:
# ------------------------------------------------------------------------------

# COMPARE BROWN & PJW

#majorLocator   = MultipleLocator(1)
#majorFormatter = FormatStrFormatter('%d')
#minorLocator   = MultipleLocator(0.5)

fig1 = plt.figure()
ax = fig1.add_subplot(111)
#ax.plot(GL11_1_2011_30sec_JB.index, GL11_1_2011_30sec_JB.velocity_myear_smooth, color='r', label='JB processing')
ax.plot(Brown_data_dt.index, Brown_data_dt.velocity_myear, color='r', alpha=0.9, label='JB velocity')
ax.plot(GL11_1_2011.index, GL11_1_2011.velocity_myear_smooth, color='b', alpha=0.9, label='PW velocity')
#ax.set_xlabel('')
ax.set_ylabel('GPS velocity (m/yr)')
ax.set_xlim('2011-08-05','2011-09-25')
ax.set_ylim(0,350)
#ax.xaxis.set_major_locator(majorLocator)
#ax.xaxis.set_major_formatter(majorFormatter)
#ax.xaxis.set_minor_locator(minorLocator)
ax.xaxis.grid(True, which='major', color='black')
ax.xaxis.grid(True, which='minor', color='gray')
ax.set_title('27km-11, GPS velocity, JB vs PW (updated results: 12/12/2017)')
#plt.subplots_adjust(left=0.09, right=0.8, top=0.9, bottom=0.1)
plt.tight_layout()
plt.subplots_adjust(left=0.06)
plt.legend()

plt.text('2011-08-7', 325, '3-hr bin size, 12-hr smoothing, 8-hr velocity')

# ------------------------------------------------------------------------------

# MULTI-PANEL: velocity, pressure, melt

fig = plt.figure(figsize=(10,10))

ax1 = plt.subplot2grid((3, 1), (0, 0)) # GPS (GL11-1 & GL11-2)
ax2 = plt.subplot2grid((3, 1), (1, 0)) # water pressure (GL11-1 & GL11-2, all holes)
ax3 = plt.subplot2grid((3, 1), (2, 0)) # melt production (GL11-1 & GL11-2) -- paired bars?

ax1.plot(GL11_1_2011.index, GL11_1_2011.velocity_myear_smooth, color='r', label='27km-11')
ax1.plot(GL11_2_2011.index, GL11_2_2011.velocity_myear_smooth, color='b', label='46km-11')
#ax1.set_xlim('2011-08-15','2011-09-15')
ax1.set_xlim('2011-08-23','2011-09-03')
ax1.set_ylim(50,250)
ax1.set_ylabel('velocity (m/yr)')
ax1.tick_params(axis='x', labelbottom='off')
plt.setp(ax1.get_yticklabels()[0], visible=False)
#plt.setp(ax1.get_yticklabels()[-1], visible=False)
#plt.setp(ax1.get_yticklabels()[2], visible=False)
#plt.setp(ax1.get_yticklabels()[4], visible=False)
#plt.setp(ax1.get_yticklabels()[6], visible=False)
#ax1.yaxis.set_tick_params(labelsize=12)
ax1.legend(loc='upper right')
ax1.grid(color='black', alpha=0.3, linestyle='-', linewidth=1)
ax1.annotate('GPS Velocity', (mdates.datestr2num('2011-08-16'), 225), style='oblique')


ax2.plot(GL11_1A_dt.index, GL11_1A_scaled, color='r', label='27km-11', linewidth=1)
ax2.plot(GL11_1B_dt.index, GL11_1B_scaled, color='r', label='', linewidth=1)
ax2.plot(GL11_1C_dt.index, GL11_1C_scaled, color='r', label='', linewidth=1)
ax2.plot(GL11_2B_dt.index, GL11_2B_scaled, color='b', label='46km-11', linewidth=1)
ax2.plot(GL11_2D_dt.index, GL11_2D_scaled, color='b', label='', linewidth=1)
#ax2.set_xlim('2011-08-15','2011-09-15')
ax2.set_xlim('2011-08-23','2011-09-03')
ax2.tick_params(axis='x', labelbottom='off')
#ax2.set_ylim(0.82,1.05)
ax2.set_ylim(0.935,1.01)
ax2.set_ylabel('pressure (fract OB)')
plt.setp(ax2.get_yticklabels()[0], visible=False)
#plt.setp(ax2.get_yticklabels()[-1], visible=False)
#plt.setp(ax2.get_yticklabels()[2], visible=False)
#plt.setp(ax2.get_yticklabels()[4], visible=False)
#plt.setp(ax2.get_yticklabels()[6], visible=False)
#ax2.yaxis.set_tick_params(labelsize=12)
ax2.legend(loc='upper right')
ax2.grid(color='black', alpha=0.3, linestyle='-', linewidth=1)
ax2.annotate('Water Pressure', (mdates.datestr2num('2011-08-16'), 1.03), style='oblique')

WIDTH = 0.3
# align can only be 'center' or 'edge', where 'edge' uses left edge of bar at corresponding index
# This implementation plots bars at midnight, representing melt over following day.
ax3.bar(GL11_1_melt.index -  pd.DateOffset(days=0.15), GL11_1_melt.optimized_mti,
        width=WIDTH, edgecolor='black', color='red', align='center', label='27km-11')
ax3.bar(GL11_2_melt.index +  pd.DateOffset(days=0.15), GL11_2_melt.optimized_mti,
        width=WIDTH, edgecolor='black', color='blue', align='center', label='46km-11')
ax3.xaxis_date()
ax3.set_ylabel('melt (m/day)')
#ax3.set_xlim('2011-08-15','2011-09-15')
ax3.set_xlim('2011-08-23','2011-09-03')
ax3.set_ylim(0,0.05)
#ax3.tick_params(axis='x', labelbottom='off')

#plt.setp(ax3.get_yticklabels()[0], visible=False)
#plt.setp(ax3.get_yticklabels()[-1], visible=False)
#plt.setp(ax3.get_yticklabels()[2], visible=False)
#plt.setp(ax3.get_yticklabels()[4], visible=False)
#plt.setp(ax3.get_yticklabels()[5], visible=False)

ax3.legend(loc='upper right')
ax3.grid(color='black', alpha=0.3, linestyle='-', linewidth=1)
ax3.annotate('Melt Production (MTI)', (mdates.datestr2num('2011-08-16'), 0.045), style='oblique')

plt.subplots_adjust(wspace=0.001, hspace=0.001)
plt.subplots_adjust(
  left=0.085, right=0.95,
  top=0.95, bottom=0.13)

#plt.legend()

plt.xticks(rotation=90, fontsize=10)

#plt.savefig('meltvelocity.svg', format='svg')

plt.show()