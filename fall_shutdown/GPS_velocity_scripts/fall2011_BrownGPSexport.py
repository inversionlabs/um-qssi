#!/usr/bin/env python

"""
Script to examine 2011 fall "shutdown" period, GrIS
Patrick Wright
Inversion Labs, LLC
Job: UM QSSI
11/10/2017
"""

import numpy as np
import scipy as sp
from scipy import signal
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator, FormatStrFormatter
import datetime as dt

from IPython import embed

smoothing_window_hrs = 6

smoothing_window_hrs_velocity = 6

# Remove outliers using threshold limit in reported error (m):
limit = 0.05

#-------------------------------------------------------------------------------
# READ DATA, CONVERT DOY --> DATETIME
#-------------------------------------------------------------------------------
print 'load data...'

# Define directory paths:
fpath1_gps='GPS_Pressure_Data_Organized/GL11_1/GL11-1_GPS.txt'
fpath2_press='GPS_Pressure_Data_Organized/GL11_1/GL11-1_Ps_A.txt'
fpath3_press='GPS_Pressure_Data_Organized/GL11_1/GL11-1_Ps_B.txt'
fpath4_press='GPS_Pressure_Data_Organized/GL11_1/GL11-1_Ps_C.txt'

# Read in data:
gps = pd.read_csv(fpath1_gps, sep=',',header=None,
                  names=['DOY','North','East','Up','dNorth','dEast','dUp'], skiprows=1)
P_A = pd.read_csv(fpath2_press, sep=',',header=None, names=['DOY','Ps'], skiprows=1)
P_B = pd.read_csv(fpath3_press, sep=',',header=None, names=['DOY','Ps'], skiprows=1)
P_C = pd.read_csv(fpath4_press, sep=',',header=None, names=['DOY','Ps'], skiprows=1)

print 'convert DOY --> Datetime'
# Convert DOY to Datetime:
# NOTE: All times are in UTC!
gps.Datetime = pd.to_datetime('2011-1-1') + pd.to_timedelta(gps.DOY, unit='D') - pd.Timedelta(days=1) #'Name' is still DOY
P_A.Datetime = pd.to_datetime('2011-1-1') + pd.to_timedelta(P_A.DOY, unit='D') - pd.Timedelta(days=1)
P_B.Datetime = pd.to_datetime('2011-1-1') + pd.to_timedelta(P_B.DOY, unit='D') - pd.Timedelta(days=1)
P_C.Datetime = pd.to_datetime('2011-1-1') + pd.to_timedelta(P_C.DOY, unit='D') - pd.Timedelta(days=1) # can also use dt.timedelta(days=1)

# set index of dataframes to datetime, round to nearest interval:
def set_index_gps(df):
  df = df.set_index(df.Datetime)
  round_index = df.index.round('s') # data is 30 sec
  df = df.set_index(round_index)
  df.index.rename('Datetime', inplace=True)
  return df

def set_index_Ps(df):
  df = df.set_index(df.Datetime)
  round_index = df.index.round('min') # data is 5 min
  df = df.set_index(round_index)
  df.index.rename('Datetime', inplace=True)
  return df

# Define datetime-indexed dataframes:
gps = set_index_gps(gps) # GPS positions (meters)
P_A = set_index_Ps(P_A) # pressure units (meters head)
P_B = set_index_Ps(P_B)
P_C = set_index_Ps(P_C)

# Index into shorter time-segment:
gps_fall = gps['2011-07-02':'2011-12-01']

#-------------------------------------------------------------------------------
# REMOVE HIGH-ERROR POSITIONS
#-------------------------------------------------------------------------------
print 'removing high-error positions'
gps_fall = gps_fall[gps_fall.dEast < limit] 
gps_fall = gps_fall[gps_fall.dNorth < limit]

#-------------------------------------------------------------------------------
# RE-INDEX GPS DATA
#-------------------------------------------------------------------------------
print 're-index gps data'

# This produces a continuous datetime index, with NaNs inserted for missing data.
gps_fall_NaNs = gps_fall.reindex(pd.date_range(start=min(gps_fall.index), end=max(gps_fall.index), freq='30S'))

#counting null values in the data:
#This represents low-quality data dropped in the Track processing + periods where no data was recorded.
NaNcount_North = gps_fall_NaNs.North.isnull().sum()
fractNaNNorth = float(NaNcount_North) / float(len(gps_fall_NaNs.North))
NaNcount_East = gps_fall_NaNs.East.isnull().sum()
fractNaNEast = float(NaNcount_East) / float(len(gps_fall_NaNs.East))
#NaNcount_Up = d_date_NaNs.Up.isnull().sum()
#fractNaNUp = float(NaNcount_Up) / float(len(d_date_NaNs.Up))
print '------------------------------'
print "%s NaN values in North" %NaNcount_North
print "%f of data" %fractNaNNorth 
print "%s NaN values in East" %NaNcount_East 
print "%f of data" %fractNaNEast
#print "%s NaN values in Up" %NaNcount_Up
#print "%f of data" %fractNaNUp
print '------------------------------'

#-------------------------------------------------------------------------------
# INTERPOLATE (continuous data needed for position smoothing)
#-------------------------------------------------------------------------------
print 'interpolate gps data'

North_interp = gps_fall_NaNs.North.interpolate(method='time')
East_interp = gps_fall_NaNs.East.interpolate(method='time')
DOY_interp = gps_fall_NaNs.DOY.interpolate(method='time')

#-------------------------------------------------------------------------------
# GAUSSIAN SMOOTHING
#-------------------------------------------------------------------------------

# Note: This code is based on the "Convolution with numpy" routine presented here:
# http://glowingpython.blogspot.com/2012/02/convolution-with-numpy.html 

# 'con_shift' centers the 'valid' convolution (see numpy doc) with the original data, creating (window/2) 
# NaNs on either end of the smoothed curve. This results in a smoothed curve that is the same length as the 
# original data. To have equal NaNs on either end, and to have resulting datasets of the same length, window size
# must be an odd value!

def smooth_gaussian(data,window,std):
  g = sp.signal.gaussian(window,std,sym=True)
  con = np.convolve(g/g.sum(),data,mode='valid')
  con_shift = np.r_[np.full((window/2),fill_value=np.NaN), # NOTE: '/2' instead of '*0.5' 
                    con,
                    np.full((window/2),fill_value=np.NaN)]
  return con_shift

data_north = North_interp
data_east = East_interp

# (+ 1) gaurantees odd (i.e. 1440 --> 1441 data entries for 6 hrs):
window = ((smoothing_window_hrs * 60 * 60) / 30) + 1 # 30-sec data

# std dev, controls gaussian window shape
std = 0.3 * window

print 'smoothing positions...'

data_north_smooth = smooth_gaussian(data_north,window,std)
data_east_smooth = smooth_gaussian(data_east,window,std)

# EXPORT SMOOTHED POSITION DATA:
#data = {'Fract_DOY':Fract_DOY_interp, 'dNorth_smooth':data_north_smooth, 'dEast_smooth':data_east_smooth}
#df = pd.DataFrame(data=data, index=Fract_DOY_interp.index)
#df.to_csv("%s_positions_6smooth.csv" % station,columns=['Fract_DOY','dNorth_smooth','dEast_smooth'],index=True)

#-------------------------------------------------------------------------------
# CALCULATE GPS VELOCITY
#-------------------------------------------------------------------------------
print 'calculating velocities...'

# 30 sec:
def vector(positions):
	vector = positions[1:] - positions[:-1]
	return vector

# 6 hr:
def vector6(positions):
	vector = positions[720:] - positions[:-720] # (6 hr * 60 min * 60 sec) / 30 sec
	return vector

# 30-sec:
east_vector = vector(data_east_smooth)
north_vector = vector(data_north_smooth)

total_vector = ((east_vector**2) + (north_vector**2))**(0.5)
velocity_ms = (total_vector) / 30. # 30-sec data
velocity_mday = (velocity_ms) * 60. * 60. * 24.
#velocity_mday_mean = np.mean(velocity_mday)
velocity_myear = velocity_mday * 365.

# 6-hr:
east_vector6 = vector6(data_east_smooth)
north_vector6 = vector6(data_north_smooth)

total_vector6 = ((east_vector6**2) + (north_vector6**2))**(0.5)
velocity_ms6 = (total_vector6) / 21600. # 6 hr * 60 min * 60 sec
velocity_mday6 = (velocity_ms6) * 60. * 60. * 24.
velocity_myear6 = velocity_mday6 * 365.

#------------------------------------------------------------------------------
# SMOOTH THE VELOCITY CURVE (optional)
#------------------------------------------------------------------------------
print 'smoothing velocities...'

# (+ 1) gaurantees odd (i.e. 1440 --> 1441 data entries for 6 hrs):
window_velocity = ((smoothing_window_hrs_velocity * 60 * 60) / 30) + 1

# optional, enter smoothing window size in minutes
#smoothing_window_mins_velocity = 10
#window_velocity = ((smoothing_window_mins_velocity * 60) / 30) + 1

std = 0.1 * window_velocity # tighter gaussian will retain peaks better
 
velocity_ms_smooth = smooth_gaussian(velocity_ms,window_velocity,std)
velocity_mday_smooth = (velocity_ms_smooth) * 60. * 60. * 24.
#velocity_mday_mean_smooth = np.mean(velocity_mday_smooth)
velocity_myear_smooth = velocity_mday_smooth * 365.

#-------------------------------------------------------------------------------
# Gaussian smoothing routine strips series of datetime index.
# The following gets datetime-indexed DOY series to match the velocity product,
# and creates a new datetime-indexed dataframe:

# 30-sec velocity:
last_index = (DOY_interp.last('30S')).index[0]
DOY_drop = DOY_interp.drop(last_index, axis=0)
data = {'DOY_drop':DOY_drop, 'velocity_myear':velocity_myear}
df = pd.DataFrame(data=data, index=DOY_drop.index)

# smoothed 30-sec velocity:
data_smooth = {'DOY_drop':DOY_drop, 'velocity_myear_smooth':velocity_myear_smooth}
df_smooth = pd.DataFrame(data=data_smooth, index=DOY_drop.index)

df_smooth.to_csv("27km-11_30sec_smooth.csv",columns=['DOY_drop','velocity_myear_smooth'],index=True) # EXPORT

# 6-hr velocity:
DOY_drop6 = DOY_interp[(DOY_interp.first('10830S').index[-1]) : (DOY_interp.last('10830S').index[0])] # (6 hr * 60 min * 60 sec / 2) + 30 sec
data6 = {'DOY_drop6':DOY_drop6, 'velocity_myear6':velocity_myear6}
df6 = pd.DataFrame(data=data6, index=DOY_drop6.index)

df6.to_csv("27km-11_6hr.csv",columns=['DOY_drop6','velocity_myear6'],index=True) # EXPORT

#-------------------------------------------------------------------------------
# PLOT RAW DATA
#-------------------------------------------------------------------------------

##plot raw positions, colored by DOY:
#fig1 = plt.figure()
#ax = fig1.add_subplot(111)
#ax.scatter(gps.East,gps.North,c=gps.DOY,cmap=plt.cm.coolwarm, marker='.',s=75)
#ax.set_xlabel('East position (m)')
#ax.set_ylabel('North position (m)')
#ax.set_title('RAW DATA: 27km-11')

##majorLocator   = MultipleLocator(1)
##majorFormatter = FormatStrFormatter('%d')
##minorLocator   = MultipleLocator(0.5)

##plot raw East component
#fig2 = plt.figure()
#ax = fig2.add_subplot(111)
#ax.scatter(gps.index,gps.East, marker='.',s=75)
##ax.set_xlabel('')
#ax.set_ylabel('East position (m)')
##ax.xaxis.set_major_locator(majorLocator)
##ax.xaxis.set_major_formatter(majorFormatter)
##ax.xaxis.set_minor_locator(minorLocator)
#ax.xaxis.grid(True, which='major', color='black')
#ax.xaxis.grid(True, which='minor', color='gray')
#ax.set_title('RAW DATA (East): GPS')

##plot raw North component
#fig3 = plt.figure()
#ax = fig3.add_subplot(111)
#ax.scatter(gps.index,gps.North, marker='.',s=75)
##ax.set_xlabel('')
#ax.set_ylabel('North position (m)')
##ax.xaxis.set_major_locator(majorLocator)
##ax.xaxis.set_major_formatter(majorFormatter)
##ax.xaxis.set_minor_locator(minorLocator)
#ax.xaxis.grid(True, which='major', color='black')
#ax.xaxis.grid(True, which='minor', color='gray')
#ax.set_title('RAW DATA (North)')

#------------------------------------------------------------------------------
# PLOT VELOCITY:
#------------------------------------------------------------------------------

majorLocator   = MultipleLocator(1)
majorFormatter = FormatStrFormatter('%d')
minorLocator   = MultipleLocator(0.5)

# plot velocity
fig10 = plt.figure(figsize=(20,8))
ax = fig10.add_subplot(111)

ax.plot(df.index,df.velocity_myear, color='black')
ax.plot(df_smooth.index,df_smooth.velocity_myear_smooth, color='red') 
ax.plot(df6.index,df6.velocity_myear6, color='c')

ax.set_xlabel('DOY (UTC)')
ax.set_ylabel('velocity (m/yr)')
#ax.set_xlim([196,202])
ax.set_ylim([0,500])
#ax.xaxis.set_major_locator(majorLocator)
#ax.xaxis.set_major_formatter(majorFormatter)
#ax.xaxis.set_minor_locator(minorLocator)
ax.xaxis.grid(True, which='major', color='black')
#ax.xaxis.grid(True, which='minor', color='gray')
ax.set_title('GPS 27km-11')
fig10.tight_layout(pad=1.25)

#fig10.savefig('July2014_%s.png' % station,dpi=100)

# TO DROP HIGH VELOCITIES:
#s = pd.Series(velocity_myear)
#vel_outliers = s[(s > 300.)]
#vel_outliers[:] = 0.
#velocity_myear_removeout = vel_outliers.combine_first(s)
#velocity_myear_removeout[velocity_myear_removeout==0.] = np.nan

plt.show()