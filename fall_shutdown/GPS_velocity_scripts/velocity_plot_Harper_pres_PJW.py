# -*- coding: utf-8 -*-
"""
Created on Wed Aug 16 09:19:12 2017

Modified: P. Wright, 12/11/2017

@author: Dr. Joel Brown PhD
"""

import matplotlib.pyplot as plt
import numpy as np

from IPython import embed

## Clear data if esists
if 'gps' in locals():
    del gps
    

""" Define data read and write functions """

## Load pressure data functions

def loadPdata(filenam):
    with open(filenam,"r") as f:
        pData=np.loadtxt(f,delimiter=',')
        return pData
    
def loadGPSdata(filenam):
    with open(filenam,"r") as f:
        pData=np.loadtxt(f, skiprows=1,delimiter=',')
        return pData

""" 
--------------------------------------------
      Define binning function                   
if data in DOY, timeMultiplier = 24(60/tbin)
where tbin == number of minutes to bin over 
timeMultiplier == 96    --> 15 min
timeMultiplier == 144   --> 10 min
timeMultiplier == 48    --> 30 min
timeMultiplier == 4  --> 6 hrs
NOTE: the calculation of timeMultiplier is independent of GPS measurement
interval (i.e. 15s or 30s)
-------------------------------------------- 
"""
    
def binDataByTime(gpsDat,timeMultiplier):
    dailyAveGPS=[]
    gpsint, ind = np.unique(np.floor(gpsDat[:,0]*timeMultiplier),return_index="true")     
    for ii in range(len(ind)-1):
        dailyAveGPS.append(np.median(gpsDat[ind[ii]:ind[ii+1],:],axis=0))
    dailyAveGPS.append(np.median(gpsDat[ind[ii+1]:-1,:],axis=0))
    dailyAveGPS=np.asarray(dailyAveGPS)
    return dailyAveGPS
    
""" 
----------------------------------------------
      Define smoothing function
the window length for this function is number 
of samples in the window.
window_len = smooth length *(60/dt)
dt      |smooth length  |window_len
15 min  |6 hr           |24
10 min  |6 hr           |36
40 min  |12 hr          |18
----------------------------------------------
"""

def smooth(x,window_len=24,window='hamming'):
        s=np.r_[2*x[0]-x[window_len-1::-1],x,2*x[-1]-x[-1:-window_len:-1]]
        if window == 'flat': #moving average
                w=np.ones(window_len,'d')
        else:  
                w=eval('np.'+window+'(window_len)')
        y=np.convolve(w/w.sum(),s,mode='same')
        return y[window_len:-window_len+1]

"""
------------------------------------------------------------------------
Velocity Calculation over longer time periods - Bartholomew method
dt and dvel are in hours
dt is data interval time in hours (ie. dt=0.25 --> every 15 min.)
dvel is calculation time interval (i.e. dvel=4 --> ave vel over 4 hours)
------------------------------------------------------------------------
"""

def calculateVelocity(gpsDat,dt=.25,dvel=6):
	win=np.zeros(int(dvel/dt))
	win[0]=-1
	win[-1]=1
	xdiff = np.convolve(gpsDat[:,1],win,mode='same')
	ydiff = np.convolve(gpsDat[:,2],win,mode='same')	
	tdiff = np.convolve(gpsDat[:,0],win,mode='same')
	velx=xdiff/tdiff
	vely=ydiff/tdiff
	return velx, vely


""" Load data """

site = 'GL11-1'
print("Loading GL11-1 Data")
gps1 = loadGPSdata("GPS_Pressure_Data_Organized/GL11_1/GL11-1_GPS.txt")  # Takes about 2 minutes (Center, Block Site)
            

""" ---------------------------- """
""" Deal with GPS data           """
""" ---------------------------- """

""" Bin position data 
# 96 == 15 minutes 
# 48 == 30 minutes 
# 24 == 1 hour

"""
print("Bin data on 15 minute intervals")
gpssm = binDataByTime(gps1,96)

""" Smooth position data """
print("Apply 6 hour smoothing")
# 720 == 6 hrs @ 30 sec intervals
# 24 == 6 hrs @ 15 min intervals
gpssm[:,1] = smooth(gpssm[:,1],24)  
gpssm[:,2] = smooth(gpssm[:,2],24)  

""" Calculate velocity """
print("Calculating velocity over six-hour spans")
xVelsm, yVelsm = calculateVelocity(gpssm,.25,6)
smVel = np.sqrt(xVelsm**2+yVelsm**2)

embed()

plt.figure(1)
plt.clf()
fg=plt.gcf()
fg.set_figheight(6)
fg.set_figwidth(6)
plt.plot(gpssm[:,0],smVel*365.25,'r')
plt.plot(gpssm[:,0],smVel*365.25,'k.')
plt.ylabel('Velocity (m/yr)')
plt.xlabel('DOY from 2011-1-1')
plt.title('Seasonal velocity variation')
axes = plt.gca()
axes.set_ylim([0,400])
plt.grid()

plt.show()
""" Save the figure """

#plt.savefig('GPS_Vel'+ site +'.png',bbox_inches='tight',pad_inches=0.2)




