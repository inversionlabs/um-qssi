# -*- coding: utf-8 -*-
"""
Created on Wed Aug 16 09:19:12 2017


@author: Dr. Joel Brown PhD
Property of Aesir Consulting LLC 
"""

import argparse
import matplotlib.pyplot as plt
import numpy as np
from scipy import spatial
#import scipy.ndimage
#from itertools import product

parser = argparse.ArgumentParser(description='Determine site to process')
parser.add_argument('sitenam', metavar='site', type=str, nargs='+',
                   help='Site name')
args = parser.parse_args()


## Clear data if esists
if 'gps' in locals():
    del gps
    
if 'pDataC' in locals():
    del pDataC

""" Define data read and write functions """

## Load pressure data functions

def loadPdata(filenam):
    with open(filenam,"r") as f:
        pData=np.loadtxt(f,delimiter=',')
        return pData
    
def loadGPSdata(filenam):
    with open(filenam,"r") as f:
        pData=np.loadtxt(f, skiprows=1,delimiter=',')
        return pData
    
def smooth(x,window_len=20,window='hamming'):
        s=np.r_[2*x[0]-x[window_len-1::-1],x,2*x[-1]-x[-1:-window_len:-1]]
        if window == 'flat': #moving average
                w=np.ones(window_len,'d')
        else:  
                w=eval('np.'+window+'(window_len)')
        y=np.convolve(w/w.sum(),s,mode='same')
        return y[window_len:-window_len+1]
    
def findnearesttime(t1,t2):
    posMap = np.dstack([t1, np.zeros(np.asarray(t1).shape)])[0]
    points = np.dstack([t2, np.zeros(np.asarray(t2).shape)])[0]
    mytree=spatial.cKDTree(posMap)
    dist, indices = mytree.query(points)
    return dist, indices
    
def timeavepos(indx,dat,dt,pDataC):
    ind = indx
    datx = []
    pdat = []
    for ii in range (0,np.max(ind)):
        if dt[ii] <= 1.0105:
           datx.append(np.median(dat[np.subtract(np.where(indx==ii),1)]))
           pdat.append(pDataC[ii])
    return datx, pdat

def binDataByTime(gpsDat,timeMultiplier=4):
    dailyAveGPS=[]
    gpsint, ind = np.unique(np.floor(gpsDat[:,0]*timeMultiplier),return_index="true")     
    for ii in range(len(ind)-1):
        dailyAveGPS.append(np.median(gpsDat[ind[ii]:ind[ii+1],:],axis=0))
    dailyAveGPS.append(np.median(gpsDat[ind[ii+1]:-1,:],axis=0))
    dailyAveGPS=np.asarray(dailyAveGPS)
    return dailyAveGPS

### finds track errors that are too large and removes the data associated with them
def removeBadGPS(gpsDat,dx=0.05,dy=0.05,dz=0.1):
#    gdGPS=[]
    goodGPS = []
    for ii in range (len(gpsDat[:,0])):
        if gpsDat[ii,4]<dx and gpsDat[ii,5]<dy and gpsDat[ii,6]<dz:
            goodGPS.append(gpsDat[ii,:])
    goodGPS=np.asarray(goodGPS)
#    dgpsx=np.diff(gdGPS[:,1])
#    dgpsy = np.diff(gdGPS[:,2])
#    for ii in range (len(gdGPS[0:-1,0])):
#        if dgpsx[ii] < 0.01 and dgpsy[ii] <0.01:
#            goodGPS.append(gdGPS[ii,:])
#    goodGPS=np.asarray(goodGPS)
    return goodGPS

def calculateVelocity(gpsDat,dt=.25,dvel=4):
	win=np.zeros(dvel/dt)
	win[0]=-1
	win[-1]=1
	xdiff = np.convolve(gpsDat[:,1],win,mode='same')
	ydiff = np.convolve(gpsDat[:,2],win,mode='same')	
	tdiff = np.convolve(gpsDat[:,0],win,mode='same')
	velx=xdiff/tdiff
	vely=ydiff/tdiff
	return velx, vely
	
#def removeHighVelocity(gpsDat):
#    goodGPS = []
#    for ii in range (len(gpsDat[:,0])):
#        if 


""" Load data """
## 2011-1

site = args.sitenam[0]
if site == 'North':
    print("Loading Grid-N Data")
    gps1 = loadGPSdata("GL_grid_N_GPS.txt")  # Takes about 2 minutes
    print("Loading pressure data")
    pDataB = loadPdata("GL_grid_15_N_Pw_0m.txt")
    pDataC = loadPdata("GL_grid_14_N_Pw.txt")
    depthb = 635
    depthc = 640
elif site =='South':
    print("Loading Grid-S Data")
    gps1 = loadGPSdata("GL_grid_S_GPS.txt")  # Takes about 2 minutes
    print("Loading pressure data")
    pDataB = loadPdata("GL_grid_14_S_Pw_H1.txt")
    pDataC = loadPdata("GL_grid_14_S_Pw_H2.txt")
    pDataD = loadPdata("GL_grid_15S_hifreq_5min_avg.txt")
    depthb = 677
    depthc = 674
    depthd = 677
elif site =='East':
    print("Loading Grid-E Data")
    gps1 = loadGPSdata("GL_grid_E_GPS.txt")  # Takes about 2 minutes
    print("Loading pressure data")
    pDataB = loadPdata("GL_grid_15_E_B_Pw_5m.txt") 
    depthb = 660
elif site =='West':
    print("Loading Grid-W Data")
    gps1 = loadGPSdata("GL_grid_W_GPS.txt")  # Takes about 2 minutes
    print("Loading pressure data")
    pDataB = loadPdata("GL_grid_14_W_Pw.txt")
    depthb = 661
elif site =='Center':
    print("Loading Grid-C Data")
    gps1 = loadGPSdata("GL_grid_C_GPS.txt")  # Takes about 2 minutes
    print("Loading pressure data")
    pDataB = loadPdata("GL_grid_15_C_B_Pw_1m.txt")
    pDataC = loadPdata("GL_grid_15CA_hifreq_5min_avg.txt")
    depthb = 675
    depthc = 671
elif site =='GL11-1':
    print("Loading GL11-1 Data")
    gps1 = loadGPSdata("GL11-1_GPS.txt")  # Takes about 2 minutes
    print("Loading pressure data")
    pDataB = loadPdata("GL11-1_Ps_A.txt")
    pDataC = loadPdata("GL11-1_Ps_B.txt")
    pDataD = loadPdata("GL11-1_Ps_C.txt")
    depthb = 457.5
    depthc = 466
    depthd = 459.5
elif site =='GL11-2':
    print("Loading GL11-2 Data")
    gps1 = loadGPSdata("GL11-2_GPS.txt")  # Takes about 2 minutes
    print("Loading pressure data")
    pDataB = loadPdata("GL11-2_Ps_B.txt")
    pDataC = loadPdata("GL11-2_Ps_D.txt")
    depthb = 821
    depthc = 814.5
elif site =='GL12':
    print("Loading GL12 Data")
    gps1 = loadGPSdata("GL12_GPS.txt")  # Takes about 2 minutes
    print("Loading pressure data")
    pDataB = loadPdata("GL12_A.txt")
    pDataC = loadPdata("GL12_C.txt")
    pDataD = loadPdata("GL12_D.txt")
    depthb = 695.5
    depthc = 688
    depthd = 696
"""elif site == 'GL12_GPS':
	print("Loading GL11-2 Data")
    gps1 = loadGPSdata("GL12_GPS.txt")  # Takes about 2 minutes
    print("Loading pressure data")
    pDataB = loadPdata("GL12_A.txt")
    pDataC = loadPdata("GL12_D.txt")"""

            
#%%
""" ---------------------------- """
""" Deal with GPS data           """
""" ---------------------------- """



""" Bin position data 
# 60 == 15 minutes @ 15 sec intervals
# 40 == 10 minutes @ 15 sec intervals

"""
print("Bin data on 15 minute intervals")
gps = binDataByTime(gps1,96)  
gpssm = binDataByTime(gps1,96)

""" Smooth position data """
print("Apply 6 hour smoothing")
gpssm[:,1] = smooth(gpssm[:,1],24)  # 720 == 6 hrs @ 30 sec intervals
gpssm[:,2] = smooth(gpssm[:,2],24)  # 24 == 6 hrs @ 15 min intervals



""" Calculate velocity """
print("Calculating velocity over three hour spans")
xVelsm, yVelsm = calculateVelocity(gpssm,.25,3)
smVel = np.sqrt(xVelsm**2+yVelsm**2)

xVel, yVel = calculateVelocity(gps,.25,3)
magVel = np.sqrt(xVel**2+yVel**2)



""" ---------------------------- """
""" Deal with Pressure data      """
""" ---------------------------- """

""" Bin position data 

# 96 == 15 minutes @ 15 sec intervals

# 144 == 10 minutes @ 15 sec intervals

"""
print("Bin data on 15 minute intervals")
#pdat = binDataByTime(pDataB,96)  
pdatsm = binDataByTime(pDataB,96)

""" Smooth position data """
print("Apply 6 hour smoothing")
pdatsm[:,1] = smooth(pdatsm[:,1],24)  # 12 == 6 hrs @ 30 sec intervals
#gpssm[:,2] = smooth(pdatsm[:,2],24)  # 24 == 6 hrs @ 15 min intervals



#%%
minx=np.min((np.min(pDataB[:,0]),np.min(gps[:,0])))
maxx=np.max((np.max(pDataB[:,0]),np.max(gps[:,0])))
figwid=360/(2600/(maxx-minx+10))
plt.figure(1)
plt.clf()
fg=plt.gcf()
fg.set_figheight(10)
fg.set_figwidth(figwid)
plt.subplot(2,1,1)
plt.plot(gps[:,0],magVel*365.25,'k.')
plt.plot(gpssm[:,0],smVel*365.25,'r.-')
plt.ylabel('Velocity (m/yr)')
#plt.xlabel('DOY from 2011-1-1')
plt.title(site)
axes = plt.gca()
#axes.set_xlim([200,220])
axes.set_xlim([minx-5,maxx+5])
axes.set_ylim([0,400])
plt.grid()

plt.subplot(2,1,2)
overburdenb = np.multiply(np.divide(np.asarray(pdatsm[:,1]),np.multiply(depthb,0.9)),100)
plt.plot(pdatsm[:,0],overburdenb,'.')
#plt.plot(pDataD[:,0],pDataD[:,1],'.')
plt.ylabel('Percent overburden pressure')
plt.xlabel('DOY from 2011-1-1')
#plt.legend(['Hole B', 'Hole D'])
axes = plt.gca()
##axes.set_xlim([200,220])
axes.set_xlim([minx-5,maxx+5])
miny=np.nanmin(pdatsm[:,1])
maxy=np.nanmax(pdatsm[:,1])
plt.grid()
#

if site == "North":
    print("Bin data on 15 minute intervals")
    pdatsmc = binDataByTime(pDataC,96)
    """ Smooth position data """
    print("Apply 6 hour smoothing")
    pdatsmc[:,1] = smooth(pdatsmc[:,1],24)
    overburdenc = np.multiply(np.divide(np.asarray(pdatsmc[:,1]),np.multiply(depthc,0.9)),100)
    plt.plot(pdatsmc[:,0],overburdenc,'.')
    miny=np.min((np.nanmin(pdatsmc[:,1]),miny))
    maxy=np.max((np.nanmax(pdatsmc[:,1]),maxy))
    plt.legend(['2015 (635 m)', '2015 (640)'])
#    print("Bin data on 15 minute intervals")
#    pdatsmd = binDataByTime(pDataD,96)
#    
#    """ Smooth position data """
#    print("Apply 6 hour smoothing")
#    pdatsmd[:,1] = smooth(pdatsmd[:,1],24)
#    plt.plot(pdatsmd[:,0],np.multiply(np.multiply(depth,0.9)-np.asarray(pdatsmd[:,1]),9.807),'.') 
#    miny=np.min((np.nanmin(pdatsmd[:,1]),miny))
#    maxy=np.max((np.nanmax(pdatsmd[:,1]),maxy))   
#    plt.legend(['2015 5m','2015 0 m','2014'])
elif site == "South":
    print("Bin data on 15 minute intervals")
    pdatsmc = binDataByTime(pDataC,96)
    
    """ Smooth position data """
    print("Apply 6 hour smoothing")
    pdatsmc[:,1] = smooth(pdatsmc[:,1],24)
    overburdenc = np.multiply(np.divide(np.asarray(pdatsmc[:,1]),np.multiply(depthc,0.9)),100)
    plt.plot(pdatsmc[:,0],overburdenc,'.')
    miny=np.min((np.nanmin(pdatsmc[:,1]),miny))
    maxy=np.max((np.nanmax(pdatsmc[:,1]),maxy))
    
    print("Bin data on 15 minute intervals")
    pdatsmd = binDataByTime(pDataD,96)
    
    """ Smooth position data """
    print("Apply 6 hour smoothing")
    pdatsmd[:,1] = smooth(pdatsmd[:,1],24)
    overburdenc = np.multiply(np.divide(np.asarray(pdatsmc[:,1]),np.multiply(depthd,0.9)),100)
    miny=np.min((np.nanmin(pdatsmd[:,1]),miny))
    maxy=np.max((np.nanmax(pdatsmd[:,1]),maxy))
    plt.legend(['2014-1 (667 m)', '2014-2 (674 m)', '2015-hf (667 m)'])

elif site == 'East':
    plt.legend(['2015 (660 m)'])
    
elif site == 'West':
    plt.legend(['2014 (661 m)'])
    
elif site == 'Center':
    print("Bin data on 15 minute intervals")
    pdatsmc = binDataByTime(pDataC,96)
    
    """ Smooth position data """
    print("Apply 6 hour smoothing")
    pdatsmc[:,1] = smooth(pdatsmc[:,1],24)
    overburdenc = np.multiply(np.divide(np.asarray(pdatsmc[:,1]),np.multiply(depthc,0.9)),100)
    plt.plot(pdatsmc[:,0],overburdenc,'.')
    miny=np.min((np.nanmin(pdatsmc[:,1]),miny))
    maxy=np.max((np.nanmax(pdatsmc[:,1]),maxy))    
    plt.legend(['2015 (675 m)', '2015-hf (671 m)'])

elif site == "GL11-1":
    print("Bin data on 15 minute intervals")
    pdatsmc = binDataByTime(pDataC,96)
    
    """ Smooth position data """
    print("Apply 6 hour smoothing")
    pdatsmc[:,1] = smooth(pdatsmc[:,1],24)
    overburdenc = np.multiply(np.divide(np.asarray(pdatsmc[:,1]),np.multiply(depthc,0.9)),100)
    plt.plot(pdatsmc[:,0],overburdenc,'.')
    miny=np.min((np.min(pdatsmc[:,1]),miny))
    maxy=np.max((np.max(pdatsmc[:,1]),maxy))
    print("Bin data on 15 minute intervals")
    pdatsmd = binDataByTime(pDataD,96)
    
    """ Smooth position data """
    print("Apply 6 hour smoothing")
    pdatsmd[:,1] = smooth(pdatsmd[:,1],24)
    overburdend = np.multiply(np.divide(np.asarray(pdatsmd[:,1]),np.multiply(depthd,0.9)),100)
    plt.plot(pdatsmd[:,0],overburdend,'.')
    miny=np.min((np.nanmin(pdatsmd[:,1]),miny))
    maxy=np.max((np.nanmax(pdatsmd[:,1]),maxy))    
    plt.legend(['2011-A (457.5 m)','2011-B (466 m)','2011-C (459.5 m)'])
elif site == "GL11-2":
    print("Bin data on 15 minute intervals")
    pdatsmc = binDataByTime(pDataC,96)
    
    """ Smooth position data """
    print("Apply 6 hour smoothing")
    pdatsmc[:,1] = smooth(pdatsmc[:,1],24)
    overburdenc = np.multiply(np.divide(np.asarray(pdatsmc[:,1]),np.multiply(depthc,0.9)),100)
    plt.plot(pdatsmc[:,0],overburdenc,'.')
    miny=np.min((np.min(pdatsmc[:,1]),miny))
    maxy=np.max((np.max(pdatsmc[:,1]),maxy))
    plt.legend(['2011-B (821 m)', '2011-D (814.5 m)'])
elif site == "GL12":
    print("Bin data on 15 minute intervals")
    pdatsmc = binDataByTime(pDataC,96)
    
    """ Smooth position data """
    print("Apply 6 hour smoothing")
    pdatsmc[:,1] = smooth(pdatsmc[:,1],24)
    overburdenc = np.multiply(np.divide(np.asarray(pdatsmc[:,1]),np.multiply(depthc,0.9)),100)
    plt.plot(pdatsmc[:,0],overburdenc,'.')
    miny=np.min((np.nanmin(pdatsmc[:,1]),miny))
    maxy=np.max((np.nanmax(pdatsmc[:,1]),maxy))
    print("Bin data on 15 minute intervals")
    pdatsmd = binDataByTime(pDataD,96)
    
    """ Smooth position data """
    print("Apply 6 hour smoothing")
    pdatsmd[:,1] = smooth(pdatsmd[:,1],24)
    overburdend = np.multiply(np.divide(np.asarray(pdatsmd[:,1]),np.multiply(depthd,0.9)),100)
    plt.plot(pdatsmd[:,0],overburdend,'.')
    miny=np.min((np.nanmin(pdatsmd[:,1]),miny))
    maxy=np.max((np.nanmax(pdatsmd[:,1]),maxy))    
    plt.legend(['2012-A (695.5 m)','2012-C (688 m)','2012-D (696 m)'])
    

#axes.set_ylim([miny, maxy])

#plt.show()
plt.savefig('GPS_Ps'+ site +'.png',bbox_inches='tight',pad_inches=0.2)



#%%

""" old code """

""" Remove data with large dx,dy """

#print("Remove data with large position error")

#gps = removeBadGPS(gps1,0.05,0.05,0.1)


#gps[np.isnan(gps)]=0
#gps=removeBadGPS(gps)
#gps=gps[np.abs(np.diff(gps[:,1].append(gps[1,:])))<0.0031,:]
#gps=gps[np.abs(np.diff(gps[0:-0,2]))<0.0031,:]
#gps[gps==0] = np.nan


#pDataC = binDataByTime(pDataC)


#xVel[np.isnan(xVel)]=0
#smxVel = smooth(xVel,25)

#yVel[np.isnan(yVel)]=0
#smyVel = smooth(yVel,25)



#gps=gps[smVel<np.divide(500,325.25),:]
#smVel=smVel[smVel<np.divide(500,325.25)]

#smpos = np.sqrt(gps[:,1]**2+gps[:,2]**2)
#smv = np.divide(np.abs(np.diff(smpos)),np.diff(gps[:,0]))




#gps[gps==0] = np.nan
#smVel[smVel==0] = np.nan
#dt, ind = findnearesttime(pDataC[:,0],gps[:,0])
#dx, pData = timeavepos(ind,smVel,dt,pDataC[:,1])
#depth = 457.5
