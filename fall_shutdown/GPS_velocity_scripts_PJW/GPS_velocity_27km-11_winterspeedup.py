#!/usr/bin/env python

"""
GPS post-processing (velocity calculations)
Patrick Wright, Inversion Labs
adapted from Joel Brown, Aesir Consulting
Job: UM QSSI
Dec. 2017
"""

import numpy as np
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import datetime as dt

# ------------------------------------------------------------------------------
# DEFINE FILE PATH AND SITE NAME FOR POSITION DATA:
# ------------------------------------------------------------------------------

filepath = "../GPS_Pressure_Data_Organized/GL11_1/GL11-1_GPS.txt"
site = '27km-11'

# ------------------------------------------------------------------------------
# DEFINE FUNCTIONS:
# ------------------------------------------------------------------------------
  
# Convert DOY to datetime (2011):
def dateparser_2011(doy):
  return dt.datetime(year=2011, month=1, day=1) + dt.timedelta(days=doy) - dt.timedelta(days=1)

# set index of Pandas dataframe to datetime:
def set_index(df):
  index = pd.to_datetime(df.Datetime)
  df = df.set_index(index)
  df.index.rename('Datetime_index', inplace=True)
  df = df.drop('Datetime', axis=1)
  return df

# Make a histogram:
def histoplot(data, num_bins, titlestring):
    fig1 = plt.figure(figsize=(6*1.618, 6))
    ax1 = fig1.add_subplot(111)
    n, bins, patches = plt.hist(data, num_bins, facecolor='blue', edgecolor='k', alpha=0.5)
    ax1.set_title(titlestring, fontsize=12)
    ax1.set_xlabel('error', fontsize=12)
    ax1.set_ylabel('samples', fontsize=12)
    ax1.set_xlim(0,0.1)
    plt.xticks(fontsize=12)
    plt.yticks(fontsize=12)

#-------------------------------------------------------------------------------
# Joel Brown's loading / binning / smoothing / velocity functions:

""" Define data read and write functions """
    
def loadGPSdata(filenam):
    with open(filenam,"r") as f:
        pData=np.loadtxt(f, skiprows=1,delimiter=',')
        return pData

""" 
--------------------------------------------
      Define binning function                   
if data in DOY, timeMultiplier = 24(60/tbin)
where tbin == number of minutes to bin over
timeMultiplier == 144   --> 10 min
timeMultiplier == 96    --> 15 min
timeMultiplier == 48    --> 30 min
timeMultiplier == 24    --> 1 hr
timeMultiplier == 8     --> 3 hrs
timeMultiplier == 4     --> 6 hrs
NOTE: the calculation of timeMultiplier is independent of GPS measurement
interval (i.e. 15s or 30s)
-------------------------------------------- 
"""
    
def binDataByTime(gpsDat,timeMultiplier):
    dailyAveGPS=[]
    gpsint, ind = np.unique(np.floor(gpsDat[:,0]*timeMultiplier),return_index="true")     
    for ii in range(len(ind)-1):
        dailyAveGPS.append(np.median(gpsDat[ind[ii]:ind[ii+1],:],axis=0))
    dailyAveGPS.append(np.median(gpsDat[ind[ii+1]:-1,:],axis=0))
    dailyAveGPS=np.asarray(dailyAveGPS)
    return dailyAveGPS
    
""" 
----------------------------------------------
      Define smoothing function
the window length for this function is number 
of samples in the window.
window_len = smooth length *(60/dt)
dt      |smooth length  |window_len
15 min  |6 hr           |24
30 min  |6 hr           |12
10 min  |6 hr           |36
40 min  |12 hr          |18
----------------------------------------------
"""

def smooth(x,window_len,window='hamming'):
        s=np.r_[2*x[0]-x[window_len-1::-1],x,2*x[-1]-x[-1:-window_len:-1]]
        if window == 'flat': #moving average
          w=np.ones(window_len,'d')
        else:  
          w=eval('np.'+window+'(window_len)')
        y=np.convolve(w/w.sum(),s,mode='same')
        return y[window_len:-window_len+1]

"""
------------------------------------------------------------------------
Velocity Calculation over longer time periods - Bartholomew method
dt and dvel are in hours
dt is data interval time in hours (ie. dt=0.25 --> every 15 min., dt=0.5 --> every 30 min)
dvel is calculation time interval (i.e. dvel=4 --> ave vel over 4 hours)
------------------------------------------------------------------------
"""

def calculateVelocity(gpsDat,dt,dvel):
	win=np.zeros(int(dvel/dt))
	win[0]=-1
	win[-1]=1
	xdiff = np.convolve(gpsDat[:,1],win,mode='same')
	ydiff = np.convolve(gpsDat[:,2],win,mode='same')	
	tdiff = np.convolve(gpsDat[:,0],win,mode='same')
	velx=xdiff/tdiff
	vely=ydiff/tdiff
	return velx, vely

# ------------------------------------------------------------------------------
# MAIN PROGRAM (import data and perform binning/smoothing/velocity):
# ------------------------------------------------------------------------------

# JB positions (UM QSSI Box) (in UTC):
file2 = loadGPSdata(filepath)

# Make histograms of reported error:
#histoplot(file2[:,4], 200, 'North error')
#histoplot(file2[:,5], 200, 'East error')

# BROWN'S VELOCITY:
""" ---------------------------- """
""" Deal with GPS data           """
""" ---------------------------- """

# Define threshold limit for reported error (m):
limit = 0.02 # (m)

print 'removing high-error positions'
print ''
print 'fraction of North error > threshold:'
print np.float(len(file2[file2[:,4] > limit])) / np.float(len(file2[:,4]))
print ''
print 'fraction of East error > threshold:'
print np.float(len(file2[file2[:,5] > limit])) / np.float(len(file2[:,5]))
print''

# Remove outliers using threshold limit:
file2=file2[file2[:,4] < limit]
file2=file2[file2[:,5] < limit]

# Make histograms of reported error, with > threshold removed:
#histoplot(file2[:,4], 25, 'North error - remove high')
#histoplot(file2[:,5], 25, 'East error - remove high')

""" Bin position data 
# 96 == 15 minutes 
# 48 == 30 minutes 
# 24 == 1 hour

"""
print("Bin data on 30 minute intervals")
gpssm = binDataByTime(file2,48)

#-------------------------------------------------------------------------------

#plot binned positions, colored by DOY:
#fig1 = plt.figure()
#ax = fig1.add_subplot(111)
#ax.scatter(gpssm[:,2],gpssm[:,1],c=gpssm[:,0],cmap=plt.cm.coolwarm, marker='.',s=75)
#ax.set_xlabel('East position (m)')
#ax.set_ylabel('North position (m)')
#ax.set_title('Binned Position Data')

#-------------------------------------------------------------------------------
# EAST
#plot binned East component
#fig2 = plt.figure()
#ax = fig2.add_subplot(111)
#ax.scatter(gpssm[:,0],gpssm[:,2], marker='.',s=75)
#ax.set_ylabel('East position (m)')
#ax.xaxis.grid(True, which='major', color='black')
#ax.xaxis.grid(True, which='minor', color='gray')
#ax.set_title('Binned Position Data, 15 min (East)')

# NORTH
# plot binned North component
#fig3 = plt.figure()
#ax = fig3.add_subplot(111)
#ax.scatter(gpssm[:,0],gpssm[:,1], marker='.',s=75)
#ax.set_ylabel('North position (m)')
#ax.xaxis.grid(True, which='major', color='black')
#ax.xaxis.grid(True, which='minor', color='gray')
#ax.set_title('Binned Position Data, 15 min (North)')
#-------------------------------------------------------------------------------

""" Smooth position data """
print("Apply 12 hour smoothing to binned positions")

# window_len == smooth length *(60/dt)
# 720 == 6 hrs @ 30 sec intervals (6 hrs * (60 / 0.5))
#  24 == 6 hrs @ 15 min intervals (6 hrs * (60 / 15))
#  12 == 6 hrs @ 30 min intervals (6 hrs * (60 / 30))
#   6 == 6 hrs @ 1 hr intervals (6 hrs * (60 / 60))
#  32 == 8 hrs @ 15 min intervals (8 hrs * (60 / 15))
#  16 == 8 hrs @ 30 min intervals (8 hrs * (60 / 30))
#   8 == 8 hrs @ 1 hr intervals (8 hrs * (60 / 60))
#  24 == 12 hrs @ 30 min intervals (12 hrs * (60 / 30))
#   4 == 12 hrs @ 3 hr intervals (12 hrs * (60/ (3*60))

gpssm[:,1] = smooth(gpssm[:,1],24)  
gpssm[:,2] = smooth(gpssm[:,2],24)

""" Calculate velocity """
print("Calculating velocity over 12-hr spans")
xVelsm, yVelsm = calculateVelocity(gpssm,0.5,12) # 30 min data, 12-hr velocity
smVel = np.sqrt(xVelsm**2+yVelsm**2) # m/day

""" Smooth velocity """
print("Apply 12 hour smoothing to velocity")
smVel = smooth(smVel,24)

#-------------------------------------------------------------------------------
""" Create Pandas dataframe and export velocity data """

Brown_data = {'DOY':gpssm[:,0], 'velocity_myear':smVel*365.25} # m/year
df = pd.DataFrame(data=Brown_data)

Brown_index = df['DOY'].map(dateparser_2011) # make a datetime index
Brown_index.name = 'timestamp' # change name of index
Brown_data_dt = df.set_index(Brown_index) # set index to datetime, re-name
#Brown_data_dt.index = Brown_data_dt.index.round('min')

# Shift to local time:
Brown_data_dt.index = Brown_data_dt.index - dt.timedelta(hours=2)

# Drop high/low velocity:
#Brown_data_dt['velocity_myear'][Brown_data_dt['velocity_myear'] > 250.] = np.NaN
#Brown_data_dt['velocity_myear'][Brown_data_dt['velocity_myear'] < 60.] = np.NaN

# Export velocity data:
#Brown_data_dt.to_csv("%s_velocity_winter.csv" % site, columns=['DOY','velocity_myear'],index=True)
# ------------------------------------------------------------------------------

t1 = pd.to_datetime('2011-09-17')
t2 = pd.to_datetime('2011-11-14')

y1 = 79.3
y2 = 84.

x_1 = [t1, t2]
y_1 = [y1, y2]

m = (y2-y1) / (pd.Timedelta(t2-t1).days)

days_projected = 201
y_projected = m*days_projected + y2

print ''
print 'projected velocity for 2012-06-01:'
print y_projected

x_proj_1 = [t2, t2 + pd.Timedelta('%s days' %days_projected)]
y_proj_1 = [y2, y_projected]

# -----------------------------------------
t3 = pd.to_datetime('2012-10-02')
t4 = pd.to_datetime('2013-01-25')

y3 = 83.2
y4 = 86.5

x_2 = [t3, t4]
y_2 = [y3, y4]

m = (y4-y3) / (pd.Timedelta(t4-t3).days)

days_projected = 128
y_projected = m*days_projected + y4

print ''
print 'projected velocity for 2013-06-01:'
print y_projected

x_proj_2 = [t4, t4 + pd.Timedelta('%s days' %days_projected)]
y_proj_2 = [y4, y_projected]

# ------------------------------------------------------------------------------
# VELOCITY PLOTS:
# ------------------------------------------------------------------------------

fig1 = plt.figure()
ax = fig1.add_subplot(111)
#ax.plot(Brown_data_dt.index, Brown_data_dt.velocity_myear, color='r', alpha=0.9)
ax.plot(Brown_data_dt.index, Brown_data_dt.velocity_myear, 'k.', alpha=0.9)

plt.plot(x_1, y_1, color='r', linewidth=2)
plt.plot(x_proj_1, y_proj_1, color='r', linestyle='--', linewidth=2)
plt.plot(x_2, y_2, color='r', linewidth=2)
plt.plot(x_proj_2, y_proj_2, color='r', linestyle='--', linewidth=2)

ax.set_ylabel('GPS velocity (m/yr)')
#ax.set_xlim('2011-08-05','2011-09-25')
ax.set_xlim('2011-06-15','2013-06-15')
ax.set_ylim(0,350)
ax.xaxis.grid(True, which='major', color='black')
ax.xaxis.grid(True, which='minor', color='gray')
ax.set_title('27km-11, GPS velocity')
#plt.subplots_adjust(left=0.09, right=0.8, top=0.9, bottom=0.1)
plt.tight_layout()
plt.subplots_adjust(left=0.06)
#plt.legend(loc=1)

#plt.text('2011-08-07', 325, '15-min bin size, 6-hr smoothing, 6-hr velocity')

plt.show()