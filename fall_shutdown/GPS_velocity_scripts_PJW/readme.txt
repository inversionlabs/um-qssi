Patrick Wright
Inversion Labs, LLC
pwright@inversionlabs.com

May 1, 2018

--------------------------------------------------------------------------------
WHEN RUNNING SCRIPTS, YOU MUST DEFINE THE FOLLOWING:

1. Directory path to GPS position data for your local machine.
   (this should be the position data processed by J. Brown, in 'GPS_Pressure_Data_Organized')
   
2. Name of site (this is used only for title text on plots)

--> 1. and 2. are defined at the top of the script.

3. All parameters related to bin size, smoothing window size, and velocity time-step
must be manually adjusted throughout the script! Look at every section of the code
in detail and make sure the parameters are appropriate for your application. There
is detailed commenting for every section providing examples of how to define different
bin sizes / window length / etc. (there are also usually terminal print statements
that must be edited).
--------------------------------------------------------------------------------

Add'l info:

For all sites, default is:
15-min bin size, 8-hr smoothing, 8-hr velocity, 8-hr velocity smoothing
30-min bin size, 12-hr smoothing, 12-hr velocity, 12-hr velocity smoothing (winterspeedup)

All of the main functions for binning, smoothing, and velocity calculations are
directly from Joel Brown. The velocity results from these scripts are generally very
close to the results shown in J. Brown's giant PDF file.

I added all of Brown's functions together in a continuous script with comments
throughout, in addition to diagnostic plots (these can be uncommented). I also 
incorporate the use of Pandas, which uses Datetime-indexing. Creation of the
Pandas Datetime-indexed Dataframe occurs near the end of the script, prior to 
optional export of the velocity file and plotting of final velocity results. 

If you uncomment the velocity file export line, you will get a single .csv file of 
velocity results with datetime, DOY, and velocity (m/yr). This file can then be read
into an additional script for making plots.

The 'winterspeedup' files use longer binning and smoothing windows, and include
methods to plot a projected linear trend through the winter periods.

Note that I have added a procedure to remove positions with high reported error.
Feel free to adjust this limit, however 0.02 seems to be an appropriate general cutoff.
You will see this as:
# Define threshold limit for reported error (m):
limit = 0.02 # (m)

The velocity scripts in this directory are stripped-down and simplified versions,
intended for use by J. Harper to make plots needed for a winter speedup paper. I have 
stripped out any un-needed imports, functions, or sections of the script that were
not mandatory to get basic velocity results.

The original velocity scripts I developed during winter 2017-18 included some 
hastily developed methods to drop outliers from the north and east position data,
perform linear interpolation through the missing data, and then calculate velocity. 
Although this was helpful to address noisy velocity data for certain time periods, 
I generally found that these methods were not necessary and perhaps not an appropriate
way to treat the data. These methods can be re-examined and used if needed on a case 
by case basis. Contact P. Wright for more info or questions if you are having trouble 
getting clean velocity results.