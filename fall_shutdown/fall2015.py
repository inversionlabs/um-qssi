#!/usr/bin/env python

"""
Analysis of Fall 2014 GPS, pressure, and melt
Patrick Wright, Inversion Labs
Job: UM QSSI
Jan. 2018
"""

import datetime as dt
import numpy as np
import scipy as sp
import pandas as pd
import matplotlib
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from matplotlib.dates import date2num
#from matplotlib.ticker import MultipleLocator, FormatStrFormatter
from matplotlib import rcParams
from dateutil.relativedelta import relativedelta
import pylab
from IPython import embed
from math import factorial
from datetime import timedelta

# Convert DOY to datetime:
def dateparser_2011(doy):
  return dt.datetime(year=2011, month=1, day=1) + dt.timedelta(days=doy) - dt.timedelta(days=1)

def dateparser_2014(doy):
  return dt.datetime(year=2014, month=1, day=1) + dt.timedelta(days=doy) - dt.timedelta(days=1)

# set index of dataframes to datetime:
def set_index(df):
  index = pd.to_datetime(df.Datetime)
  df = df.set_index(index)
  df.index.rename('Datetime_index', inplace=True)
  df = df.drop('Datetime', axis=1)
  return df

# Load pressure data function (JB)
def loadPdata(filenam):
    with open(filenam,"r") as f:
        pData=np.loadtxt(f, skiprows=1,delimiter=',')
        return pData

# ------------------------------------------------------------------------------
# VELOCITY IMPORTS:
# ------------------------------------------------------------------------------

vel1 = pd.read_csv('velocity/processed_revisedJB/33km-14-West_velocity.csv', sep=',', header=0, names=['timestamp','DOY','velocity_myear'])
vel1_index = pd.to_datetime(vel1['timestamp'])
vel1_index.name = 'datetimeindex'
vel1 = vel1.set_index(vel1_index)
vel1 = vel1.drop('timestamp', axis=1)

# drop suspect periods:
#vel1['2015-08-27 13:00':'2015-08-31 10:00'] = np.NaN

vel2 = pd.read_csv('velocity/processed_revisedJB/33km-14-North_velocity.csv', sep=',', header=0, names=['timestamp','DOY','velocity_myear'])
vel2_index = pd.to_datetime(vel2['timestamp'])
vel2_index.name = 'datetimeindex'
vel2 = vel2.set_index(vel2_index)
vel2 = vel2.drop('timestamp', axis=1)

# drop suspect periods:
#vel2['2015-08-27 13:00':'2015-08-31 10:00'] = np.NaN

vel3 = pd.read_csv('velocity/processed_revisedJB/33km-14-East_velocity.csv', sep=',', header=0, names=['timestamp','DOY','velocity_myear'])
vel3_index = pd.to_datetime(vel3['timestamp'])
vel3_index.name = 'datetimeindex'
vel3 = vel3.set_index(vel3_index)
vel3 = vel3.drop('timestamp', axis=1)

# drop suspect periods:
#vel3['2015-08-27 13:00':'2015-08-31 10:00'] = np.NaN

vel4 = pd.read_csv('velocity/processed_revisedJB/33km-14-South_velocity.csv', sep=',', header=0, names=['timestamp','DOY','velocity_myear'])
vel4_index = pd.to_datetime(vel4['timestamp'])
vel4_index.name = 'datetimeindex'
vel4 = vel4.set_index(vel4_index)
vel4 = vel4.drop('timestamp', axis=1)

# drop suspect periods:
#vel4['2015-08-27 13:00':'2015-08-31 10:00'] = np.NaN

# ------------------------------------------------------------------------------
# PRESSURE IMPORTS: 
# ------------------------------------------------------------------------------

# NOTE: These pressure data are from 'GPS_Pressure_Data_Organized/GL14'
#       (this directory is replicated on Box)

# JB's loading function:
#-----------------------

##pData15Sd=loadPdata("GL_grid_15_S_Pw_drainage.txt")
#pData15Sd=loadPdata("GPS_Pressure_Data_Organized/GL14/GL_grid_15S_hifreq_5min_avg.txt")
##pData15N5=loadPdata("GL_grid_15_N_Pw_5m.txt")
#pData15N0=loadPdata("GPS_Pressure_Data_Organized/GL14/GL_grid_15_N_Pw_0m.txt")
##pData15EB5=loadPdata("GL_grid_15_E_B_Pw_5m.txt")
#pData15CB1=loadPdata("GPS_Pressure_Data_Organized/GL14/GL_grid_15_C_B_Pw_1m.txt")
##pData15CAd=loadPdata("GL_grid_15_CA_Pw_drainage.txt")
#pData15CAd=loadPdata("GPS_Pressure_Data_Organized/GL14/GL_grid_15CA_hifreq_5min_avg.txt")


# Panda's loading & PW pressure calculations:
#--------------------------------------------

pData15Sd = pd.read_csv('GPS_Pressure_Data_Organized/GL14/GL_grid_15S_hifreq_5min_avg.txt',sep=',', header=0, names=['DOY', 'Ps'])

pData15Sd_index = pData15Sd['DOY'].map(dateparser_2011) # make a datetime index
pData15Sd_index.name = 'timestamp' # change name of index
pData15Sd_dt = pData15Sd.set_index(pData15Sd_index) # set index to datetime, re-name
pData15Sd_scaled = (9.81 * 1000 * pData15Sd.Ps)  / (910 * 9.81 * 677) # pgh water column / pgh ice 
pData15Sd_scaled.name = 'scaled' # change name of series

pData15N0 = pd.read_csv('GPS_Pressure_Data_Organized/GL14/GL_grid_15_N_Pw_0m.txt',sep=',', header=0, names=['DOY', 'Ps'])

pData15N0_index = pData15N0['DOY'].map(dateparser_2011) # make a datetime index
pData15N0_index.name = 'timestamp' # change name of index
pData15N0_dt = pData15N0.set_index(pData15N0_index) # set index to datetime, re-name
pData15N0_scaled = (9.81 * 1000 * pData15N0.Ps)  / (910 * 9.81 * 635) # pgh water column / pgh ice 
pData15N0_scaled.name = 'scaled' # change name of series

pData15CB1 = pd.read_csv('GPS_Pressure_Data_Organized/GL14/GL_grid_15_C_B_Pw_1m.txt',sep=',', header=0, names=['DOY', 'Ps'])

pData15CB1_index = pData15CB1['DOY'].map(dateparser_2011) # make a datetime index
pData15CB1_index.name = 'timestamp' # change name of index
pData15CB1_dt = pData15CB1.set_index(pData15CB1_index) # set index to datetime, re-name
pData15CB1_scaled = (9.81 * 1000 * pData15CB1.Ps)  / (910 * 9.81 * 675) # pgh water column / pgh ice 
pData15CB1_scaled.name = 'scaled' # change name of series

pData15CAd = pd.read_csv('GPS_Pressure_Data_Organized/GL14/GL_grid_15CA_hifreq_5min_avg.txt',sep=',', header=0, names=['DOY', 'Ps'])

pData15CAd_index = pData15CAd['DOY'].map(dateparser_2011) # make a datetime index
pData15CAd_index.name = 'timestamp' # change name of index
pData15CAd_dt = pData15CAd.set_index(pData15CAd_index) # set index to datetime, re-name
pData15CAd_scaled = (9.81 * 1000 * pData15CAd.Ps)  / (910 * 9.81 * 671) # pgh water column / pgh ice 
pData15CAd_scaled.name = 'scaled' # change name of series

#embed()

# ------------------------------------------------------------------------------
# MELT PRODUCTION IMPORTS: NOTE: Files replaced on 12/1/17
# ------------------------------------------------------------------------------
GL14_melt = pd.read_csv('melt/melt_MTI_33km-14.csv', sep=',', header=0, names=['Datetime', 'optimized_mti'])
GL14_melt_index = pd.to_datetime(GL14_melt['Datetime'])
GL14_melt_index.name = 'timestamp'
GL14_melt = GL14_melt.set_index(GL14_melt_index)
GL14_melt = GL14_melt.drop('Datetime', axis=1)

# ------------------------------------------------------------------------------
# RAIN IMPORT (Block Site)
# ------------------------------------------------------------------------------
GL14_rain = pd.read_csv('melt/BlockSite-rain-dailytotal-meters.csv',
                        sep=',', header=0, names=['Datetime', 'rain_dailytotal_meters'])
GL14_rain_index = pd.to_datetime(GL14_rain['Datetime'])
GL14_rain_index.name = 'timestamp'
GL14_rain = GL14_rain.set_index(GL14_rain_index)
GL14_rain = GL14_rain.drop('Datetime', axis=1)

# ------------------------------------------------------------------------------
# PLOTS:
# ------------------------------------------------------------------------------

# MULTI-PANEL: velocity, pressure, melt

fig = plt.figure(figsize=(10,10))

ax1 = plt.subplot2grid((5, 1), (0, 0)) # GPS 
ax2 = plt.subplot2grid((5, 1), (1, 0)) # water pressure 
ax3 = plt.subplot2grid((5, 1), (2, 0)) # water pressure 
ax4 = plt.subplot2grid((5, 1), (3, 0)) # melt production
ax5 = plt.subplot2grid((5, 1), (4, 0)) # rain

ax1.plot(vel1.index, vel1.velocity_myear, label='West')
ax1.plot(vel2.index, vel2.velocity_myear, label='North')
ax1.plot(vel2.index, vel2.velocity_myear, label='East')
ax1.plot(vel2.index, vel2.velocity_myear, label='South')

ax1.set_xlim('2015-08-24','2015-09-14')
ax1.set_ylim(45,350)
ax1.set_ylabel('velocity (m/yr)')
ax1.tick_params(axis='x', labelbottom='off')

plt.setp(ax1.get_yticklabels()[1], visible=False)
#plt.setp(ax1.get_yticklabels()[-1], visible=False)
#plt.setp(ax1.get_yticklabels()[2], visible=False)
#plt.setp(ax1.get_yticklabels()[4], visible=False)
#plt.setp(ax1.get_yticklabels()[6], visible=False)

#ax1.yaxis.set_tick_params(labelsize=12)
ax1.legend(loc='upper right')
ax1.grid(color='black', alpha=0.3, linestyle='-', linewidth=1)
ax1.annotate('GPS Velocity', (mdates.datestr2num('2015-08-24 12:00'), 260), style='oblique')

#-------------------------------------------------------------------------------
ax2.plot(pData15Sd_dt.index, pData15Sd_scaled, label='South', linewidth=1)
#ax2.plot(pData15N0_dt.index, pData15N0_scaled, label='North', linewidth=1)
ax2.plot(pData15CAd_dt.index, pData15CAd_scaled, label='Center A', linewidth=1)
ax2.plot(pData15CB1_dt.index, pData15CB1_scaled, label='Center B', linewidth=1)


ax2.set_xlim('2015-08-24','2015-09-14')
ax2.tick_params(axis='x', labelbottom='off')
ax2.set_ylim(0.99,1.06)
#ax2.set_ylim(0.935,1.01)
ax2.set_ylabel('pressure (fract OB)')

#plt.setp(ax2.get_yticklabels()[0], visible=False)
#plt.setp(ax2.get_yticklabels()[-1], visible=False)
#plt.setp(ax2.get_yticklabels()[1], visible=False)
#plt.setp(ax2.get_yticklabels()[3], visible=False)
#plt.setp(ax2.get_yticklabels()[5], visible=False)

#ax2.yaxis.set_tick_params(labelsize=12)
ax2.legend(loc='lower right')
ax2.grid(color='black', alpha=0.3, linestyle='-', linewidth=1)
ax2.annotate('Water Pressure', (mdates.datestr2num('2015-08-24 12:00'), 1.05), style='oblique')

#-------------------------------------------------------------------------------

ax3.plot(pData15N0_dt.index, pData15N0_scaled, label='North', linewidth=1)

ax3.set_xlim('2015-08-24','2015-09-14')
ax3.tick_params(axis='x', labelbottom='off')
ax3.set_ylim(0.85,0.95)
ax3.set_ylabel('pressure (fract OB)')

#plt.setp(ax3.get_yticklabels()[0], visible=False)
#plt.setp(ax3.get_yticklabels()[-1], visible=False)
#plt.setp(ax3.get_yticklabels()[1], visible=False)
#plt.setp(ax3.get_yticklabels()[3], visible=False)
#plt.setp(ax3.get_yticklabels()[5], visible=False)

#ax3.yaxis.set_tick_params(labelsize=12)
ax3.legend(loc='lower right')
ax3.grid(color='black', alpha=0.3, linestyle='-', linewidth=1)
#ax3.annotate('Water Pressure', (mdates.datestr2num('2015-08-15 12:00'), 1.085), style='oblique')

#-------------------------------------------------------------------------------
WIDTH = 0.3
# align can only be 'center' or 'edge', where 'edge' uses left edge of bar at corresponding index
# This implementation plots bars at midnight, representing melt over following day.
ax4.bar(GL14_melt.index, GL14_melt.optimized_mti,
        width=WIDTH, edgecolor='black', color='red', align='center', label='33km-14')

ax4.xaxis_date()
ax4.set_ylabel('melt (m/day)')
ax4.set_xlim('2015-08-24','2015-09-14')
ax4.set_ylim(0,0.035)
#ax4.tick_params(axis='x', labelbottom='off')

#plt.setp(ax4.get_yticklabels()[0], visible=False)
#plt.setp(ax4.get_yticklabels()[-1], visible=False)
#plt.setp(ax4.get_yticklabels()[2], visible=False)
#plt.setp(ax4.get_yticklabels()[4], visible=False)
#plt.setp(ax4.get_yticklabels()[5], visible=False)

ax4.legend(loc='upper right')
ax4.grid(color='black', alpha=0.3, linestyle='-', linewidth=1)
ax4.annotate('Melt Production (MTI)', (mdates.datestr2num('2015-08-24 12:00'), 0.031), style='oblique')

#-------------------------------------------------------------------------------
WIDTH = 0.3
# align can only be 'center' or 'edge', where 'edge' uses left edge of bar at corresponding index
# This implementation plots bars at midnight, representing melt over following day.
ax5.bar(GL14_rain.index, GL14_rain.rain_dailytotal_meters,
        width=WIDTH, edgecolor='black', color='blue', align='center', label='rain_dailytotal')

ax5.xaxis_date()
ax5.set_ylabel('rain (m/day)')
ax5.set_xlim('2015-08-24','2015-09-14')
ax5.set_ylim(0,0.02)

plt.setp(ax5.get_yticklabels()[-1], visible=False)

ax5.legend(loc='upper right')
ax5.grid(color='black', alpha=0.3, linestyle='-', linewidth=1)
ax5.annotate('Rain Daily Total', (mdates.datestr2num('2015-08-24 12:00'), 0.018), style='oblique')

plt.subplots_adjust(wspace=0.001, hspace=0.001)
plt.subplots_adjust(
  left=0.085, right=0.95,
  top=0.95, bottom=0.13)

#plt.legend()

plt.xticks(rotation=90, fontsize=10)

#plt.savefig('meltvelocity.svg', format='svg')

plt.show()