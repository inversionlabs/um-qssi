#!/usr/bin/env python

"""
Analysis of Winter GPS
Patrick Wright, Inversion Labs
Job: UM QSSI
Jan. 2018
"""

import datetime as dt
import numpy as np
import scipy as sp
import pandas as pd
import matplotlib
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from matplotlib.dates import date2num
#from matplotlib.ticker import MultipleLocator, FormatStrFormatter
from matplotlib import rcParams
from dateutil.relativedelta import relativedelta
import pylab
from IPython import embed
from math import factorial
from datetime import timedelta

# set index of dataframes to datetime:
def set_index(df):
  index = pd.to_datetime(df.Datetime)
  df = df.set_index(index)
  df.index.rename('Datetime_index', inplace=True)
  df = df.drop('Datetime', axis=1)
  return df

# ------------------------------------------------------------------------------
# VELOCITY IMPORTS:
# ------------------------------------------------------------------------------

site = '33km-14-Center'

vel1 = pd.read_csv('velocity/processed_winter/33km-14-Center_velocity_winter.csv', sep=',', header=0, names=['timestamp','DOY','velocity_myear'])
vel1_index = pd.to_datetime(vel1['timestamp'])
vel1_index.name = 'datetimeindex'
vel1 = vel1.set_index(vel1_index)
vel1 = vel1.drop('timestamp', axis=1)

# ------------------------------------------------------------------------------
t1 = pd.to_datetime('2014-09-15')
t2 = pd.to_datetime('2014-12-08')

y1 = 96.6
y2 = 102.4

x_1 = [t1, t2]
y_1 = [y1, y2]

m = (y2-y1) / (pd.Timedelta(t2-t1).days)

days_projected = 192
y_projected = m*days_projected + y2

print ''
print y_projected

x_proj_1 = [t2, t2 + pd.Timedelta('%s days' %days_projected)]
y_proj_1 = [y2, y_projected]

# -----------------------------------------
t3 = pd.to_datetime('2015-09-15')
t4 = pd.to_datetime('2015-11-26')

y3 = 103.
y4 = 109.

x_2 = [t3, t4]
y_2 = [y3, y4]

m = (y4-y3) / (pd.Timedelta(t4-t3).days)

days_projected = 167
y_projected = m*days_projected + y4

print ''
print y_projected

x_proj_2 = [t4, t4 + pd.Timedelta('%s days' %days_projected)]
y_proj_2 = [y4, y_projected]

# -----------------------------------------
t5 = pd.to_datetime('2016-09-06')
t6 = pd.to_datetime('2016-11-21')

y5 = 90.6
y6 = 97.

x_3 = [t5, t6]
y_3 = [y5, y6]

m = (y6-y5) / (pd.Timedelta(t6-t5).days)

days_projected = 185
y_projected = m*days_projected + y6

print ''
print y_projected

x_proj_3 = [t6, t6 + pd.Timedelta('%s days' %days_projected)]
y_proj_3 = [y6, y_projected]

# ------------------------------------------------------------------------------
# VELOCITY PLOTS:
# ------------------------------------------------------------------------------

#majorLocator   = MultipleLocator(1)
#majorFormatter = FormatStrFormatter('%d')
#minorLocator   = MultipleLocator(0.5)

fig1 = plt.figure(figsize=(20,10))
ax = fig1.add_subplot(111)
#ax.plot(Brown_data_dt.index, Brown_data_dt.velocity_myear, color='r', alpha=0.9, label='JB positions')
ax.plot(vel1.index, vel1.velocity_myear, 'k.', alpha=0.9)

plt.plot(x_1, y_1, color='r', linewidth=2)
plt.plot(x_proj_1, y_proj_1, color='r', linestyle='--', linewidth=2)
plt.plot(x_2, y_2, color='r', linewidth=2)
plt.plot(x_proj_2, y_proj_2, color='r', linestyle='--', linewidth=2)
plt.plot(x_3, y_3, color='r', linewidth=2)
plt.plot(x_proj_3, y_proj_3, color='r', linestyle='--', linewidth=2)

ax.set_ylabel('GPS velocity (m/yr)')
#ax.set_xlim('2011-08-05','2011-09-25')
#ax.set_xlim('2014-07-01','2014-09-25')
ax.set_ylim(0,350)
#ax.xaxis.set_major_locator(majorLocator)
#ax.xaxis.set_major_formatter(majorFormatter)
#ax.xaxis.set_minor_locator(minorLocator)
ax.xaxis.grid(True, which='major', color='black')
ax.xaxis.grid(True, which='minor', color='gray')
ax.set_title('%s, GPS velocity' % site )
#plt.subplots_adjust(left=0.09, right=0.8, top=0.9, bottom=0.1)
plt.tight_layout()
plt.subplots_adjust(left=0.06)
plt.legend(loc=1)

#plt.text('2011-08-07', 325, '15-min bin size, 6-hr smoothing, 6-hr velocity')

plt.show()