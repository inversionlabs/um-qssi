#!/usr/bin/env python

"""
Analysis of Winter GPS
Patrick Wright, Inversion Labs
Job: UM QSSI
Jan. 2018
"""

import datetime as dt
import numpy as np
import scipy as sp
import pandas as pd
import matplotlib
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from matplotlib.dates import date2num
#from matplotlib.ticker import MultipleLocator, FormatStrFormatter
from matplotlib import rcParams
from dateutil.relativedelta import relativedelta
import pylab
from IPython import embed
from math import factorial
from datetime import timedelta

# set index of dataframes to datetime:
def set_index(df):
  index = pd.to_datetime(df.Datetime)
  df = df.set_index(index)
  df.index.rename('Datetime_index', inplace=True)
  df = df.drop('Datetime', axis=1)
  return df

# ------------------------------------------------------------------------------
# VELOCITY IMPORTS:
# ------------------------------------------------------------------------------

site = '27km-12'

vel1 = pd.read_csv('velocity/processed_winter/27km-12_velocity_winter.csv', sep=',', header=0, names=['timestamp','DOY','velocity_myear'])
vel1_index = pd.to_datetime(vel1['timestamp'])
vel1_index.name = 'datetimeindex'
vel1 = vel1.set_index(vel1_index)
vel1 = vel1.drop('timestamp', axis=1)

# ------------------------------------------------------------------------------
t1 = pd.to_datetime('2013-06-07')
t2 = pd.to_datetime('2013-05-01')

y1 = 81.
y2 = 80.2

x_1 = [t2, t1]
y_1 = [y2, y1]

m = (y2-y1) / (pd.Timedelta(t1-t2).days)

days_projected = 224
y_projected = m*days_projected + y2

print ''
print y_projected

x_proj_1 = [t2, t2 - pd.Timedelta('%s days' %days_projected)]
y_proj_1 = [y2, y_projected]

# ------------------------------------------------------------------------------
# VELOCITY PLOTS:
# ------------------------------------------------------------------------------

#majorLocator   = MultipleLocator(1)
#majorFormatter = FormatStrFormatter('%d')
#minorLocator   = MultipleLocator(0.5)

fig1 = plt.figure()
ax = fig1.add_subplot(111)
#ax.plot(Brown_data_dt.index, Brown_data_dt.velocity_myear, color='r', alpha=0.9)
ax.plot(vel1.index,vel1.velocity_myear, 'k.', alpha=0.9)

plt.plot(x_1, y_1, color='r', linewidth=2)
plt.plot(x_proj_1, y_proj_1, color='r', linestyle='--', linewidth=2)

ax.set_ylabel('GPS velocity (m/yr)')
#ax.set_xlim('2011-08-05','2011-09-25')
ax.set_ylim(0,350)
#ax.xaxis.set_major_locator(majorLocator)
#ax.xaxis.set_major_formatter(majorFormatter)
#ax.xaxis.set_minor_locator(minorLocator)
ax.xaxis.grid(True, which='major', color='black')
ax.xaxis.grid(True, which='minor', color='gray')
ax.set_title('27km-11, GPS velocity')
#plt.subplots_adjust(left=0.09, right=0.8, top=0.9, bottom=0.1)
plt.tight_layout()
plt.subplots_adjust(left=0.06)
plt.legend(loc=1)

#plt.text('2011-08-07', 325, '15-min bin size, 6-hr smoothing, 6-hr velocity')

plt.show()