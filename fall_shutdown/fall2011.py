#!/usr/bin/env python

"""
Analysis of Fall 2011 GPS, pressure, and melt
Patrick Wright, Inversion Labs
Job: UM QSSI
Jan. 2018
"""

import datetime as dt
import numpy as np
import scipy as sp
import pandas as pd
import matplotlib
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from matplotlib.dates import date2num
#from matplotlib.ticker import MultipleLocator, FormatStrFormatter
from matplotlib import rcParams
from dateutil.relativedelta import relativedelta
import pylab
from IPython import embed
from math import factorial
from datetime import timedelta

# Convert DOY to datetime:
def dateparser_2011(doy):
  return dt.datetime(year=2011, month=1, day=1) + dt.timedelta(days=doy) - dt.timedelta(days=1)

# set index of dataframes to datetime:
def set_index(df):
  index = pd.to_datetime(df.Datetime)
  df = df.set_index(index)
  df.index.rename('Datetime_index', inplace=True)
  df = df.drop('Datetime', axis=1)
  return df

# ------------------------------------------------------------------------------
# VELOCITY IMPORTS:
# ------------------------------------------------------------------------------

vel1 = pd.read_csv('velocity/processed/27km-11_velocity.csv', sep=',', header=0, names=['timestamp','DOY','velocity_myear'])
vel1_index = pd.to_datetime(vel1['timestamp'])
vel1_index.name = 'datetimeindex'
vel1 = vel1.set_index(vel1_index)
vel1 = vel1.drop('timestamp', axis=1)

# drop suspect periods:
vel1['2011-08-21 12:00':'2011-08-22 18:00'] = np.NaN
vel1['2011-09-04 15:00':'2011-09-06 13:00'] = np.NaN
vel1['2011-09-09 11:00':'2011-09-10 12:00'] = np.NaN
vel1['2011-09-13 21:00':'2011-09-15'] = np.NaN

vel2 = pd.read_csv('velocity/processed/46km-11_velocity.csv', sep=',', header=0, names=['timestamp','DOY','velocity_myear'])
vel2_index = pd.to_datetime(vel2['timestamp'])
vel2_index.name = 'datetimeindex'
vel2 = vel2.set_index(vel2_index)
vel2 = vel2.drop('timestamp', axis=1)

# drop suspect periods:
vel2['2011-08-04 12:00':'2011-08-06 03:00'] = np.NaN
vel2['2011-08-21 12:00':'2011-08-23'] = np.NaN
vel2['2011-09-05 03:00':'2011-09-06'] = np.NaN

# ------------------------------------------------------------------------------
# PRESSURE IMPORTS:
# ------------------------------------------------------------------------------

# NOTE: These pressure data are from '~/UM_RESEARCH/water_pressure/Pressure_data_PJWpublication'
#       (this directory is replicated on Box)
# NOTE: No resample / interpolate.... just for viewing trends. Data could have uneven sampling.

GL11_1A = pd.read_csv('pressure/GL11-1A.txt', sep=' ',skipinitialspace=True, header=None, names=['DOY', 'm_h2o'])
GL11_1A_index = GL11_1A['DOY'].map(dateparser_2011) # make a datetime index
GL11_1A_index.name = 'timestamp' # change name of index
GL11_1A_dt = GL11_1A.set_index(GL11_1A_index) # set index to datetime, re-name
GL11_1A_scaled = (9.81 * 1000. * GL11_1A_dt.m_h2o)  / (910. * 9.81 * 457.5) # pgh water column / pgh ice
GL11_1A_scaled.name = 'scaled' # change name of series

GL11_1B = pd.read_csv('pressure/GL11-1B.txt', sep=' ',skipinitialspace=True, header=None, names=['DOY', 'm_h2o'])
GL11_1B_index = GL11_1B['DOY'].map(dateparser_2011) # make a datetime index
GL11_1B_index.name = 'timestamp' # change name of index
GL11_1B_dt = GL11_1B.set_index(GL11_1B_index) # set index to datetime, re-name
GL11_1B_scaled = (9.81 * 1000. * GL11_1B_dt.m_h2o)  / (910. * 9.81 * 466.) # pgh water column / pgh ice
GL11_1B_scaled.name = 'scaled' # change name of series

GL11_1C = pd.read_csv('pressure/GL11-1C.txt', sep=' ',skipinitialspace=True, header=None, names=['DOY', 'm_h2o'])
GL11_1C_index = GL11_1C['DOY'].map(dateparser_2011) # make a datetime index
GL11_1C_index.name = 'timestamp' # change name of index
GL11_1C_dt = GL11_1C.set_index(GL11_1C_index) # set index to datetime, re-name
GL11_1C_scaled = (9.81 * 1000. * GL11_1C_dt.m_h2o)  / (910. * 9.81 * 459.5) # pgh water column / pgh ice
GL11_1C_scaled.name = 'scaled' # change name of series

GL11_2B = pd.read_csv('pressure/GL11-2B.txt', sep=' ',skipinitialspace=True, header=None, names=['DOY', 'm_h2o'])
GL11_2B_index = GL11_2B['DOY'].map(dateparser_2011) # make a datetime index
GL11_2B_index.name = 'timestamp' # change name of index
GL11_2B_dt = GL11_2B.set_index(GL11_2B_index) # set index to datetime, re-name
GL11_2B_scaled = (9.81 * 1000. * GL11_2B_dt.m_h2o)  / (910. * 9.81 * 821.) # pgh water column / pgh ice
GL11_2B_scaled.name = 'scaled' # change name of series

GL11_2D = pd.read_csv('pressure/GL11-2D.txt', sep=' ',skipinitialspace=True, header=None, names=['DOY', 'm_h2o'])
GL11_2D_index = GL11_2D['DOY'].map(dateparser_2011) # make a datetime index
GL11_2D_index.name = 'timestamp' # change name of index
GL11_2D_dt = GL11_2D.set_index(GL11_2D_index) # set index to datetime, re-name
GL11_2D_scaled = (9.81 * 1000. * GL11_2D_dt.m_h2o)  / (910. * 9.81 * 814.5) # pgh water column / pgh ice
GL11_2D_scaled.name = 'scaled' # change name of series

# ------------------------------------------------------------------------------
# MELT PRODUCTION IMPORTS: NOTE: Files replaced on 12/1/17
# ------------------------------------------------------------------------------
GL11_1_melt = pd.read_csv('melt/melt_MTI_27km-11.csv', sep=',', header=0, names=['Datetime', 'optimized_mti'])
GL11_1_melt_index = pd.to_datetime(GL11_1_melt['Datetime'])
GL11_1_melt_index.name = 'timestamp'
GL11_1_melt = GL11_1_melt.set_index(GL11_1_melt_index)
GL11_1_melt = GL11_1_melt.drop('Datetime', axis=1)

GL11_2_melt = pd.read_csv('melt/melt_MTI_46km-11.csv', sep=',', header=0, names=['Datetime', 'optimized_mti'])
GL11_2_melt_index = pd.to_datetime(GL11_2_melt['Datetime'])
GL11_2_melt_index.name = 'timestamp'
GL11_2_melt = GL11_2_melt.set_index(GL11_2_melt_index)
GL11_2_melt = GL11_2_melt.drop('Datetime', axis=1)

# ------------------------------------------------------------------------------
# PLOTS:
# ------------------------------------------------------------------------------

# MULTI-PANEL: velocity, pressure, melt

fig = plt.figure(figsize=(10,10))

ax1 = plt.subplot2grid((3, 1), (0, 0)) # GPS (GL11-1 & GL11-2)
ax2 = plt.subplot2grid((3, 1), (1, 0)) # water pressure (GL11-1 & GL11-2, all holes)
ax3 = plt.subplot2grid((3, 1), (2, 0)) # melt production (GL11-1 & GL11-2) -- paired bars?

ax1.plot(vel1.index, vel1.velocity_myear, color='r', label='27km-11')
ax1.plot(vel2.index, vel2.velocity_myear, color='b', label='46km-11')
ax1.set_xlim('2011-08-01','2011-09-14')
#ax1.set_xlim('2011-08-23','2011-09-03')
ax1.set_ylim(50,225)
ax1.set_ylabel('velocity (m/yr)')
ax1.tick_params(axis='x', labelbottom='off')
#plt.setp(ax1.get_yticklabels()[0], visible=False)
#plt.setp(ax1.get_yticklabels()[-1], visible=False)
#plt.setp(ax1.get_yticklabels()[2], visible=False)
#plt.setp(ax1.get_yticklabels()[4], visible=False)
#plt.setp(ax1.get_yticklabels()[6], visible=False)
#ax1.yaxis.set_tick_params(labelsize=12)
ax1.legend(loc='upper right')
ax1.grid(color='black', alpha=0.3, linestyle='-', linewidth=1)
ax1.annotate('GPS Velocity', (mdates.datestr2num('2011-08-16'), 210), style='oblique')


ax2.plot(GL11_1A_dt.index, GL11_1A_scaled, color='r', label='27km-11', linewidth=1)
ax2.plot(GL11_1B_dt.index, GL11_1B_scaled, color='r', label='', linewidth=1)
ax2.plot(GL11_1C_dt.index, GL11_1C_scaled, color='r', label='', linewidth=1)
ax2.plot(GL11_2B_dt.index, GL11_2B_scaled, color='b', label='46km-11', linewidth=1)
ax2.plot(GL11_2D_dt.index, GL11_2D_scaled, color='b', label='', linewidth=1)
ax2.set_xlim('2011-08-01','2011-09-14')
#ax2.set_xlim('2011-08-23','2011-09-03')
ax2.tick_params(axis='x', labelbottom='off')
ax2.set_ylim(0.82,1.05)
#ax2.set_ylim(0.935,1.01)
ax2.set_ylabel('pressure (fract OB)')
plt.setp(ax2.get_yticklabels()[0], visible=False)
#plt.setp(ax2.get_yticklabels()[-1], visible=False)
#plt.setp(ax2.get_yticklabels()[2], visible=False)
#plt.setp(ax2.get_yticklabels()[4], visible=False)
#plt.setp(ax2.get_yticklabels()[6], visible=False)
#ax2.yaxis.set_tick_params(labelsize=12)
ax2.legend(loc='upper right')
ax2.grid(color='black', alpha=0.3, linestyle='-', linewidth=1)
ax2.annotate('Water Pressure', (mdates.datestr2num('2011-08-16'), 1.03), style='oblique')

WIDTH = 0.3
# align can only be 'center' or 'edge', where 'edge' uses left edge of bar at corresponding index
# This implementation plots bars at midnight, representing melt over following day.
ax3.bar(GL11_1_melt.index -  pd.DateOffset(days=0.15), GL11_1_melt.optimized_mti,
        width=WIDTH, edgecolor='black', color='red', align='center', label='27km-11')
ax3.bar(GL11_2_melt.index +  pd.DateOffset(days=0.15), GL11_2_melt.optimized_mti,
        width=WIDTH, edgecolor='black', color='blue', align='center', label='46km-11')
ax3.xaxis_date()
ax3.set_ylabel('melt (m/day)')
ax3.set_xlim('2011-08-01','2011-09-14')
#ax3.set_xlim('2011-08-23','2011-09-03')
ax3.set_ylim(0,0.05)
#ax3.tick_params(axis='x', labelbottom='off')

#plt.setp(ax3.get_yticklabels()[0], visible=False)
#plt.setp(ax3.get_yticklabels()[-1], visible=False)
#plt.setp(ax3.get_yticklabels()[2], visible=False)
#plt.setp(ax3.get_yticklabels()[4], visible=False)
#plt.setp(ax3.get_yticklabels()[5], visible=False)

ax3.legend(loc='upper right')
ax3.grid(color='black', alpha=0.3, linestyle='-', linewidth=1)
ax3.annotate('Melt Production (MTI)', (mdates.datestr2num('2011-08-16'), 0.045), style='oblique')

plt.subplots_adjust(wspace=0.001, hspace=0.001)
plt.subplots_adjust(
  left=0.085, right=0.95,
  top=0.95, bottom=0.13)

#plt.legend()

plt.xticks(rotation=90, fontsize=10)

#plt.savefig('meltvelocity.svg', format='svg')

plt.show()